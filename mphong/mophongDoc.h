
// mophongDoc.h : interface of the CMpDoc class
//


#pragma once
#include "OpenGLView.h"
#include "PlotDlg.h"
#include "PlotManagerDlg.h"
class CMpDoc : public CDocument
{
protected: // create from serialization only
	CMpDoc();
	DECLARE_DYNCREATE(CMpDoc)

// Attributes
public:
	/* MyCodes Begin */
	// khoi tao moi truong OpenGL trong Lop CView
	COpenGLView		m_OpenGLView;
	CPlots			m_Plots;
	int m_nLoaiBaiToan;			// Dialog Control
	/* MyCodes End*/
// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CMpDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
	afx_msg void OnOptionsDlg();
	afx_msg void OnViewViewtop();
	afx_msg void OnViewViewbottom();
	afx_msg void OnViewViewleft();
	afx_msg void OnViewViewright();
	afx_msg void OnViewViewfront();
	afx_msg void OnViewViewback();
	afx_msg void OnViewAxis();
	afx_msg void OnUpdateViewAxis(CCmdUI *pCmdUI);
	afx_msg void OnViewViewperspective();
	// turn on OpenGL Context
	// OpenGL is a thread global context "state" machine -> a rule is always true
	// that means you must switch the OpenGL context of the current thread to the right window
	// so that drawing commands end up where you want them.
	void switchOpenGLContext(void);
};
