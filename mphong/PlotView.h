/*******************************************************************************/
/* Le Minh Nghia KSTN CDT 54 *26/09/2013* **************************************/
/* Class CPlotView -   *********************************************************/
// Khoi tao moi truong OpenGL cho dialog Plot
// Chua du lieu cua do thi ham so (nhieu lines = PlotData)
// hien thi ra cua so Dialog ( CPlotDlg)
/*******************************************************************************/
#pragma once
#include "openglwnd.h"
#include "Plots.h"
class CPlotView :
	public COpenGLWnd
{
public:
	int		m_nResize;						// cho lan khoi tao dau tien OnSize()
	bool	bCursorInfor;					// duong giong he truc toa do
private:
	CPlots	plots;							// encapsu data
	int		xpos, ypos;						// vi tri chuot
	//
	double xO, yO;							// toa do tam O
	double scaleX, scaleY;					// scaleY = (ymax - ymin) / height;
	// xu ly chuot
	bool	m_bLeftDown, m_bRightDown;
public:	// standard methods
	CPlotView(void);
	~CPlotView(void);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	// OpenGL: draw a floating point number (for tic mark labels)
	void drawNumber(double num);
	// OpenGL: draws the axes tic marks and labels them
	void drawTic(double tic, double atXY, int XY, double big);
	void drawAxes(void);
	// ve vi tri cua chuot
	void drawPosition(void);
	void drawGrid(int xmark = 40, int ymark = 40);
	void VeDoThiHamSo(void);
	// set range - [xmin, xmax], [ymin, ymax]
	void InitialAxis(double min_x, double max_x, double min_y, double max_y);
	// set range - [xmin, xmax]
	void InitialAxis(double min_x, double max_x);
	// tinh lai scaleX, scaleY
	void InitialAxis();
	void max_min(std::vector<double> XY, double &minv, double &maxv);
	void FitWindow(void);
	// methods for handling private data
	std::string getCaption(void);
	//
	void SetData(const CPlots& Plots);
	CPlots GetData(void);
	void SetBackGroundColor(COLORREF ref);
	void SetBackGroundColor(double r, double g, double b);
	COLORREF GetBackGroundColor(void);
	void SetDrawGridAxis(bool bGrid, bool bAxis);
};

