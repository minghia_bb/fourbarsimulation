#pragma once
#include "resource.h"
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();
	DECLARE_DYNAMIC(CAboutDlg)
// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	CString		m_strCaption;
	CString		m_strStatic2;
	virtual BOOL OnInitDialog();
};


