/*******************************************************************************/
/* Le Minh Nghia KSTN CDT 54 *26/09/2013* **************************************/
/* Class CPlotDlg - cua so de hien thi do thi ham so  **************************/
/*******************************************************************************/
#pragma once
#include "PlotView.h"		// data

// CPlotDlg dialog

class CPlotDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPlotDlg)

public:
	CPlotDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPlotDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_PLOT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	//CStatusBar	m_bar;
	bool bGrid;
	bool bAxis;
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CPlotView m_Plot;
	afx_msg void OnHelpAboutplotdlg();
	afx_msg void OnOptionsBckgrdcolor();
	afx_msg void OnOptionsDrawgrid();
	afx_msg void OnOptionsDrawaxes();
	afx_msg void OnUpdateOptionsDrawgrid(CCmdUI *pCmdUI);
	afx_msg void OnUpdateOptionsDrawaxes(CCmdUI *pCmdUI);
	// UpdateCommand message Handler not spported in menu dialog, so we need to handler this message
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnOptionsCursorinfor();
	afx_msg void OnUpdateOptionsCursorinfor(CCmdUI *pCmdUI);
	afx_msg void OnOptionsFitwindow();
};
