/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 26/9/2013
	Du lieu 1 dialog plot - n-line
*/
#pragma once
#include "PlotData.h"
class CPlots
{
public:
	// du lieu cua so hien thi
	std::string m_Caption;
	std::string xtitle, ytitle;
	double xmax, ymax, xmin, ymin;		// gioi han cua cac truc toa do
	bool	fitWindow;					// whether fitWindow
	bool bGrid, bAxes;					// whether drawGrid and drawAxis or not
	
	// du lieu do thi ham so
	std::vector<CPlotData>	plots;
	float	color_bgrd[4];				// color background
public:
	CPlots(void);
	~CPlots(void);
	// number of lines
	int size(void);
	// copy construction
	CPlotData operator[](int i_th);
	CPlots(const CPlots& plots);
	void AddPlot(std::vector<double> X, std::vector<double> Y, std::string legend,
		float r = 0.0, float g = 0.0, float b = 1.0, 
		int line_type = LINE_CONTINUOUS, double thickness = 1.0);
	void clear(void);
};

