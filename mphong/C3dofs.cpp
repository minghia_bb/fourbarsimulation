/*************************************************************************************************/
/* (c) Le Minh Nghia KSTN-CDT 54 09/13/2013                                                      */
/* Robot 3 BTD		: 2^3 * 10 = 80 functions													 */
/* Tinh toan vi tri, van toc, gia toc khau thao tac;											 */
/* tinh luc (momen) dat vao cac khop (PTVPCD)													 */
/* khong co lenh combine trong code Maple														 */
/*************************************************************************************************/
#include "StdAfx.h"
#include "C3dofs.h"
#include <math.h>

double C3dofs::th1 = 0.0;	double C3dofs::th2 = 0.0;	double C3dofs::th3 = 0.0;
double C3dofs::d1 = 18.0;	double C3dofs::d2 = 0.0;	double C3dofs::d3 = 0.0;
double C3dofs::a1 = 15.0;	double C3dofs::a2 = 10.0;	double C3dofs::a3 = 5.0;
double C3dofs::alp1 = PI/2;	double C3dofs::alp2 = 0.0;	double C3dofs::alp3 = 0.0;
//
double C3dofs::m1 = 3.5;	double C3dofs::m2 = 2.0;	double C3dofs::m3 = 1.0;		//[[kg]]
double C3dofs::g1 = 0.0;	double C3dofs::g2 = 0.0;	double C3dofs::g3 = -9.81;		//[[m.s^(-2)]]
double C3dofs::xCi1 = 7.5;	double C3dofs::yCi1 = 5.0;	double C3dofs::zCi1 = 2.5;
double C3dofs::xCi2 = 0.0;	double C3dofs::yCi2 = 0.0;	double C3dofs::zCi2	= 0.0;
double C3dofs::xCi3 = 0.0;	double C3dofs::yCi3 = 0.0;	double C3dofs::zCi3 = 0.0;
//
double C3dofs::Ix1 = 0.0;	double C3dofs::Iy1 = 0.0;	double C3dofs::Iz1 = 0.0;
double C3dofs::Ix2 = 0.0;	double C3dofs::Iy2 = 0.0;	double C3dofs::Iz2 = 0.0;
double C3dofs::Ix3 = 0.0;	double C3dofs::Iy3 = 0.0;	double C3dofs::Iz3 = 0.0;
C3dofs::C3dofs(void)
{
	// default for my robot - 3 dofs - RRR
	stlFile[0].ObjectName = "F:/Documents/C++/mp robot/stl_reader/stl_reader/de.stl";
	stlFile[1].ObjectName = "F:/Documents/C++/mp robot/stl_reader/stl_reader/khau1.stl";
	stlFile[2].ObjectName = "F:/Documents/C++/mp robot/stl_reader/stl_reader/khau2.stl";
	stlFile[3].ObjectName = "F:/Documents/C++/mp robot/stl_reader/stl_reader/khau3.stl";
	//
	pCenter = Vec3D(0.0, 0.0, (18.0 + 10.0 + 5.0) / 2.0);
	size = (15.0 + 10.0 + 5.0) * 2.0;
	numLink = 4;
	displayLists[0] = displayLists[1] = displayLists[2] = displayLists[3] = 0;
	// theta				d					a					alp				// don vi la degree
	bDH[0][0] =			bDH[0][1] =			bDH[0][2] =			bDH[0][3] = 0.0;	// link 0
	bDH[1][0] = 0.0;	bDH[1][1] = 18.0;	bDH[1][2] = 15.0;	bDH[1][3] = 90.0;	// link 1
	bDH[2][0] = 60.0;	bDH[2][1] = 0.0;	bDH[2][2] = 10.0;	bDH[2][3] = 0.0;	// link 2
	bDH[3][0] = -70.0;	bDH[3][1] = 0.0;	bDH[3][2] = 5.0;	bDH[3][3] = 0.0;	// link 3
	//
	color[0][0] = 0.2; color[0][1] = 0.2; color[0][2] = 0.8; color[0][3] = 1.0;
	color[1][0] = 0.5; color[1][1] = 0.8; color[1][2] = 0.0; color[1][3] = 1.0;
	color[2][0] = 0.8; color[2][1] = 0.8; color[2][2] = 1.0; color[2][3] = 1.0;
	color[3][0] = 0.1; color[3][1] = 0.8; color[3][2] = 0.2; color[3][3] = 1.0;
	// setting configuration
	m_bConfig[0] = m_bConfig[1] = m_bConfig[2] = true;
}


C3dofs::~C3dofs(void)
{
}


/*==========================================================================================================//
//*********** T-T-T * #0 ***********
//==========================================================================================================//
double C3dofs::xE_TTT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(th2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::yE_TTT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(th2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}


double C3dofs::zE_TTT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(th3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * q2 + q1);
}


double C3dofs::vEx_TTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t10;
  double t12;
  double t15;
  double t2;
  double t3;
  double t5;
  double t6;
  double t8;
  t1 = sin(th1);
  t2 = sin(alp1);
  t3 = t1 * t2;
  t5 = cos(th1);
  t6 = sin(th2);
  t8 = sin(alp2);
  t10 = cos(alp1);
  t12 = cos(th2);
  t15 = cos(alp2);
  return(t3 * dq2 + (t5 * t6 * t8 + t1 * t10 * t12 * t8 + t3 * t15) * dq3);
}


double C3dofs::vEy_TTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t10;
  double t12;
  double t15;
  double t2;
  double t3;
  double t5;
  double t6;
  double t8;
  t1 = cos(th1);
  t2 = sin(alp1);
  t3 = t1 * t2;
  t5 = sin(th1);
  t6 = sin(th2);
  t8 = sin(alp2);
  t10 = cos(alp1);
  t12 = cos(th2);
  t15 = cos(alp2);
  return(-t3 * dq2 + (t5 * t6 * t8 - t1 * t10 * t12 * t8 - t3 * t15) * dq3);
}


double C3dofs::vEz_TTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t3;
  double t4;
  double t6;
  double t8;
  t1 = cos(alp1);
  t3 = sin(alp1);
  t4 = cos(th2);
  t6 = sin(alp2);
  t8 = cos(alp2);
  return(dq1 + t1 * dq2 + (-t3 * t4 * t6 + t1 * t8) * dq3);
}


double C3dofs::aEx_TTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(th2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::aEy_TTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(th2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}

double C3dofs::aEz_TTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(th3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * q2 + q1);
}
//==========================================================================================================
//*********** R-T-T * #1 ***********
//==========================================================================================================
double C3dofs::xE_RTT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(th2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::yE_RTT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(th2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}


double C3dofs::zE_RTT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(th3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * q2 + d1);
}


double C3dofs::vEx_RTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t27;
  double t4;
  double t40;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(th2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  t27 = t2 * t20;
  t40 = t1 * t18;
  return(((-t1 * t2 - t6 * t7) * a3 * t11 + (t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (-t13 * t20 + t6 * t27 + t19 * t14) * q3 - t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 - a1 * t1) * dq1 + t40 * dq2 + (t4 * t7 * t20 + t1 * t5 * t27 + t40 * t14) * dq3);
}


double C3dofs::vEy_RTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t27;
  double t4;
  double t40;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(th2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  t27 = t2 * t20;
  t40 = t1 * t18;
  return(((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t27 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1) * dq1 - t40 * dq2 + (t4 * t7 * t20 - t1 * t5 * t27 - t40 * t14) * dq3);
}


double C3dofs::vEz_RTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t3;
  double t4;
  double t6;
  double t8;
  t1 = cos(alp1);
  t3 = sin(alp1);
  t4 = cos(th2);
  t6 = sin(alp2);
  t8 = cos(alp2);
  return(t1 * dq2 + (-t3 * t4 * t6 + t1 * t8) * dq3);
}


double C3dofs::aEx_RTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(th2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::aEy_RTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(th2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}


double C3dofs::aEz_RTT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(th3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * q2 + d1);
}

//==========================================================================================================
//*********** T-R-T * #2 ***********
//==========================================================================================================
double C3dofs::xE_TRT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(q2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1);
}


double C3dofs::yE_TRT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(q2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * d2 + a1 * t1);
}


double C3dofs::zE_TRT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(q2);
  t4 = cos(th3);
  t7 = cos(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * d2 + q1);
}


double C3dofs::vEx_TRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t2;
  double t20;
  double t22;
  double t3;
  double t37;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = sin(q2);
  t3 = t1 * t2;
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = cos(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t20 = sin(th3);
  t22 = sin(alp2);
  t37 = sin(alp1);
  return(((-t3 - t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14) * a3 * t20 + (t13 * t22 - t6 * t2 * t22) * q3 - t1 * a2 * t2 - t6 * a2 * t7) * dq2 + (t3 * t22 + t6 * t7 * t22 + t4 * t37 * t14) * dq3);
}


double C3dofs::vEy_TRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t2;
  double t20;
  double t22;
  double t3;
  double t37;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = sin(q2);
  t3 = t1 * t2;
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = cos(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t20 = sin(th3);
  t22 = sin(alp2);
  t37 = sin(alp1);
  return(((-t3 + t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14) * a3 * t20 + (t13 * t22 + t6 * t2 * t22) * q3 - t1 * a2 * t2 + t6 * a2 * t7) * dq2 + (t3 * t22 - t6 * t7 * t22 - t4 * t37 * t14) * dq3);
}


double C3dofs::vEz_TRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t14;
  double t2;
  double t22;
  double t3;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = cos(q2);
  t3 = t1 * t2;
  t4 = cos(th3);
  t7 = sin(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = sin(th3);
  t14 = sin(alp2);
  t22 = cos(alp1);
  return(dq1 + (t3 * a3 * t4 - t8 * t9 * a3 * t11 + t8 * t14 * q3 + t1 * a2 * t2) * dq2 + (-t3 * t14 + t9 * t22) * dq3);
}


double C3dofs::aEx_TRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(q2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1);
}


double C3dofs::aEy_TRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(q2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * d2 + a1 * t1);
}


double C3dofs::aEz_TRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(q2);
  t4 = cos(th3);
  t7 = cos(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * d2 + q1);
}


//==========================================================================================================//
// R-R-T * #3
//==========================================================================================================//
double C3dofs::xE_RRT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(q2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1);
}


double C3dofs::yE_RRT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(q2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * d2 + a1 * t1);
}


double C3dofs::zE_RRT (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(q2);
  t4 = cos(th3);
  t7 = cos(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * d2 + d1);
}


double C3dofs::vEx_RRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t27;
  double t4;
  double t40;
  double t41;
  double t46;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(q2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  t27 = t2 * t20;
  t40 = t4 * t7;
  t41 = t1 * t5;
  t46 = t4 * t2;
  return(((-t1 * t2 - t6 * t7) * a3 * t11 + (t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (-t13 * t20 + t6 * t27 + t19 * t14) * q3 - t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 - a1 * t1) * dq1 + ((-t40 - t41 * t2) * a3 * t11 + (-t46 * t14 + t41 * t7 * t14) * a3 * t24 + (t46 * t20 - t41 * t7 * t20) * q3 - t4 * a2 * t7 - t41 * a2 * t2) * dq2 + (t40 * t20 + t41 * t27 + t1 * t18 * t14) * dq3);
}


double C3dofs::vEy_RRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t27;
  double t4;
  double t40;
  double t41;
  double t46;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(q2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(th3);
  t27 = t2 * t20;
  t40 = t4 * t7;
  t41 = t1 * t5;
  t46 = t4 * t2;
  return(((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t27 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1) * dq1 + ((-t40 + t41 * t2) * a3 * t11 + (-t46 * t14 - t41 * t7 * t14) * a3 * t24 + (t46 * t20 + t41 * t7 * t20) * q3 - t4 * a2 * t7 + t41 * a2 * t2) * dq2 + (t40 * t20 - t41 * t27 - t1 * t18 * t14) * dq3);
}


double C3dofs::vEz_RRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t14;
  double t2;
  double t22;
  double t3;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = cos(q2);
  t3 = t1 * t2;
  t4 = cos(th3);
  t7 = sin(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = sin(th3);
  t14 = sin(alp2);
  t22 = cos(alp1);
  return((t3 * a3 * t4 - t8 * t9 * a3 * t11 + t8 * t14 * q3 + t1 * a2 * t2) * dq2 + (-t3 * t14 + t22 * t9) * dq3);
}


double C3dofs::aEx_RRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(q2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t18 * t4;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * q3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1);
}


double C3dofs::aEy_RRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(q2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(th3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t18 * t4;
  t20 = sin(alp2);
  t24 = sin(th3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * q3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * d2 + a1 * t1);
}


double C3dofs::aEz_RRT (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(q2);
  t4 = cos(th3);
  t7 = cos(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(th3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * q3 + t1 * a2 * t2 + t11 * d2 + d1);
}

//==========================================================================================================
// T-T-R * #4
//==========================================================================================================
double C3dofs::xE_TTR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(th2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::yE_TTR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(th2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * d3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}


double C3dofs::zE_TTR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(q3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(q3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * d3 + t1 * a2 * t2 + t11 * q2 + q1);
}


double C3dofs::vEx_TTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t10;
  double t14;
  double t17;
  double t2;
  double t21;
  double t25;
  double t3;
  double t5;
  double t6;
  double t8;
  double t9;
  t1 = sin(th1);
  t2 = sin(alp1);
  t3 = t1 * t2;
  t5 = cos(th1);
  t6 = cos(th2);
  t8 = cos(alp1);
  t9 = t1 * t8;
  t10 = sin(th2);
  t14 = sin(q3);
  t17 = cos(alp2);
  t21 = sin(alp2);
  t25 = cos(q3);
  return(t3 * dq2 + (-(t5 * t6 - t9 * t10) * a3 * t14 + (-t5 * t10 * t17 - t9 * t6 * t17 + t21 * t3) * a3 * t25) * dq3);
}


double C3dofs::vEy_TTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t10;
  double t14;
  double t17;
  double t2;
  double t21;
  double t25;
  double t3;
  double t5;
  double t6;
  double t8;
  double t9;
  t1 = cos(th1);
  t2 = sin(alp1);
  t3 = t1 * t2;
  t5 = sin(th1);
  t6 = cos(th2);
  t8 = cos(alp1);
  t9 = t1 * t8;
  t10 = sin(th2);
  t14 = sin(q3);
  t17 = cos(alp2);
  t21 = sin(alp2);
  t25 = cos(q3);
  return(-t3 * dq2 + (-(t5 * t6 + t9 * t10) * a3 * t14 + (-t5 * t10 * t17 + t9 * t6 * t17 - t21 * t3) * a3 * t25) * dq3);
}


double C3dofs::vEz_TTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t17;
  double t3;
  double t4;
  double t6;
  double t9;
  t1 = cos(alp1);
  t3 = sin(alp1);
  t4 = sin(th2);
  t6 = sin(q3);
  t9 = cos(th2);
  t11 = cos(alp2);
  t13 = sin(alp2);
  t17 = cos(q3);
  return(dq1 + t1 * dq2 + (-t3 * t4 * a3 * t6 + (t3 * t9 * t11 + t1 * t13) * a3 * t17) * dq3);
}


double C3dofs::aEx_TTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(th2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::aEy_TTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(th2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * d3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}


double C3dofs::aEz_TTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(q3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(q3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * d3 + t1 * a2 * t2 + t11 * q2 + q1);
}
//==========================================================================================================
// R-T-R * #5
//==========================================================================================================
double C3dofs::xE_RTR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(th2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::yE_RTR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(th2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * d3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}


double C3dofs::zE_RTR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(q3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(q3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * d3 + t1 * a2 * t2 + t11 * q2 + d1);
}


double C3dofs::vEx_RTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t16;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t40;
  double t43;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(th2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t16 = t2 * t14;
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  t40 = t1 * t18;
  t43 = t1 * t5;
  return(((-t1 * t2 - t6 * t7) * a3 * t11 + (t13 * t14 - t6 * t16 + t19 * t20) * a3 * t24 + (-t13 * t20 + t6 * t2 * t20 + t14 * t19) * d3 - t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 - a1 * t1) * dq1 + t40 * dq2 + (-(t4 * t2 - t43 * t7) * a3 * t24 + (-t4 * t7 * t14 - t43 * t16 + t40 * t20) * a3 * t11) * dq3);
}


double C3dofs::vEy_RTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t16;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t40;
  double t43;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(th2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t16 = t2 * t14;
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  t40 = t1 * t18;
  t43 = t1 * t5;
  return(((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t16 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t14 * t19) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1) * dq1 - t40 * dq2 + (-(t4 * t2 + t43 * t7) * a3 * t24 + (-t4 * t7 * t14 + t43 * t16 - t40 * t20) * a3 * t11) * dq3);
}


double C3dofs::vEz_RTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t17;
  double t3;
  double t4;
  double t6;
  double t9;
  t1 = cos(alp1);
  t3 = sin(alp1);
  t4 = sin(th2);
  t6 = sin(q3);
  t9 = cos(th2);
  t11 = cos(alp2);
  t13 = sin(alp2);
  t17 = cos(q3);
  return(t1 * dq2 + (-t3 * t4 * a3 * t6 + (t3 * t9 * t11 + t1 * t13) * a3 * t17) * dq3);
}


double C3dofs::aEx_RTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(th2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * q2 + a1 * t1);
}


double C3dofs::aEy_RTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(th2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(th2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * d3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * q2 + a1 * t1);
}


double C3dofs::aEz_RTR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(th2);
  t4 = cos(q3);
  t7 = cos(th2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(q3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * d3 + t1 * a2 * t2 + t11 * q2 + d1);
}
//==========================================================================================================
/T-R-R * #6
//==========================================================================================================
double C3dofs::xE_TRR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(q2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1);
}


double C3dofs::yE_TRR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(q2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * d3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * d2 + a1 * t1);
}


double C3dofs::zE_TRR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(q2);
  t4 = cos(q3);
  t7 = cos(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(q3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * d3 + t1 * a2 * t2 + t11 * d2 + q1);
}


double C3dofs::vEx_TRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t2;
  double t20;
  double t22;
  double t3;
  double t4;
  double t41;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = sin(q2);
  t3 = t1 * t2;
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = cos(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t20 = sin(q3);
  t22 = sin(alp2);
  t41 = sin(alp1);
  return(((-t3 - t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14) * a3 * t20 + (t13 * t22 - t6 * t2 * t22) * d3 - t1 * a2 * t2 - t6 * a2 * t7) * dq2 + (-(t13 - t6 * t2) * a3 * t20 + (-t3 * t14 - t6 * t7 * t14 + t4 * t41 * t22) * a3 * t11) * dq3);
}


double C3dofs::vEy_TRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t2;
  double t20;
  double t22;
  double t3;
  double t4;
  double t41;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = sin(q2);
  t3 = t1 * t2;
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = cos(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t20 = sin(q3);
  t22 = sin(alp2);
  t41 = sin(alp1);
  return(((-t3 + t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14) * a3 * t20 + (t13 * t22 + t6 * t2 * t22) * d3 - t1 * a2 * t2 + t6 * a2 * t7) * dq2 + (-(t13 + t6 * t2) * a3 * t20 + (-t3 * t14 + t6 * t7 * t14 - t4 * t41 * t22) * a3 * t11) * dq3);
}


double C3dofs::vEz_TRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t14;
  double t2;
  double t24;
  double t3;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = cos(q2);
  t3 = t1 * t2;
  t4 = cos(q3);
  t7 = sin(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = sin(q3);
  t14 = sin(alp2);
  t24 = cos(alp1);
  return(dq1 + (t3 * a3 * t4 - t8 * t9 * a3 * t11 + t8 * t14 * d3 + t1 * a2 * t2) * dq2 + (-t8 * a3 * t11 + (t3 * t9 + t24 * t14) * a3 * t4) * dq3);
}


double C3dofs::aEx_TRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(th1);
  t2 = cos(q2);
  t4 = sin(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1);
}


double C3dofs::aEy_TRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(th1);
  t2 = cos(q2);
  t4 = cos(th1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * d3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * d2 + a1 * t1);
}


double C3dofs::aEz_TRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(q2);
  t4 = cos(q3);
  t7 = cos(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(q3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * d3 + t1 * a2 * t2 + t11 * d2 + q1);
}
*/
//==========================================================================================================
// R-R-R * #7
//==========================================================================================================
double C3dofs::xE_RRR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(q2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t2 * t14 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t19 * t14) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1);
}


double C3dofs::yE_RRR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(q2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  return((t1 * t2 + t6 * t7) * a3 * t11 + (-t13 * t14 + t6 * t2 * t14 - t19 * t20) * a3 * t24 + (t13 * t20 - t6 * t2 * t20 - t19 * t14) * d3 + t1 * a2 * t2 + t6 * a2 * t7 - t19 * d2 + a1 * t1);
}


double C3dofs::zE_RRR (double q1, double q2, double q3)
{
  double t1;
  double t11;
  double t12;
  double t16;
  double t2;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = sin(q2);
  t4 = cos(q3);
  t7 = cos(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = cos(alp1);
  t12 = sin(alp2);
  t16 = sin(q3);
  return(t1 * t2 * a3 * t4 + (t8 * t9 + t11 * t12) * a3 * t16 + (-t12 * t8 + t11 * t9) * d3 + t1 * a2 * t2 + t11 * d2 + d1);
}


double C3dofs::vEx_RRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t16;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t40;
  double t41;
  double t46;
  double t5;
  double t6;
  double t7;
  t1 = sin(q1);
  t2 = cos(q2);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t16 = t2 * t14;
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  t40 = t4 * t7;
  t41 = t1 * t5;
  t46 = t4 * t2;
  return(((-t1 * t2 - t6 * t7) * a3 * t11 + (t13 * t14 - t6 * t16 + t19 * t20) * a3 * t24 + (-t13 * t20 + t6 * t2 * t20 + t14 * t19) * d3 - t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 - a1 * t1) * dq1 + ((-t40 - t41 * t2) * a3 * t11 + (-t46 * t14 + t41 * t7 * t14) * a3 * t24 + (t46 * t20 - t41 * t7 * t20) * d3 - t4 * a2 * t7 - t41 * a2 * t2) * dq2 + (-(t46 - t41 * t7) * a3 * t24 + (-t40 * t14 - t41 * t16 + t1 * t18 * t20) * a3 * t11) * dq3);
}


double C3dofs::vEy_RRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t13;
  double t14;
  double t16;
  double t18;
  double t19;
  double t2;
  double t20;
  double t24;
  double t4;
  double t40;
  double t41;
  double t46;
  double t5;
  double t6;
  double t7;
  t1 = cos(q1);
  t2 = cos(q2);
  t4 = sin(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t16 = t2 * t14;
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t24 = sin(q3);
  t40 = t4 * t7;
  t41 = t1 * t5;
  t46 = t4 * t2;
  return(((t1 * t2 - t6 * t7) * a3 * t11 + (-t13 * t14 - t6 * t16 + t19 * t20) * a3 * t24 + (t13 * t20 + t6 * t2 * t20 + t14 * t19) * d3 + t1 * a2 * t2 - t6 * a2 * t7 + t19 * d2 + a1 * t1) * dq1 + ((-t40 + t41 * t2) * a3 * t11 + (-t46 * t14 - t41 * t7 * t14) * a3 * t24 + (t46 * t20 + t41 * t7 * t20) * d3 - t4 * a2 * t7 + t41 * a2 * t2) * dq2 + (-(t46 + t41 * t7) * a3 * t24 + (-t40 * t14 + t41 * t16 - t1 * t18 * t20) * a3 * t11) * dq3);
}


double C3dofs::vEz_RRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3)
{
  double t1;
  double t11;
  double t14;
  double t2;
  double t24;
  double t3;
  double t4;
  double t7;
  double t8;
  double t9;
  t1 = sin(alp1);
  t2 = cos(q2);
  t3 = t1 * t2;
  t4 = cos(q3);
  t7 = sin(q2);
  t8 = t1 * t7;
  t9 = cos(alp2);
  t11 = sin(q3);
  t14 = sin(alp2);
  t24 = cos(alp1);
  return((t3 * a3 * t4 - t8 * t9 * a3 * t11 + t8 * t14 * d3 + t1 * a2 * t2) * dq2 + (-t8 * a3 * t11 + (t3 * t9 + t24 * t14) * a3 * t4) * dq3);
}


double C3dofs::aEx_RRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
	return (((-cos(q1) * cos(q2) + sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (cos(q1) * sin(q2) * cos(alp2) + sin(q1) * cos(alp1) * cos(q2) * cos(alp2) - sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * sin(alp2) - sin(q1) * cos(alp1) * cos(q2) * sin(alp2) - sin(q1) * sin(alp1) * cos(alp2)) * d3 - cos(q1) * a2 * cos(q2) + sin(q1) * cos(alp1) * a2 * sin(q2) - sin(q1) * sin(alp1) * d2 - a1 * cos(q1)) * dq1 + ((sin(q1) * sin(q2) - cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (sin(q1) * cos(q2) * cos(alp2) + cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * sin(alp2) - cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 + sin(q1) * a2 * sin(q2) - cos(q1) * cos(alp1) * a2 * cos(q2)) * dq2 + (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq3) * dq1 + (((sin(q1) * sin(q2) - cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (sin(q1) * cos(q2) * cos(alp2) + cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * sin(alp2) - cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 + sin(q1) * a2 * sin(q2) - cos(q1) * cos(alp1) * a2 * cos(q2)) * dq1 + ((-cos(q1) * cos(q2) + sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (cos(q1) * sin(q2) * cos(alp2) + sin(q1) * cos(alp1) * cos(q2) * cos(alp2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * sin(alp2) - sin(q1) * cos(alp1) * cos(q2) * sin(alp2)) * d3 - cos(q1) * a2 * cos(q2) + sin(q1) * cos(alp1) * a2 * sin(q2)) * dq2 + (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq3) * dq2 + ((-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq1 + (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq2 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3)) * dq3) * dq3 + ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) + cos(q1) * sin(alp1) * d2 - a1 * sin(q1)) * ddq1 + ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) * ddq2 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * ddq3;
}


double C3dofs::aEy_RRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
	return (((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) + cos(q1) * sin(alp1) * d2 - a1 * sin(q1)) * dq1 + ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) * dq2 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq3) * dq1 + (((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) * dq1 + ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2)) * dq2 + (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq3) * dq2 + ((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq1 + (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq2 + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3)) * dq3) * dq3 + ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * d3 + cos(q1) * a2 * cos(q2) - sin(q1) * cos(alp1) * a2 * sin(q2) + sin(q1) * sin(alp1) * d2 + a1 * cos(q1)) * ddq1 + ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - sin(q1) * a2 * sin(q2) + cos(q1) * cos(alp1) * a2 * cos(q2)) * ddq2 + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * ddq3;
}


double C3dofs::aEz_RRR (
  double q1,
  double q2,
  double q3,
  double dq1,
  double dq2,
  double dq3,
  double ddq1,
  double ddq2,
  double ddq3)
{
	return ((-sin(alp1) * sin(q2) * a3 * cos(q3) - sin(alp1) * cos(q2) * cos(alp2) * a3 * sin(q3) + sin(alp1) * cos(q2) * sin(alp2) * d3 - sin(alp1) * a2 * sin(q2)) * dq2 + (-sin(alp1) * cos(q2) * a3 * sin(q3) - sin(alp1) * sin(q2) * cos(alp2) * a3 * cos(q3)) * dq3) * dq2 + ((-sin(alp1) * cos(q2) * a3 * sin(q3) - sin(alp1) * sin(q2) * cos(alp2) * a3 * cos(q3)) * dq2 + (-sin(alp1) * sin(q2) * a3 * cos(q3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * a3 * sin(q3)) * dq3) * dq3 + (sin(alp1) * cos(q2) * a3 * cos(q3) - sin(alp1) * sin(q2) * cos(alp2) * a3 * sin(q3) + sin(alp1) * sin(q2) * sin(alp2) * d3 + sin(alp1) * a2 * cos(q2)) * ddq2 + (-sin(alp1) * sin(q2) * a3 * sin(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * a3 * cos(q3)) * ddq3;
}


void C3dofs::H_RRR (double q1, double q2, double q3, double dq1, double dq2, double dq3, double cgret[3])
{
  double t11;
  double t12;
  double t13;
  double t15;
  double t17;
  double t19;
  double t2;
  double t20;
  double t23;
  double t25;
  double t26;
  double t27;
  double t29;
  double t30;
  double t31;
  double t32;
  double t33;
  double t34;
  double t35;
  double t36;
  double t38;
  double t4;
  double t40;
  double t41;
  double t42;
  double t44;
  double t46;
  double t47;
  double t48;
  double t49;
  double t5;
  double t50;
  double t51;
  double t52;
  double t53;
  double t55;
  double t56;
  double t57;
  double t58;
  double t59;
  double t6;
  double t61;
  double t63;
  double t64;
  double t66;
  double t68;
  double t69;
  double t70;
  double t71;
  double t72;
  double t74;
  double t76;
  double t78;
  double t8;
  double t81;
  double t84;
  double t87;
  double t9;
  double t92;
  double t95;
  double t98;
  double t1001;
  double t1002;
  double t1006;
  double t1019;
  double t1021;
  double t103;
  double t1032;
  double t1033;
  double t1038;
  double t1039;
  double t1042;
  double t1045;
  double t1048;
  double t1053;
  double t106;
  double t1064;
  double t1065;
  double t1066;
  double t1072;
  double t109;
  double t1100;
  double t1102;
  double t111;
  double t113;
  double t114;
  double t116;
  double t117;
  double t118;
  double t119;
  double t120;
  double t122;
  double t123;
  double t124;
  double t125;
  double t127;
  double t129;
  double t131;
  double t134;
  double t136;
  double t137;
  double t138;
  double t141;
  double t142;
  double t143;
  double t145;
  double t147;
  double t148;
  double t149;
  double t152;
  double t154;
  double t157;
  double t160;
  double t164;
  double t166;
  double t167;
  double t168;
  double t170;
  double t171;
  double t172;
  double t173;
  double t175;
  double t177;
  double t178;
  double t180;
  double t182;
  double t183;
  double t184;
  double t185;
  double t188;
  double t191;
  double t192;
  double t193;
  double t194;
  double t196;
  double t199;
  double t204;
  double t207;
  double t208;
  double t209;
  double t212;
  double t217;
  double t220;
  double t221;
  double t223;
  double t226;
  double t231;
  double t233;
  double t237;
  double t240;
  double t242;
  double t245;
  double t247;
  double t249;
  double t250;
  double t251;
  double t252;
  double t253;
  double t255;
  double t259;
  double t263;
  double t265;
  double t266;
  double t267;
  double t268;
  double t270;
  double t271;
  double t273;
  double t274;
  double t277;
  double t278;
  double t280;
  double t281;
  double t284;
  double t285;
  double t287;
  double t288;
  double t292;
  double t294;
  double t296;
  double t299;
  double t304;
  double t311;
  double t312;
  double t313;
  double t314;
  double t315;
  double t317;
  double t319;
  double t322;
  double t327;
  double t329;
  double t331;
  double t334;
  double t335;
  double t337;
  double t338;
  double t340;
  double t343;
  double t345;
  double t346;
  double t349;
  double t350;
  double t351;
  double t352;
  double t354;
  double t355;
  double t358;
  double t362;
  double t364;
  double t365;
  double t367;
  double t369;
  double t373;
  double t377;
  double t381;
  double t382;
  double t383;
  double t387;
  double t391;
  double t395;
  double t396;
  double t398;
  double t401;
  double t403;
  double t404;
  double t407;
  double t408;
  double t410;
  double t411;
  double t414;
  double t417;
  double t419;
  double t420;
  double t422;
  double t429;
  double t434;
  double t437;
  double t438;
  double t440;
  double t444;
  double t449;
  double t450;
  double t451;
  double t453;
  double t457;
  double t464;
  double t465;
  double t467;
  double t471;
  double t473;
  double t478;
  double t480;
  double t483;
  double t485;
  double t488;
  double t489;
  double t490;
  double t491;
  double t492;
  double t494;
  double t495;
  double t497;
  double t498;
  double t500;
  double t502;
  double t504;
  double t508;
  double t510;
  double t513;
  double t514;
  double t515;
  double t517;
  double t519;
  double t521;
  double t523;
  double t527;
  double t529;
  double t532;
  double t533;
  double t535;
  double t537;
  double t539;
  double t541;
  double t545;
  double t547;
  double t548;
  double t555;
  double t557;
  double t559;
  double t563;
  double t566;
  double t568;
  double t572;
  double t576;
  double t579;
  double t581;
  double t585;
  double t590;
  double t592;
  double t596;
  double t599;
  double t604;
  double t607;
  double t608;
  double t610;
  double t612;
  double t614;
  double t615;
  double t616;
  double t618;
  double t620;
  double t631;
  double t639;
  double t642;
  double t650;
  double t652;
  double t653;
  double t655;
  double t657;
  double t660;
  double t664;
  double t669;
  double t671;
  double t672;
  double t674;
  double t676;
  double t679;
  double t683;
  double t688;
  double t690;
  double t699;
  double t701;
  double t703;
  double t712;
  double t714;
  double t716;
  double t724;
  double t726;
  double t728;
  double t729;
  double t730;
  double t731;
  double t732;
  double t733;
  double t737;
  double t741;
  double t745;
  double t746;
  double t747;
  double t748;
  double t749;
  double t753;
  double t757;
  double t761;
  double t762;
  double t763;
  double t766;
  double t768;
  double t773;
  double t774;
  double t775;
  double t777;
  double t780;
  double t782;
  double t787;
  double t788;
  double t789;
  double t791;
  double t794;
  double t796;
  double t800;
  double t801;
  double t802;
  double t805;
  double t806;
  double t807;
  double t809;
  double t811;
  double t813;
  double t815;
  double t817;
  double t819;
  double t822;
  double t823;
  double t834;
  double t839;
  double t845;
  double t851;
  double t857;
  double t860;
  double t861;
  double t862;
  double t863;
  double t865;
  double t867;
  double t869;
  double t871;
  double t872;
  double t874;
  double t876;
  double t880;
  double t888;
  double t890;
  double t898;
  double t900;
  double t908;
  double t910;
  double t913;
  double t914;
  double t917;
  double t919;
  double t921;
  double t923;
  double t943;
  double t946;
  double t948;
  double t950;
  double t951;
  double t953;
  double t957;
  double t964;
  double t969;
  double t971;
  double t972;
  double t974;
  double t977;
  double t981;
  double t984;
  double t989;
  double t992;
  double t993;
  double t997;
  double A[3]; 
  A[0] = A[1] = A[2] = 0.0;
  t2 = sin(q1);
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t8 = sin(alp1);
  t9 = t4 * t8;
  t11 = a1 * t2;
  t12 = -t2 * xCi1 - t6 * yCi1 + t9 * zCi1 - t11;
  t13 = m1 * t12;
  t15 = t2 * t5;
  t17 = t2 * t8;
  t19 = a1 * t4;
  t20 = -t4 * xCi1 + t15 * yCi1 - t17 * zCi1 - t19;
  t23 = -m1 * t20;
  t25 = cos(q2);
  t26 = t2 * t25;
  t27 = sin(q2);
  t29 = -t26 - t6 * t27;
  t30 = t29 * xCi2;
  t31 = t2 * t27;
  t32 = cos(alp2);
  t33 = t31 * t32;
  t34 = t25 * t32;
  t35 = t6 * t34;
  t36 = sin(alp2);
  t38 = t33 - t35 + t9 * t36;
  t40 = t31 * t36;
  t41 = t25 * t36;
  t42 = t6 * t41;
  t44 = -t40 + t42 + t9 * t32;
  t46 = t2 * a2;
  t47 = t46 * t25;
  t48 = a2 * t27;
  t49 = t6 * t48;
  t50 = t9 * d2;
  t51 = t30 + t38 * yCi2 + t44 * zCi2 - t47 - t49 + t50 - t11;
  t52 = m2 * t51;
  t53 = t4 * t25;
  t55 = -t53 + t15 * t27;
  t56 = t55 * xCi2;
  t57 = t4 * t27;
  t58 = t57 * t32;
  t59 = t15 * t34;
  t61 = t58 + t59 - t17 * t36;
  t63 = t57 * t36;
  t64 = t15 * t41;
  t66 = -t63 - t64 - t17 * t32;
  t68 = t4 * a2;
  t69 = t68 * t25;
  t70 = t15 * t48;
  t71 = t17 * d2;
  t72 = t56 + t61 * yCi2 + t66 * zCi2 - t69 + t70 - t71 - t19;
  t74 = -t55;
  t76 = -t61;
  t78 = -t66;
  t81 = m2 * (t74 * xCi2 + t76 * yCi2 + t78 * zCi2 + t69 - t70 + t71 + t19);
  t84 = -t44;
  t87 = (t78 * t38 + t84 * t76) * Ix2;
  t92 = t44 * t38 + t78 * t61 + t78 * t76 + t84 * t38;
  t95 = -t29;
  t98 = (t74 * t44 + t95 * t78) * Iy2;
  t103 = t29 * t44 + t74 * t66 + t74 * t78 + t95 * t44;
  t106 = -t38;
  t109 = (t76 * t29 + t106 * t74) * Iz2;
  t111 = t76 * t55;
  t113 = t106 * t29;
  t114 = t38 * t29 + t111 + t76 * t74 + t113;
  t116 = cos(q3);
  t117 = t29 * t116;
  t118 = sin(q3);
  t119 = t38 * t118;
  t120 = t117 + t119;
  t122 = t29 * t118;
  t123 = cos(alp3);
  t124 = t122 * t123;
  t125 = t38 * t116;
  t127 = sin(alp3);
  t129 = -t124 + t125 * t123 + t44 * t127;
  t131 = t122 * t127;
  t134 = t131 - t125 * t127 + t44 * t123;
  t136 = t29 * a3;
  t137 = t136 * t116;
  t138 = t38 * a3;
  t141 = t120 * xCi3 + t129 * yCi3 + t134 * zCi3 + t137 + t138 * t118 + t44 * d3 - t47 - t49 + t50 - t11;
  t142 = m3 * t141;
  t143 = t55 * t116;
  t145 = t143 + t61 * t118;
  t147 = t55 * t118;
  t148 = t147 * t123;
  t149 = t61 * t116;
  t152 = -t148 + t149 * t123 + t66 * t127;
  t154 = t147 * t127;
  t157 = t154 - t149 * t127 + t66 * t123;
  t160 = t55 * a3 * t116;
  t164 = t145 * xCi3 + t152 * yCi3 + t157 * zCi3 + t160 + t61 * a3 * t118 + t66 * d3 - t69 + t70 - t71 - t19;
  t166 = t74 * t116;
  t167 = t76 * t118;
  t168 = t166 + t167;
  t170 = t74 * t118;
  t171 = t170 * t123;
  t172 = t76 * t116;
  t173 = t172 * t123;
  t175 = -t171 + t173 + t78 * t127;
  t177 = t170 * t127;
  t178 = t172 * t127;
  t180 = t177 - t178 + t78 * t123;
  t182 = t74 * a3;
  t183 = t182 * t116;
  t184 = t76 * a3;
  t185 = t184 * t118;
  t188 = m3 * (t168 * xCi3 + t175 * yCi3 + t180 * zCi3 + t183 + t185 + t78 * d3 + t69 - t70 + t71 + t19);
  t191 = t95 * t118;
  t192 = t191 * t127;
  t193 = t106 * t116;
  t194 = t193 * t127;
  t196 = t192 - t194 + t84 * t123;
  t199 = (t180 * t129 + t196 * t175) * Ix3;
  t204 = t134 * t129 + t180 * t152 + t180 * t175 + t196 * t129;
  t207 = t95 * t116;
  t208 = t106 * t118;
  t209 = t207 + t208;
  t212 = (t168 * t134 + t209 * t180) * Iy3;
  t217 = t120 * t134 + t168 * t157 + t168 * t180 + t209 * t134;
  t220 = t191 * t123;
  t221 = t193 * t123;
  t223 = -t220 + t221 + t84 * t127;
  t226 = (t175 * t120 + t223 * t168) * Iz3;
  t231 = t129 * t120 + t175 * t145 + t175 * t168 + t223 * t120;
  t233 = t13 * t20 + t23 * t12 + t52 * t72 + t81 * t51 + t87 * t92 + t98 * t103 + t109 * t114 + t142 * t164 + t188 * t141 + t199 * t204 + t212 * t217 + t226 * t231;
  t237 = t31 - t6 * t25;
  t240 = t27 * t32;
  t242 = t26 * t32 + t6 * t240;
  t245 = t27 * t36;
  t247 = -t26 * t36 - t6 * t245;
  t249 = t46 * t27;
  t250 = a2 * t25;
  t251 = t6 * t250;
  t252 = t237 * xCi2 + t242 * yCi2 + t247 * zCi2 + t249 - t251;
  t253 = t52 * t252;
  t255 = -t57 - t15 * t25;
  t259 = -t53 * t32 + t15 * t240;
  t263 = t53 * t36 - t15 * t245;
  t265 = t68 * t27;
  t266 = t15 * t250;
  t267 = t255 * xCi2 + t259 * yCi2 + t263 * zCi2 - t265 - t266;
  t268 = t81 * t267;
  t270 = t78 * t242;
  t271 = -t247;
  t273 = t84 * t259;
  t274 = t263 * t38 + t270 + t271 * t76 + t273;
  t277 = t74 * t247;
  t278 = -t237;
  t280 = t95 * t263;
  t281 = t255 * t44 + t277 + t278 * t78 + t280;
  t284 = t76 * t237;
  t285 = -t242;
  t287 = t106 * t255;
  t288 = t259 * t29 + t284 + t285 * t74 + t287;
  t292 = t237 * t116 + t242 * t118;
  t294 = t237 * t118;
  t296 = t242 * t116;
  t299 = -t294 * t123 + t296 * t123 + t247 * t127;
  t304 = t294 * t127 - t296 * t127 + t247 * t123;
  t311 = t292 * xCi3 + t299 * yCi3 + t304 * zCi3 + t237 * a3 * t116 + t242 * a3 * t118 + t247 * d3 + t249 - t251;
  t312 = t142 * t311;
  t313 = t255 * t116;
  t314 = t259 * t118;
  t315 = t313 + t314;
  t317 = t255 * t118;
  t319 = t259 * t116;
  t322 = -t317 * t123 + t319 * t123 + t263 * t127;
  t327 = t317 * t127 - t319 * t127 + t263 * t123;
  t329 = t255 * a3;
  t331 = t259 * a3;
  t334 = t315 * xCi3 + t322 * yCi3 + t327 * zCi3 + t329 * t116 + t331 * t118 + t263 * d3 - t265 - t266;
  t335 = t188 * t334;
  t337 = t180 * t299;
  t338 = t278 * t118;
  t340 = t285 * t116;
  t343 = t338 * t127 - t340 * t127 + t271 * t123;
  t345 = t196 * t322;
  t346 = t327 * t129 + t337 + t343 * t175 + t345;
  t349 = t168 * t304;
  t350 = t278 * t116;
  t351 = t285 * t118;
  t352 = t350 + t351;
  t354 = t209 * t327;
  t355 = t315 * t134 + t349 + t352 * t180 + t354;
  t358 = t175 * t292;
  t362 = -t338 * t123 + t340 * t123 + t271 * t127;
  t364 = t223 * t315;
  t365 = t322 * t120 + t358 + t362 * t168 + t364;
  t367 = t253 + t268 + t87 * t274 + t98 * t281 + t109 * t288 + t312 + t335 + t199 * t346 + t212 * t355 + t226 * t365;
  t369 = -t122 + t125;
  t373 = -t117 * t123 - t119 * t123;
  t377 = t117 * t127 + t119 * t127;
  t381 = t369 * xCi3 + t373 * yCi3 + t377 * zCi3 - t136 * t118 + t138 * t116;
  t382 = t142 * t381;
  t383 = -t170 + t172;
  t387 = -t166 * t123 - t167 * t123;
  t391 = t166 * t127 + t167 * t127;
  t395 = t383 * xCi3 + t387 * yCi3 + t391 * zCi3 - t182 * t118 + t184 * t116;
  t396 = t188 * t395;
  t398 = t180 * t373;
  t401 = t207 * t127 + t208 * t127;
  t403 = t196 * t387;
  t404 = t391 * t129 + t398 + t401 * t175 + t403;
  t407 = t168 * t377;
  t408 = -t191 + t193;
  t410 = t209 * t391;
  t411 = t383 * t134 + t407 + t408 * t180 + t410;
  t414 = t175 * t369;
  t417 = -t207 * t123 - t208 * t123;
  t419 = t223 * t383;
  t420 = t387 * t120 + t414 + t417 * t168 + t419;
  t422 = t382 + t396 + t199 * t404 + t212 * t411 + t226 * t420;
  t429 = t278 * xCi2 + t285 * yCi2 + t271 * zCi2 - t249 + t251;
  t434 = t8 * t25;
  t437 = -t434 * t36 + t5 * t32;
  t438 = t437 * t8;
  t440 = t78 * t259 + t84 * t285 - t438 * t240;
  t444 = t44 * t259 + t270 + t78 * t285 + t273;
  t449 = t8 * t8;
  t450 = t27 * t27;
  t451 = t449 * t450;
  t453 = t74 * t263 + t95 * t271 + t451 * t36;
  t457 = t29 * t263 + t277 + t74 * t271 + t280;
  t464 = t434 * t32 + t5 * t36;
  t465 = t464 * t8;
  t467 = t76 * t255 + t106 * t278 + t465 * t25;
  t471 = t38 * t255 + t284 + t76 * t278 + t287;
  t473 = m3 * t164;
  t478 = t278 * a3;
  t480 = t285 * a3;
  t483 = t352 * xCi3 + t362 * yCi3 + t343 * zCi3 + t478 * t116 + t480 * t118 + t271 * d3 - t249 + t251;
  t485 = t204 * Ix3;
  t488 = t8 * t27;
  t489 = t118 * t127;
  t490 = t488 * t489;
  t491 = t464 * t116;
  t492 = t491 * t127;
  t494 = t490 - t492 + t437 * t123;
  t495 = t118 * t123;
  t497 = t32 * t116;
  t498 = t497 * t123;
  t500 = t36 * t127;
  t502 = -t434 * t495 - t488 * t498 + t488 * t500;
  t504 = t180 * t322 + t196 * t362 + t494 * t502;
  t508 = t134 * t322 + t337 + t180 * t362 + t345;
  t510 = t217 * Iy3;
  t513 = t488 * t116;
  t514 = t464 * t118;
  t515 = t513 + t514;
  t517 = t497 * t127;
  t519 = t36 * t123;
  t521 = t434 * t489 + t488 * t517 + t488 * t519;
  t523 = t168 * t327 + t209 * t343 + t515 * t521;
  t527 = t120 * t327 + t349 + t168 * t343 + t354;
  t529 = t231 * Iz3;
  t532 = t488 * t495;
  t533 = t491 * t123;
  t535 = -t532 + t533 + t437 * t127;
  t537 = t32 * t118;
  t539 = t434 * t116 - t488 * t537;
  t541 = t175 * t315 + t223 * t352 + t535 * t539;
  t545 = t129 * t315 + t358 + t175 * t352 + t364;
  t547 = m2 * t72 * t267 + t253 + t52 * t429 + t268 + t92 * Ix2 * t440 + t87 * t444 + t103 * Iy2 * t453 + t98 * t457 + t114 * Iz2 * t467 + t109 * t471 + t473 * t334 + t312 + t142 * t483 + t335 + t485 * t504 + t199 * t508 + t510 * t523 + t212 * t527 + t529 * t541 + t226 * t545;
  t548 = t547 / 0.2e1;
  t555 = t95 * a3;
  t557 = t106 * a3;
  t559 = t408 * xCi3 + t417 * yCi3 + t401 * zCi3 - t555 * t118 + t557 * t116;
  t563 = t116 * t123;
  t566 = -t488 * t563 - t514 * t123;
  t568 = t180 * t387 + t196 * t417 + t494 * t566;
  t572 = t134 * t387 + t398 + t180 * t417 + t403;
  t576 = t116 * t127;
  t579 = t488 * t576 + t514 * t127;
  t581 = t168 * t391 + t209 * t401 + t515 * t579;
  t585 = t120 * t391 + t407 + t168 * t401 + t410;
  t590 = -t488 * t118 + t491;
  t592 = t175 * t383 + t223 * t408 + t535 * t590;
  t596 = t129 * t383 + t414 + t175 * t408 + t419;
  t599 = t473 * t395 / 0.2e1 + t382 / 0.2e1 + t142 * t559 / 0.2e1 + t396 / 0.2e1 + t485 * t568 / 0.2e1 + t199 * t572 / 0.2e1 + t510 * t581 / 0.2e1 + t212 * t585 / 0.2e1 + t529 * t592 / 0.2e1 + t226 * t596 / 0.2e1;
  t604 = t548 * dq1;
  t607 = m2 * t252 * t267;
  t608 = t58 + t59;
  t610 = -t63 - t64;
  t612 = t56 + t608 * yCi2 + t610 * zCi2 - t69 + t70;
  t614 = m2 * t267;
  t615 = t614 * t429;
  t616 = t33 - t35;
  t618 = -t40 + t42;
  t620 = t30 + t616 * yCi2 + t618 * zCi2 - t47 - t49;
  t631 = t263 * t259 + t78 * t608 + t271 * t285 + t84 * t616 - t451 * t36 * t32 - t438 * t34;
  t639 = t449 * t27;
  t642 = t255 * t263 + t74 * t610 + t278 * t271 + t95 * t618 + 0.2e1 * t639 * t41;
  t650 = t259 * t255 + t111 + t285 * t278 + t113 - t639 * t34 - t465 * t27;
  t652 = m3 * t311;
  t653 = t652 * t334;
  t655 = t143 + t608 * t118;
  t657 = t608 * t116;
  t660 = -t148 + t657 * t123 + t610 * t127;
  t664 = t154 - t657 * t127 + t610 * t123;
  t669 = t655 * xCi3 + t660 * yCi3 + t664 * zCi3 + t160 + t608 * a3 * t118 + t610 * d3 - t69 + t70;
  t671 = m3 * t334;
  t672 = t671 * t483;
  t674 = t117 + t616 * t118;
  t676 = t616 * t116;
  t679 = -t124 + t676 * t123 + t618 * t127;
  t683 = t131 - t676 * t127 + t618 * t123;
  t688 = t674 * xCi3 + t679 * yCi3 + t683 * zCi3 + t137 + t616 * a3 * t118 + t618 * d3 - t47 - t49;
  t690 = t346 * Ix3;
  t699 = t532 - t434 * t498 + t434 * t500;
  t701 = t327 * t322 + t180 * t660 + t343 * t362 + t196 * t679 + t521 * t502 + t494 * t699;
  t703 = t355 * Iy3;
  t712 = -t490 + t434 * t517 + t434 * t519;
  t714 = t315 * t327 + t168 * t664 + t352 * t343 + t209 * t683 + t539 * t521 + t515 * t712;
  t716 = t365 * Iz3;
  t724 = -t513 - t434 * t537;
  t726 = t322 * t315 + t175 * t655 + t362 * t352 + t223 * t674 + t502 * t539 + t535 * t724;
  t728 = t607 + t52 * t612 + t615 + t81 * t620 + t274 * Ix2 * t440 + t87 * t631 + t281 * Iy2 * t453 + t98 * t642 + t288 * Iz2 * t467 + t109 * t650 + t653 + t142 * t669 + t672 + t188 * t688 + t690 * t504 + t199 * t701 + t703 * t523 + t212 * t714 + t716 * t541 + t226 * t726;
  t729 = t728 / 0.2e1;
  t730 = t729 * dq2;
  t731 = m3 * t381;
  t732 = t731 * t334;
  t733 = -t317 + t319;
  t737 = -t313 * t123 - t314 * t123;
  t741 = t313 * t127 + t314 * t127;
  t745 = t733 * xCi3 + t737 * yCi3 + t741 * zCi3 - t329 * t118 + t331 * t116;
  t746 = t142 * t745;
  t747 = m3 * t395;
  t748 = t747 * t483;
  t749 = -t338 + t340;
  t753 = -t350 * t123 - t351 * t123;
  t757 = t350 * t127 + t351 * t127;
  t761 = t749 * xCi3 + t753 * yCi3 + t757 * zCi3 - t478 * t118 + t480 * t116;
  t762 = t188 * t761;
  t763 = t404 * Ix3;
  t766 = t180 * t737;
  t768 = t196 * t753;
  t773 = -t434 * t563 + t488 * t537 * t123;
  t774 = t494 * t773;
  t775 = t391 * t322 + t766 + t401 * t362 + t768 + t579 * t502 + t774;
  t777 = t411 * Iy3;
  t780 = t168 * t741;
  t782 = t209 * t757;
  t787 = t434 * t576 - t488 * t537 * t127;
  t788 = t515 * t787;
  t789 = t383 * t327 + t780 + t408 * t343 + t782 + t590 * t521 + t788;
  t791 = t420 * Iz3;
  t794 = t175 * t733;
  t796 = t223 * t749;
  t800 = -t434 * t118 - t488 * t497;
  t801 = t535 * t800;
  t802 = t387 * t315 + t794 + t417 * t352 + t796 + t566 * t539 + t801;
  t805 = t732 / 0.2e1 + t746 / 0.2e1 + t748 / 0.2e1 + t762 / 0.2e1 + t763 * t504 / 0.2e1 + t199 * t775 / 0.2e1 + t777 * t523 / 0.2e1 + t212 * t789 / 0.2e1 + t791 * t541 / 0.2e1 + t226 * t802 / 0.2e1;
  t806 = t805 * dq3;
  t807 = t440 * Ix2;
  t809 = t453 * Iy2;
  t811 = t467 * Iz2;
  t813 = t504 * Ix3;
  t815 = t523 * Iy3;
  t817 = t541 * Iz3;
  t819 = t607 + t615 + t807 * t444 + t809 * t457 + t811 * t471 + t653 + t672 + t813 * t508 + t815 * t527 + t817 * t545;
  t822 = t652 * t395;
  t823 = t671 * t559;
  t834 = t822 / 0.2e1 + t732 / 0.2e1 + t823 / 0.2e1 + t748 / 0.2e1 + t508 * Ix3 * t568 / 0.2e1 + t813 * t572 / 0.2e1 + t527 * Iy3 * t581 / 0.2e1 + t815 * t585 / 0.2e1 + t545 * Iz3 * t592 / 0.2e1 + t817 * t596 / 0.2e1;
  t839 = t599 * dq1;
  t845 = t327 * t387 + t766 + t343 * t417 + t768 + t521 * t566 + t774;
  t851 = t315 * t391 + t780 + t352 * t401 + t782 + t539 * t579 + t788;
  t857 = t322 * t383 + t794 + t362 * t408 + t796 + t502 * t590 + t801;
  t860 = t822 / 0.2e1 + t746 / 0.2e1 + t823 / 0.2e1 + t762 / 0.2e1 + t690 * t568 / 0.2e1 + t199 * t845 / 0.2e1 + t703 * t581 / 0.2e1 + t212 * t851 / 0.2e1 + t716 * t592 / 0.2e1 + t226 * t857 / 0.2e1;
  t861 = t860 * dq2;
  t862 = t731 * t395;
  t863 = -t168;
  t865 = t171 - t173;
  t867 = -t177 + t178;
  t869 = t863 * xCi3 + t865 * yCi3 + t867 * zCi3 - t183 - t185;
  t871 = t747 * t559;
  t872 = -t209;
  t874 = t220 - t221;
  t876 = -t192 + t194;
  t880 = t872 * xCi3 + t874 * yCi3 + t876 * zCi3 - t555 * t116 - t557 * t118;
  t888 = t532 - t533;
  t890 = t391 * t387 + t180 * t865 + t401 * t417 + t196 * t874 + t579 * t566 + t494 * t888;
  t898 = -t490 + t492;
  t900 = t383 * t391 + t168 * t867 + t408 * t401 + t209 * t876 + t590 * t579 + t515 * t898;
  t908 = -t515;
  t910 = t387 * t383 + t175 * t863 + t417 * t408 + t223 * t872 + t566 * t590 + t535 * t908;
  t913 = t862 / 0.2e1 + t142 * t869 / 0.2e1 + t871 / 0.2e1 + t188 * t880 / 0.2e1 + t763 * t568 / 0.2e1 + t199 * t890 / 0.2e1 + t777 * t581 / 0.2e1 + t212 * t900 / 0.2e1 + t791 * t592 / 0.2e1 + t226 * t910 / 0.2e1;
  t914 = t913 * dq3;
  t917 = t568 * Ix3;
  t919 = t581 * Iy3;
  t921 = t592 * Iz3;
  t923 = t862 + t871 + t917 * t572 + t919 * t585 + t921 * t596;
  A[0] = (t233 * dq1 / 0.2e1 + t367 * dq2 + t422 * dq3 - t548 * dq2 / 0.2e1 - t599 * dq3 / 0.2e1) * dq1 + (t604 / 0.2e1 + t730 + t806 - t819 * dq2 / 0.2e1 - t834 * dq3 / 0.2e1) * dq2 + (t839 / 0.2e1 + t861 + t914 - t834 * dq2 / 0.2e1 - t923 * dq3 / 0.2e1) * dq3 - t13 * g1 - t23 * g2 - t52 * g1 - t81 * g2 - t142 * g1 - t188 * g2;
  t943 = m2 * t429;
  t946 = t32 * yCi2;
  t948 = t36 * zCi2;
  t950 = t8 * a2;
  t951 = t950 * t25;
  t953 = m2 * (t434 * xCi2 - t488 * t946 + t488 * t948 + t951);
  t957 = t950 * t27;
  t964 = m3 * t483;
  t969 = a3 * t116;
  t971 = t32 * a3;
  t972 = t971 * t118;
  t974 = t36 * d3;
  t977 = m3 * (t539 * xCi3 + t502 * yCi3 + t521 * zCi3 + t434 * t969 - t488 * t972 + t488 * t974 + t951);
  t981 = t488 * t969;
  t984 = t724 * xCi3 + t699 * yCi3 + t712 * zCi3 - t981 - t434 * t972 + t434 * t974 - t957;
  t989 = t614 * t612 + t943 * t620 + t953 * (-t488 * xCi2 - t434 * t946 + t434 * t948 - t957) + t807 * t631 + t809 * t642 + t811 * t650 + t671 * t669 + t964 * t688 + t977 * t984 + t813 * t701 + t815 * t714 + t817 * t726;
  t992 = t671 * t745;
  t993 = t964 * t761;
  t997 = a3 * t118;
  t1001 = t800 * xCi3 + t773 * yCi3 + t787 * zCi3 - t434 * t997 - t488 * t971 * t116;
  t1002 = t977 * t1001;
  t1006 = t992 + t993 + t1002 + t813 * t775 + t815 * t789 + t817 * t802;
  t1019 = t464 * a3;
  t1021 = t590 * xCi3 + t566 * yCi3 + t579 * zCi3 - t488 * t997 + t1019 * t116;
  t1032 = m3 * t669 * t395 + t992 + m3 * t688 * t559 + t993 + m3 * t984 * t1021 + t1002 + t701 * Ix3 * t568 + t813 * t845 + t714 * Iy3 * t581 + t815 * t851 + t726 * Iz3 * t592 + t817 * t857;
  t1033 = t1032 / 0.2e1;
  t1038 = t834 * dq1;
  t1039 = t1033 * dq2;
  t1042 = m3 * t745 * t395;
  t1045 = m3 * t761 * t559;
  t1048 = m3 * t1001 * t1021;
  t1053 = t908 * xCi3 + t888 * yCi3 + t898 * zCi3 - t981 - t1019 * t118;
  t1064 = t1042 + t671 * t869 + t1045 + t964 * t880 + t1048 + t977 * t1053 + t775 * Ix3 * t568 + t813 * t890 + t789 * Iy3 * t581 + t815 * t900 + t802 * Iz3 * t592 + t817 * t910;
  t1065 = t1064 / 0.2e1;
  t1066 = t1065 * dq3;
  t1072 = t1042 + t1045 + t1048 + t917 * t845 + t919 * t851 + t921 * t857;
  A[1] = (t604 + t730 / 0.2e1 + t806 - t367 * dq1 / 0.2e1 - t860 * dq3 / 0.2e1) * dq1 + (t819 * dq1 + t989 * dq2 / 0.2e1 + t1006 * dq3 - t729 * dq1 / 0.2e1 - t1033 * dq3 / 0.2e1) * dq2 + (t1038 + t1039 / 0.2e1 + t1066 - t860 * dq1 / 0.2e1 - t1072 * dq3 / 0.2e1) * dq3 - t614 * g1 - t943 * g2 - t953 * g3 - t671 * g1 - t964 * g2 - t977 * g3;
  t1100 = m3 * t559;
  t1102 = m3 * t1021;
  A[2] = (t839 + t861 + t914 / 0.2e1 - t422 * dq1 / 0.2e1 - t805 * dq2 / 0.2e1) * dq1 + (t1038 + t1039 + t1066 / 0.2e1 - t805 * dq1 / 0.2e1 - t1006 * dq2 / 0.2e1) * dq2 + (t923 * dq1 + t1072 * dq2 + (t747 * t869 + t1100 * t880 + t1102 * t1053 + t917 * t890 + t919 * t900 + t921 * t910) * dq3 / 0.2e1 - t913 * dq1 / 0.2e1 - t1065 * dq2 / 0.2e1) * dq3 - t747 * g1 - t1100 * g2 - t1102 * g3;
  cgret[0] = A[0];
  cgret[1] = A[1];
  cgret[2] = A[2];
}

// ma tran khoi luong 
void C3dofs::M_RRR (double q1, double q2, double q3, double cgret[3][3])
{
  double A[3][3];
  A[0][0] = A[0][1] = A[0][2] = 0.0;
  A[1][0] = A[1][1] = A[1][2] = 0.0;
  A[2][0] = A[2][1] = A[2][2] = 0.0;
  A[0][0] = m1 * pow(-sin(q1) * xCi1 - cos(q1) * cos(alp1) * yCi1 + cos(q1) * sin(alp1) * zCi1 - a1 * sin(q1), 0.2e1) / 0.2e1 
	  + m1 * pow(cos(q1) * xCi1 - sin(q1) * cos(alp1) * yCi1 + sin(q1) * sin(alp1) * zCi1 + a1 * cos(q1), 0.2e1) / 0.2e1 + pow(pow(cos(q1), 0.2e1) * sin(alp1) 
	  + pow(sin(q1), 0.2e1) * sin(alp1), 0.2e1) * Iy1 / 0.2e1 
	  + pow(pow(sin(q1), 0.2e1) * cos(alp1) 
	  + pow(cos(q1), 0.2e1) * cos(alp1), 0.2e1) * Iz1 / 0.2e1 
	  + m2 * pow((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * xCi2 
	  + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) * yCi2 + (-sin(q1) * sin(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * zCi2 
	  - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) + cos(q1) * sin(alp1) * d2 
	  - a1 * sin(q1), 0.2e1) / 0.2e1 + m2 * pow((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * xCi2 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * yCi2 
	  + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * zCi2 
	  + cos(q1) * a2 * cos(q2) - sin(q1) * cos(alp1) * a2 * sin(q2) + sin(q1) * sin(alp1) * d2 + a1 * cos(q1), 0.2e1) / 0.2e1 
	  + pow((cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)), 0.2e1) * Ix2 / 0.2e1 
	  + pow((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * (-sin(q1) * sin(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) 
	  + (sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)), 0.2e1) * Iy2 / 0.2e1 
	  + pow((-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * (-sin(q1) * cos(q2) 
	  - cos(q1) * cos(alp1) * sin(q2)) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * (cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)), 0.2e1) * Iz2 / 0.2e1 
	  + m3 * pow(((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * xCi3 
	  + (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) 
	  + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * yCi3 
	  + ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * zCi3 
	  + (-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) + cos(q1) * sin(alp1) * d2 
	  - a1 * sin(q1), 0.2e1) / 0.2e1 + m3 * pow(((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * xCi3 
	  + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * yCi3 + ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) 
	  - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) 
	  + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * zCi3 
	  + (cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * d3 
	  + cos(q1) * a2 * cos(q2) - sin(q1) * cos(alp1) * a2 * sin(q2) + sin(q1) * sin(alp1) * d2 + a1 * cos(q1), 0.2e1) / 0.2e1 
	  + pow(((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) 
	  + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) 
	  + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) 
	  + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)), 0.2e1) * Ix3 / 0.2e1 
	  + pow(((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)), 0.2e1) * Iy3 / 0.2e1 
	  + pow((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * cos(q3) 
	  + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) 
	  + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)), 0.2e1) * Iz3 / 0.2e1;
  A[0][1] = m2 * ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * xCi2 + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * yCi2 + (-sin(q1) * sin(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * zCi2 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) 
	  + cos(q1) * sin(alp1) * d2 - a1 * sin(q1)) * ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * xCi2 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * yCi2 + (cos(q1) * cos(q2) * sin(alp2) 
	  - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * zCi2 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) / 0.2e1 
	  + m2 * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * xCi2 + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * yCi2 + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * zCi2 
	  + cos(q1) * a2 * cos(q2) - sin(q1) * cos(alp1) * a2 * sin(q2) + sin(q1) * sin(alp1) * d2 + a1 * cos(q1)) * ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * xCi2 
	  + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * yCi2 + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * zCi2 
	  - sin(q1) * a2 * sin(q2) + cos(q1) * cos(alp1) * a2 * cos(q2)) / 0.2e1 + ((cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) 
	  + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2))) * Ix2 * ((cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) + (sin(q1) * sin(q2) * sin(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) 
	  - (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp1) * sin(q2) * cos(alp2)) / 0.2e1 + ((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) 
	  + (sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2))) * Iy2 * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * (cos(q1) * cos(q2) * sin(alp2) 
	  - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) + (sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) 
	  + pow(sin(alp1), 0.2e1) * pow(sin(q2), 0.2e1) * sin(alp2)) / 0.2e1 + ((-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * (-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * (cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2))) * Iz2 * ((-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * (-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * (-sin(q1) * sin(q2) 
	  + cos(q1) * cos(alp1) * cos(q2)) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(alp1) * cos(q2)) / 0.2e1 + m3 * (((-sin(q1) * cos(q2) 
	  - cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * xCi3 
	  + (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * yCi3 + ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * zCi3 + (-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) + cos(q1) * sin(alp1) * d2 - a1 * sin(q1)) * (((-cos(q1) * sin(q2) 
	  - sin(q1) * cos(alp1) * cos(q2)) * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) * xCi3 + (-(-cos(q1) * sin(q2) 
	  - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) 
	  + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) * yCi3 + ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) 
	  - (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) 
	  + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) * zCi3 + (-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 
	  - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) / 0.2e1 + m3 * (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * xCi3 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * yCi3 + ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) 
	  - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * zCi3 + (cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * d3 + cos(q1) * a2 * cos(q2) - sin(q1) * cos(alp1) * a2 * sin(q2) + sin(q1) * sin(alp1) * d2 
	  + a1 * cos(q1)) * (((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * cos(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) * xCi3 
	  + (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) 
	  + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) * yCi3 + ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) 
	  - (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) 
	  + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) * zCi3 + (-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) 
	  + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 
	  - sin(q1) * a2 * sin(q2) + cos(q1) * cos(alp1) * a2 * cos(q2)) / 0.2e1 + (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) 
	  + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3))) * Ix3 * (((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * cos(q2) * cos(alp2) 
	  + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) 
	  + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * cos(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) 
	  + (sin(alp1) * sin(q2) * sin(q3) * sin(alp3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) 
	  + cos(alp1) * cos(alp2)) * cos(alp3)) * (-sin(alp1) * cos(q2) * sin(q3) * cos(alp3) - sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * cos(alp3) 
	  + sin(alp1) * sin(q2) * sin(alp2) * sin(alp3))) / 0.2e1 + (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-sin(q1) * cos(q2) 
	  - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) 
	  - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3))) * Iy3 * (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-cos(q1) * sin(q2) 
	  - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) 
	  + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3)
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-sin(q1) * sin(q2) 
	  + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) 
	  + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) + (sin(alp1) * sin(q2) * cos(q3) + (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * sin(q3)) * (sin(alp1) * cos(q2) * sin(q3) * sin(alp3) 
	  + sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * sin(alp3) + sin(alp1) * sin(q2) * sin(alp2) * cos(alp3))) / 0.2e1 
	  + ((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3))) * Iz3 * ((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * cos(q3) 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * cos(q3) 
	  + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) + (-sin(alp1) * sin(q2) * sin(q3) * cos(alp3) + (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp3)) * (sin(alp1) * cos(q2) * cos(q3) 
	  - sin(alp1) * sin(q2) * cos(alp2) * sin(q3))) / 0.2e1;
  A[0][2] = m3 * (((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * xCi3 + (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * yCi3 + ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * zCi3 + (-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + cos(q1) * sin(alp1) * cos(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) + cos(q1) * sin(alp1) * d2 - a1 * sin(q1)) * ((-(cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3)) * xCi3 
	  + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - (cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) / 0.2e1 + m3 * (((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * xCi3 
	  + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * yCi3 + ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * zCi3 + (cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * d3 + cos(q1) * a2 * cos(q2) - sin(q1) * cos(alp1) * a2 * sin(q2) + sin(q1) * sin(alp1) * d2 + a1 * cos(q1)) * ((-(sin(q1) * cos(q2) 
	  + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3)) * xCi3 
	  + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - (sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) / 0.2e1 + (((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) 
	  + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-sin(q1) * cos(q2) 
	  - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) 
	  + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) 
	  + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3))) * Ix3 * (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) 
	  - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) 
	  - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) + ((sin(q1) * cos(q2) 
	  + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) 
	  + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(sin(q1) * cos(q2) 
	  + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) + (sin(alp1) * sin(q2) * sin(q3) * sin(alp3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * sin(alp3) 
	  + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * cos(alp3)) * (-sin(alp1) * sin(q2) * cos(q3) * cos(alp3) - (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * sin(q3) * cos(alp3))) / 0.2e1 + (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) 
	  - (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) 
	  + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3))) * Iy3 * (((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) 
	  + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) + (sin(alp1) * sin(q2) * cos(q3) + (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * sin(q3)) * (sin(alp1) * sin(q2) * cos(q3) * sin(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * sin(alp3))) / 0.2e1 
	  + ((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3))) * Iz3 * ((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3)) + (-sin(alp1) * sin(q2) * sin(q3) * cos(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * cos(alp3) 
	  + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp3)) * (-sin(alp1) * sin(q2) * sin(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3))) / 0.2e1;
  A[1][0] = A[0][1];
  A[1][1] = m2 * pow((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * xCi2 + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * yCi2 
	  + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * zCi2 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2), 0.2e1) / 0.2e1 
	  + m2 * pow((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * xCi2 + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * yCi2 + (sin(q1) * cos(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * zCi2 - sin(q1) * a2 * sin(q2) + cos(q1) * cos(alp1) * a2 * cos(q2), 0.2e1) / 0.2e1 + m2 * pow(sin(alp1) * cos(q2) * xCi2 
	  - sin(alp1) * sin(q2) * cos(alp2) * yCi2 + sin(alp1) * sin(q2) * sin(alp2) * zCi2 + sin(alp1) * a2 * cos(q2), 0.2e1) / 0.2e1 + pow((cos(q1) * sin(q2) * sin(alp2) 
	  + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) + (sin(q1) * sin(q2) * sin(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) 
	  - (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp1) * sin(q2) * cos(alp2), 0.2e1) * Ix2 / 0.2e1 + pow((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) + (sin(q1) * cos(q2) 
	  + cos(q1) * cos(alp1) * sin(q2)) * (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) + pow(sin(alp1), 0.2e1) * pow(sin(q2), 0.2e1) * sin(alp2), 0.2e1) * Iy2 / 0.2e1 
	  + pow((-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * (-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * (-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) 
	  + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(alp1) * cos(q2), 0.2e1) * Iz2 / 0.2e1 + m3 * pow(((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * cos(q3) 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) * xCi3 + (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * cos(q2) * sin(alp2) 
	  - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) * yCi3 + ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * cos(q2) * cos(alp2) 
	  + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) * zCi3 + (-cos(q1) * sin(q2) 
	  - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) 
	  - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2), 0.2e1) / 0.2e1 + m3 * pow(((-sin(q1) * sin(q2) 
	  + cos(q1) * cos(alp1) * cos(q2)) * cos(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) * xCi3 + (-(-sin(q1) * sin(q2) 
	  + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * cos(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) * yCi3 + ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * cos(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) * zCi3 
	  + (-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) 
	  + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - sin(q1) * a2 * sin(q2) + cos(q1) * cos(alp1) * a2 * cos(q2), 0.2e1) / 0.2e1 
	  + m3 * pow((sin(alp1) * cos(q2) * cos(q3) - sin(alp1) * sin(q2) * cos(alp2) * sin(q3)) * xCi3 + (-sin(alp1) * cos(q2) * sin(q3) * cos(alp3) 
	  - sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * cos(alp3) + sin(alp1) * sin(q2) * sin(alp2) * sin(alp3)) * yCi3 + (sin(alp1) * cos(q2) * sin(q3) * sin(alp3) 
	  + sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * sin(alp3) + sin(alp1) * sin(q2) * sin(alp2) * cos(alp3)) * zCi3 + sin(alp1) * cos(q2) * a3 * cos(q3) 
	  - sin(alp1) * sin(q2) * cos(alp2) * a3 * sin(q3) + sin(alp1) * sin(q2) * sin(alp2) * d3 + sin(alp1) * a2 * cos(q2), 0.2e1) / 0.2e1 + pow(((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) 
	  + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-cos(q1) * sin(q2) 
	  - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * cos(q2) * sin(alp2) 
	  - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) 
	  + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) 
	  + (sin(alp1) * sin(q2) * sin(q3) * sin(alp3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) 
	  + cos(alp1) * cos(alp2)) * cos(alp3)) * (-sin(alp1) * cos(q2) * sin(q3) * cos(alp3) - sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * cos(alp3) 
	  + sin(alp1) * sin(q2) * sin(alp2) * sin(alp3)), 0.2e1) * Ix3 / 0.2e1 + pow(((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) 
	  - (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) 
	  + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * cos(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) 
	  + (sin(alp1) * sin(q2) * cos(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3)) * (sin(alp1) * cos(q2) * sin(q3) * sin(alp3) 
	  + sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * sin(alp3) + sin(alp1) * sin(q2) * sin(alp2) * cos(alp3)), 0.2e1) * Iy3 / 0.2e1 + pow((-(cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * cos(q3) 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * cos(q3) 
	  + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) + (-sin(alp1) * sin(q2) * sin(q3) * cos(alp3) + (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp3)) * (sin(alp1) * cos(q2) * cos(q3) 
	  - sin(alp1) * sin(q2) * cos(alp2) * sin(q3)), 0.2e1) * Iz3 / 0.2e1;
  A[1][2] = m3 * (((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) * xCi3 
	  + (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) 
	  + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) * yCi3 + ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) 
	  - (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * cos(q2) * sin(alp2) 
	  - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) * zCi3 + (-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 
	  - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) * ((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3)) * xCi3 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) 
	  - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + ((cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - (cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) / 0.2e1 
	  + m3 * (((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * cos(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) * xCi3 
	  + (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) 
	  + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) * yCi3 + ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) 
	  - (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * cos(q2) * sin(alp2) 
	  + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) * zCi3 + (-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) 
	  + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 
	  - sin(q1) * a2 * sin(q2) + cos(q1) * cos(alp1) * a2 * cos(q2)) * ((-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3)) * xCi3 + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) 
	  - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + ((sin(q1) * cos(q2) 
	  + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - (sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) 
	  + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) / 0.2e1 + m3 * ((sin(alp1) * cos(q2) * cos(q3) 
	  - sin(alp1) * sin(q2) * cos(alp2) * sin(q3)) * xCi3 + (-sin(alp1) * cos(q2) * sin(q3) * cos(alp3) - sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * cos(alp3) 
	  + sin(alp1) * sin(q2) * sin(alp2) * sin(alp3)) * yCi3 + (sin(alp1) * cos(q2) * sin(q3) * sin(alp3) + sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * sin(alp3) 
	  + sin(alp1) * sin(q2) * sin(alp2) * cos(alp3)) * zCi3 + sin(alp1) * cos(q2) * a3 * cos(q3) - sin(alp1) * sin(q2) * cos(alp2) * a3 * sin(q3) + sin(alp1) * sin(q2) * sin(alp2) * d3 
	  + sin(alp1) * a2 * cos(q2)) * ((-sin(alp1) * sin(q2) * sin(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3)) * xCi3 + (-sin(alp1) * sin(q2) * cos(q3) * cos(alp3) 
	  - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + (sin(alp1) * sin(q2) * cos(q3) * sin(alp3) + (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - sin(alp1) * sin(q2) * a3 * sin(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * a3 * cos(q3)) / 0.2e1 
	  + (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * cos(q2) * cos(alp2) 
	  + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) 
	  + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * cos(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * sin(alp3)) 
	  + (sin(alp1) * sin(q2) * sin(q3) * sin(alp3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) 
	  + cos(alp1) * cos(alp2)) * cos(alp3)) * (-sin(alp1) * cos(q2) * sin(q3) * cos(alp3) - sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * cos(alp3) 
	  + sin(alp1) * sin(q2) * sin(alp2) * sin(alp3))) * Ix3 * (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) 
	  - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) 
	  - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) + (sin(alp1) * sin(q2) * sin(q3) * sin(alp3) 
	  - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) 
	  + cos(alp1) * cos(alp2)) * cos(alp3)) * (-sin(alp1) * sin(q2) * cos(q3) * cos(alp3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * cos(alp3))) / 0.2e1 
	  + (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * cos(q2) * cos(alp2) 
	  + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) 
	  + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * cos(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * cos(q2) * sin(alp2) + cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * cos(alp3)) 
	  + (sin(alp1) * sin(q2) * cos(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3)) * (sin(alp1) * cos(q2) * sin(q3) * sin(alp3) 
	  + sin(alp1) * sin(q2) * cos(alp2) * cos(q3) * sin(alp3) + sin(alp1) * sin(q2) * sin(alp2) * cos(alp3))) * Iy3 * (((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) + ((sin(q1) * cos(q2) 
	  + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) + (sin(alp1) * sin(q2) * cos(q3) + (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * sin(q3)) * (sin(alp1) * sin(q2) * cos(q3) * sin(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * sin(alp3))) / 0.2e1 
	  + ((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * cos(q3) 
	  + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) 
	  - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * ((-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * cos(q3) + (-sin(q1) * cos(q2) * cos(alp2) 
	  - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * sin(q3)) + (-sin(alp1) * sin(q2) * sin(q3) * cos(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp3)) * (sin(alp1) * cos(q2) * cos(q3) - sin(alp1) * sin(q2) * cos(alp2) * sin(q3))) * Iz3 * ((-(cos(q1) * cos(q2) 
	  - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) 
	  + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) 
	  + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * (-(sin(q1) * cos(q2) 
	  + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3)) 
	  + (-sin(alp1) * sin(q2) * sin(q3) * cos(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp3)) * (-sin(alp1) * sin(q2) * sin(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3))) / 0.2e1;
  A[2][0] = A[0][2];
  A[2][1] = A[1][2];
  A[2][2] = m3 * pow((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3)) * xCi3 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-cos(q1) * sin(q2) * cos(alp2) 
	  - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - (cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3), 0.2e1) / 0.2e1 + m3 * pow((-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3)) * xCi3 + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - (sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) 
	  - cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3), 0.2e1) / 0.2e1 + m3 * pow((-sin(alp1) * sin(q2) * sin(q3) 
	  + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3)) * xCi3 + (-sin(alp1) * sin(q2) * cos(q3) * cos(alp3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) * yCi3 + (sin(alp1) * sin(q2) * cos(q3) * sin(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) * zCi3 - sin(alp1) * sin(q2) * a3 * sin(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * a3 * cos(q3), 0.2e1) / 0.2e1 + pow(((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * sin(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * cos(alp3)) * (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * cos(alp3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * cos(alp3)) + (sin(alp1) * sin(q2) * sin(q3) * sin(alp3) - (sin(alp1) * cos(q2) * cos(alp2) 
	  + cos(alp1) * sin(alp2)) * cos(q3) * sin(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * cos(alp3)) * (-sin(alp1) * sin(q2) * cos(q3) * cos(alp3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * cos(alp3)), 0.2e1) * Ix3 / 0.2e1 + pow(((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) + ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3)) * ((sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * cos(q3) * sin(alp3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * sin(q3) * sin(alp3)) + (sin(alp1) * sin(q2) * cos(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3)) * (sin(alp1) * sin(q2) * cos(q3) * sin(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * sin(q3) * sin(alp3)), 0.2e1) * Iy3 / 0.2e1 + pow((-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (cos(q1) * sin(q2) * sin(alp2) + sin(q1) * cos(alp1) * cos(q2) * sin(alp2) + sin(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * cos(q3)) + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) * cos(alp3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (sin(q1) * sin(q2) * sin(alp2) - cos(q1) * cos(alp1) * cos(q2) * sin(alp2) - cos(q1) * sin(alp1) * cos(alp2)) * sin(alp3)) * (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * sin(q3) + (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * cos(q3)) + (-sin(alp1) * sin(q2) * sin(q3) * cos(alp3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3) * cos(alp3) + (-sin(alp1) * cos(q2) * sin(alp2) + cos(alp1) * cos(alp2)) * sin(alp3)) * (-sin(alp1) * sin(q2) * sin(q3) + (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * cos(q3)), 0.2e1) * Iz3 / 0.2e1;
  cgret[0][0] = A[0][0];
  cgret[0][1] = A[0][1];
  cgret[0][2] = A[0][2];
  cgret[1][0] = A[1][0];
  cgret[1][1] = A[1][1];
  cgret[1][2] = A[1][2];
  cgret[2][0] = A[2][0];
  cgret[2][1] = A[2][1];
  cgret[2][2] = A[2][2];
}

void C3dofs::Jacobi_RRR (  double q1,  double q2,  double q3,  double cgret[3][3])
{
  double jac[3][3];
  double t1;
  double t11;
  double t13;
  double t14;
  double t16;
  double t18;
  double t19;
  double t2;
  double t20;
  double t22;
  double t24;
  double t27;
  double t3;
  double t32;
  double t34;
  double t38;
  double t39;
  double t4;
  double t44;
  double t46;
  double t5;
  double t52;
  double t56;
  double t58;
  double t6;
  double t62;
  double t66;
  double t69;
  double t7;
  double t9;
  double t103;
  double t106;
  jac[0][0] = 0;  jac[0][1] = 0;  jac[0][2] = 0;
  jac[1][0] = 0;  jac[1][1] = 0;  jac[1][2] = 0;
  jac[2][0] = 0;  jac[2][1] = 0;  jac[2][2] = 0;
  t1 = sin(q1);
  t2 = cos(q2);
  t3 = t1 * t2;
  t4 = cos(q1);
  t5 = cos(alp1);
  t6 = t4 * t5;
  t7 = sin(q2);
  t9 = -t3 - t6 * t7;
  t11 = cos(q3);
  t13 = t1 * t7;
  t14 = cos(alp2);
  t16 = t2 * t14;
  t18 = sin(alp1);
  t19 = t4 * t18;
  t20 = sin(alp2);
  t22 = t13 * t14 - t6 * t16 + t19 * t20;
  t24 = sin(q3);
  t27 = t2 * t20;
  t32 = t1 * a2;
  t34 = a2 * t7;
  jac[0][0] = t9 * a3 * t11 + t22 * a3 * t24 + (-t13 * t20 + t6 * t27 + t19 * t14) * d3 - t32 * t2 - t6 * t34 + t19 * d2 - a1 * t1;
  t38 = t4 * t7;
  t39 = t1 * t5;
  t44 = t4 * t2;
  t46 = t7 * t14;
  t52 = t7 * t20;
  t56 = t4 * a2;
  t58 = t2 * a2;
  jac[0][1] = (-t38 - t39 * t2) * a3 * t11 + (-t44 * t14 + t39 * t46) * a3 * t24 + (t44 * t20 - t39 * t52) * d3 - t56 * t7 - t39 * t58;
  t62 = (t44 - t39 * t7) * a3;
  t66 = t1 * t18;
  t69 = (-t38 * t14 - t39 * t16 + t66 * t20) * a3;
  jac[0][2] = -t62 * t24 + t69 * t11;
  jac[1][0] = t62 * t11 + t69 * t24 + (t38 * t20 + t39 * t27 + t66 * t14) * d3 + t56 * t2 - t39 * t34 + t66 * d2 + a1 * t4;
  jac[1][1] = (-t13 + t6 * t2) * a3 * t11 + (-t3 * t14 - t6 * t46) * a3 * t24 + (t3 * t20 + t6 * t52) * d3 - t32 * t7 + t6 * t58;
  jac[1][2] = t9 * a3 * t24 - t22 * a3 * t11;
  jac[2][0] = 0.0e0;
  t103 = t18 * t2;
  t106 = t18 * t7;
  jac[2][1] = t103 * a3 * t11 - t106 * t14 * a3 * t24 + t106 * t20 * d3 + t18 * a2 * t2;
  jac[2][2] = -t106 * a3 * t24 + (t103 * t14 + t5 * t20) * a3 * t11;
  cgret[0][0] = jac[0][0];
  cgret[0][1] = jac[0][1];
  cgret[0][2] = jac[0][2];
  cgret[1][0] = jac[1][0];
  cgret[1][1] = jac[1][1];
  cgret[1][2] = jac[1][2];
  cgret[2][0] = jac[2][0];
  cgret[2][1] = jac[2][1];
  cgret[2][2] = jac[2][2];
}

// dao ham ma tran Jacobi theo thoi gian
void C3dofs::diff_Jacobi_RRR (double q1, double q2, double q3, double dq1, double dq2, double dq3, double cgret[3][3])
{
  double A[3][3];
  A[0][0] = 0;  A[0][1] = 0;  A[0][2] = 0;
  A[1][0] = 0;  A[1][1] = 0;  A[1][2] = 0;
  A[2][0] = 0;  A[2][1] = 0;  A[2][2] = 0;
  A[0][0] = ((-cos(q1) * cos(q2) + sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (cos(q1) * sin(q2) * cos(alp2) + sin(q1) * cos(alp1) * cos(q2) * cos(alp2) - sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * sin(alp2) - sin(q1) * cos(alp1) * cos(q2) * sin(alp2) - sin(q1) * sin(alp1) * cos(alp2)) * d3 - cos(q1) * a2 * cos(q2) + sin(q1) * cos(alp1) * a2 * sin(q2) - sin(q1) * sin(alp1) * d2 - a1 * cos(q1)) * dq1 + ((sin(q1) * sin(q2) - cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (sin(q1) * cos(q2) * cos(alp2) + cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * sin(alp2) - cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 + sin(q1) * a2 * sin(q2) - cos(q1) * cos(alp1) * a2 * cos(q2)) * dq2 + (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq3;
  A[0][1] = ((sin(q1) * sin(q2) - cos(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (sin(q1) * cos(q2) * cos(alp2) + cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * sin(alp2) - cos(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 + sin(q1) * a2 * sin(q2) - cos(q1) * cos(alp1) * a2 * cos(q2)) * dq1 + ((-cos(q1) * cos(q2) + sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (cos(q1) * sin(q2) * cos(alp2) + sin(q1) * cos(alp1) * cos(q2) * cos(alp2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * sin(alp2) - sin(q1) * cos(alp1) * cos(q2) * sin(alp2)) * d3 - cos(q1) * a2 * cos(q2) + sin(q1) * cos(alp1) * a2 * sin(q2)) * dq2 + (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq3;
  A[0][2] = (-(-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq1 + (-(-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq2 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) - (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3)) * dq3;
  A[1][0] = ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2) + cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2) + cos(q1) * sin(alp1) * cos(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2) + cos(q1) * sin(alp1) * d2 - a1 * sin(q1)) * dq1 + ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) * dq2 + (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq3;
  A[1][1] = ((-cos(q1) * sin(q2) - sin(q1) * cos(alp1) * cos(q2)) * a3 * cos(q3) + (-cos(q1) * cos(q2) * cos(alp2) + sin(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * sin(q3) + (cos(q1) * cos(q2) * sin(alp2) - sin(q1) * cos(alp1) * sin(q2) * sin(alp2)) * d3 - cos(q1) * a2 * sin(q2) - sin(q1) * cos(alp1) * a2 * cos(q2)) * dq1 + ((-sin(q1) * cos(q2) - cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) + (sin(q1) * sin(q2) * cos(alp2) - cos(q1) * cos(alp1) * cos(q2) * cos(alp2)) * a3 * sin(q3) + (-sin(q1) * sin(q2) * sin(alp2) + cos(q1) * cos(alp1) * cos(q2) * sin(alp2)) * d3 - sin(q1) * a2 * cos(q2) - cos(q1) * cos(alp1) * a2 * sin(q2)) * dq2 + (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq3;
  A[1][2] = (-(cos(q1) * cos(q2) - sin(q1) * cos(alp1) * sin(q2)) * a3 * sin(q3) + (-cos(q1) * sin(q2) * cos(alp2) - sin(q1) * cos(alp1) * cos(q2) * cos(alp2) + sin(q1) * sin(alp1) * sin(alp2)) * a3 * cos(q3)) * dq1 + (-(-sin(q1) * sin(q2) + cos(q1) * cos(alp1) * cos(q2)) * a3 * sin(q3) + (-sin(q1) * cos(q2) * cos(alp2) - cos(q1) * cos(alp1) * sin(q2) * cos(alp2)) * a3 * cos(q3)) * dq2 + (-(sin(q1) * cos(q2) + cos(q1) * cos(alp1) * sin(q2)) * a3 * cos(q3) - (-sin(q1) * sin(q2) * cos(alp2) + cos(q1) * cos(alp1) * cos(q2) * cos(alp2) - cos(q1) * sin(alp1) * sin(alp2)) * a3 * sin(q3)) * dq3;
  A[2][0] = 0.0e0;
  A[2][1] = (-sin(alp1) * sin(q2) * a3 * cos(q3) - sin(alp1) * cos(q2) * cos(alp2) * a3 * sin(q3) + sin(alp1) * cos(q2) * sin(alp2) * d3 - sin(alp1) * a2 * sin(q2)) * dq2 + (-sin(alp1) * cos(q2) * a3 * sin(q3) - sin(alp1) * sin(q2) * cos(alp2) * a3 * cos(q3)) * dq3;
  A[2][2] = (-sin(alp1) * cos(q2) * a3 * sin(q3) - sin(alp1) * sin(q2) * cos(alp2) * a3 * cos(q3)) * dq2 + (-sin(alp1) * sin(q2) * a3 * cos(q3) - (sin(alp1) * cos(q2) * cos(alp2) + cos(alp1) * sin(alp2)) * a3 * sin(q3)) * dq3;
  cgret[0][0] = A[0][0];
  cgret[0][1] = A[0][1];
  cgret[0][2] = A[0][2];
  cgret[1][0] = A[1][0];
  cgret[1][1] = A[1][1];
  cgret[1][2] = A[1][2];
  cgret[2][0] = A[2][0];
  cgret[2][1] = A[2][1];
  cgret[2][2] = A[2][2];
}
/*==========================================================================================================*/
/*********** End ***********/
/*==========================================================================================================*/


// doc du lieu tu file, luu vao MatrixXd m_Input
bool C3dofs::ReadData(CString fileName, int LoaiBaiToan)
{
	std::ifstream input(fileName, std::ios::in);
	if (!input.is_open())			// open fail
	{
		input.close();
		return false;
	}
	int rowNum, colNum;
	double tmp;
	input >> rowNum;	
	if (input.fail())		{input.close();	return false;}
	input >> colNum;
	if (input.fail())		{input.close(); return false;}
	m_Input = MatrixXd(rowNum, colNum);
	for (int r = 0; r < rowNum; r++)
	{
		for (int c = 0; c < colNum; c++)
		{
			input>>tmp;
			if (input.fail())	{input.close();	return false;}
			m_Input[r][c] = tmp;
		}
	}
	// doc du lieu dieu kien dau
	if (LoaiBaiToan == DHN)
	{
		m_Initial = VectorXd(3);		// 3 khau
		double tmp;
		for (int i = 0; i < 3; i++)
		{
			if (input.eof()) {input.close();return false;}
			input >> tmp;	 
			if (input.fail()) {input.close();return false;}
			m_Initial[i] = tmp;
		}
	}
	if (LoaiBaiToan == DLHT)
	{
		m_Initial = VectorXd(6);		// q0, dq0
		double tmp;
		for (int i = 0; i < 6; i++)
		{
			if (input.eof()) {input.close();return false;}
			input >> tmp;	 
			if (input.fail()) {input.close();return false;}
			m_Initial[i] = tmp;
		}
	}
	input.close();
	return true;
}


// giai bai toan dong hoc thuan
bool C3dofs::dht(void)
{
	double ti, xE, yE, zE, vEx, vEy, vEz, aEx, aEy, aEz;
	VectorXd QQ;
	if (m_bConfig[0] && m_bConfig[1] && m_bConfig[2])	// RRR
	{
		// ti, xE, yE, zE, vEx, vEy, vEz, aEx, aEy, aEz, QQ[0], QQ[1], QQ[2];
		m_Output = MatrixXd(m_Input.rows(), 13);
		for (int i = 0; i < (int) m_Input.rows(); i++)
		{
			m_Output[i][0] = ti = m_Input[i][0];
			m_Output[i][1] = xE = xE_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3]);
			m_Output[i][2] = yE = yE_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3]);
			m_Output[i][3] = zE = zE_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3]);
			m_Output[i][4] = vEx = vEx_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3], m_Input[i][4], m_Input[i][5], m_Input[i][6]);
			m_Output[i][5] = vEy = vEy_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3], m_Input[i][4], m_Input[i][5], m_Input[i][6]);
			m_Output[i][6] = vEz = vEz_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3], m_Input[i][4], m_Input[i][5], m_Input[i][6]);
			m_Output[i][7] = aEx = aEx_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3], m_Input[i][4], m_Input[i][5], m_Input[i][6], m_Input[i][7], m_Input[i][8], m_Input[i][9]);
			m_Output[i][8] = aEy = aEy_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3], m_Input[i][4], m_Input[i][5], m_Input[i][6], m_Input[i][7], m_Input[i][8], m_Input[i][9]);
			m_Output[i][9] = aEz = aEz_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3], m_Input[i][4], m_Input[i][5], m_Input[i][6], m_Input[i][7], m_Input[i][8], m_Input[i][9]);
			QQ = QQE_RRR(m_Input[i][1], m_Input[i][2], m_Input[i][3], m_Input[i][4], m_Input[i][5], m_Input[i][6], m_Input[i][7], m_Input[i][8], m_Input[i][9]);
			m_Output[i][10] = QQ[0];
			m_Output[i][11] = QQ[1];
			m_Output[i][12] = QQ[2];
		}
	}
	return true;
}


// giai bai toan dong hoc nguoc
bool C3dofs::dhn(void)
{
	// dau vao ti, xE, yE, zE, vEx, vEy, vEz, aEx, aEy, aEz = 10
	// dau ra ti, q1, q2, q3, dq1, dq2, dq3, ddq1, ddq2, ddq3, Q[1], Q[2], Q[3] = 13
	if (m_bConfig[0] && m_bConfig[1] && m_bConfig[2])	// RRR
	{
		//m_Output = MatrixXd(m_Input.rows(), 13);
		m_Output = MatrixXd(m_Input.rows(), 13, 0.0);
		VectorXd tmp = m_Initial, fE(3), x0;
		// dong hoc vi phan
		MatrixXd JacMat, difJacMat, invJacMat;
		VectorXd vE(3), aE(3), dq, ddq, QE;

		for (int i = 0; i < m_Input.rows(); i++)
		{			
			fE[0] = m_Input[i][1];		// xE
			fE[1] = m_Input[i][2];		// yE
			fE[2] = m_Input[i][3];		// zE
			x0 = tmp;
			tmp = NC::fsolve(rE_RRR, Jacobian_RRR, x0, fE);
			// kiem tra lai nghiem
			if (norm2(rE_RRR(tmp) - fE) > 1e-4)		// nghiem khong tin tuong
				return false;
			// giai bai toan dong hoc nguoc vi phan
			JacMat = Jacobian_RRR(tmp);		// tinh ma tran Jacobi J(q)
			vE[0] = m_Input[i][4];	vE[1] = m_Input[i][5];	vE[2] = m_Input[i][6];
			aE[0] = m_Input[i][7];	aE[1] = m_Input[i][8];	aE[2] = m_Input[i][9];
			invJacMat = NC::inv_gauss(JacMat);					// ma tran nghich dao cua ma tran Jacobi

			//if (invJacMat.cols() == vE.size())
			dq = invJacMat * vE;
			difJacMat = diff_Jacobian_RRR(tmp, dq);			// dao ham cua ma tran Jacobi
			ddq = invJacMat * (aE - difJacMat * (invJacMat * dq));
			// tinh QE
					
			QE = QQE_RRR(tmp[0], tmp[1], tmp[2], dq[0], dq[1], dq[2], ddq[0], ddq[1], ddq[2]);
			// them ket qua vao m_Output;
			m_Output[i][0] = m_Input[i][0];				// ti
			for (int j = 0; j < 3; j++)
			{
				m_Output[i][1 + j] = tmp[0 + j];		// q
				m_Output[i][4 + j] = dq[0 + j];			// dq
				m_Output[i][7 + j] = ddq[0 + j];		// ddq
				m_Output[i][10 + j] = QE[0 + j];		// QE
			}
		}
	}
	return true;
}


// bai toan dong luc hoc thuan, cho Luc tac dong--> giai ptvp ==> bien khop
bool C3dofs::dlht(void)
{
	if (m_bConfig[0] && m_bConfig[1] && m_bConfig[2])	// RRR
	{
		VectorXd tspan(3);
		tspan[0] = m_Input[0][0];					// t0
		tspan[1] = m_Input[2][0] - m_Input[0][0];	// h
		tspan[2] = m_Input[m_Input.rows() - 1][0];	// tf
		MatrixXd Tau;
		Tau = NC::sub_matrix(m_Input, 0, m_Input.rows() - 1, 
									  1, m_Input.cols() - 1);
		VectorXd q0, dq0;
		q0 = NC::sub_vector(m_Initial, 0, 2),
		dq0 = NC::sub_vector(m_Initial, 3, 5);
		// tinh ti, q, dq, ddq
		MatrixXd m_d_dq_ddq;
		try 
		{
			m_d_dq_ddq = NC::ode_45(ddq_RRR, M_RRR, H_RRR, Tau, tspan, q0, dq0);
		}
		catch (CErrorNumericComputing)
		{
			return false;
		}
		// tinh xE, yE, zE, vEx, vEy, vEz, aEx, aEy, aEz
		int row = m_d_dq_ddq.rows();
		int col = 19;
		m_Output = MatrixXd(row, col);
		// copy
		for (int i = 0; i < row; i++)
			for (int j = 0; j < m_d_dq_ddq.cols(); j++)
				m_Output[i][j] = m_d_dq_ddq[i][j];
		// tinh xE, vE, zE
		for (int i = 0; i < row; i++)
		{
			m_Output[i][10] = xE_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3]);		// xE
			m_Output[i][11] = yE_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3]);		// yE
			m_Output[i][12] = zE_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3]);		// zE
			m_Output[i][13] = vEx_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3], m_Output[i][4], m_Output[i][5], m_Output[i][6]);	// vEx
			m_Output[i][14] = vEx_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3], m_Output[i][4], m_Output[i][5], m_Output[i][6]);	// vEy
			m_Output[i][15] = vEx_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3], m_Output[i][4], m_Output[i][5], m_Output[i][6]);	// vEz
			m_Output[i][16] = aEx_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3], m_Output[i][4], m_Output[i][5], m_Output[i][6],m_Output[i][7], m_Output[i][8], m_Output[i][9]);	// aEx
			m_Output[i][17] = aEx_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3], m_Output[i][4], m_Output[i][5], m_Output[i][6], m_Output[i][7], m_Output[i][8], m_Output[i][9]);	// aEy
			m_Output[i][18] = aEx_RRR(m_Output[i][1], m_Output[i][2], m_Output[i][3], m_Output[i][4], m_Output[i][5], m_Output[i][6], m_Output[i][7], m_Output[i][8], m_Output[i][9]);	// aEz
		}
	}
	return true;
}
