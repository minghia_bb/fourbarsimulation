// PlotManagerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mphong.h"
#include "PlotManagerDlg.h"
#include "afxdialogex.h"

CString comboList[] = {_T("t"), 
		_T("q1"), _T("q2"), _T("q3"),
		_T("dq1"), _T("dq2"), _T("dq3"),
		_T("ddq1"), _T("ddq2"), _T("ddq3"),
		_T("xE"), _T("yE"), _T("zE"),
		_T("vEx"), _T("vEy"), _T("vEz"),
		_T("aEx"), _T("aEy"), _T("aEz"),
		_T("Q[1]"), _T("Q[2]"), _T("Q[3]")};

// CPlotManagerDlg dialog

IMPLEMENT_DYNAMIC(CPlotManagerDlg, CDialogEx)

CPlotManagerDlg::CPlotManagerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPlotManagerDlg::IDD, pParent)
{

}

CPlotManagerDlg::~CPlotManagerDlg()
{
}

void CPlotManagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_X, m_CtrlComboX);
	DDX_Control(pDX, IDC_COMBO_Y, m_CtrlComboY);
	DDX_Control(pDX, IDC_PLOT_LIST, m_CtrlPlotList);
}


BEGIN_MESSAGE_MAP(CPlotManagerDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_ADD, &CPlotManagerDlg::OnBnClickedBtnAdd)
	ON_BN_CLICKED(IDC_BTN_REMOVE, &CPlotManagerDlg::OnBnClickedBtnRemove)
	ON_BN_CLICKED(IDC_BTN_OK, &CPlotManagerDlg::OnBnClickedBtnOk)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CPlotManagerDlg::OnBnClickedBtnCancel)
END_MESSAGE_MAP()


// CPlotManagerDlg message handlers


BOOL CPlotManagerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here
	for (int i = 0; i < 22; i++)
	{
		m_CtrlComboX.AddString(comboList[i]);
		m_CtrlComboY.AddString(comboList[i]);
	}
	// set default 
	m_CtrlComboX.SetCurSel(0);
	m_CtrlComboY.SetCurSel(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CPlotManagerDlg::OnBnClickedBtnAdd()
{
	// TODO: Add your control notification handler code here
	int m_nCurSelX, m_nCurSelY;
	m_nCurSelX = m_CtrlComboX.GetCurSel();
	m_nCurSelY = m_CtrlComboY.GetCurSel();
	// kiem tra ? da ton tai trong danh sach chua?
	if (m_X.size() > 0)
		for (int i = 0; i < (int)m_X.size(); i++)
			if (((int)m_X[i] == m_nCurSelX) && ((int)m_Y[i] == m_nCurSelY))
				return;
	m_X.push_back(m_nCurSelX);
	m_Y.push_back(m_nCurSelY);
	// Update List
	m_CtrlPlotList.ResetContent();
	CString str;
	for (int i = 0; i < (int) m_X.size(); i++)
	{
		m_nCurSelX = m_X[i];
		m_nCurSelY = m_Y[i];
		str = CString("<") + comboList[m_nCurSelX] + CString(", ") + comboList[m_nCurSelY] + CString(">");
		m_CtrlPlotList.AddString(str);
	}
}


void CPlotManagerDlg::OnBnClickedBtnRemove()
{
	// TODO: Add your control notification handler code here
	int m_nCurSel = m_CtrlPlotList.GetCurSel();
	if (m_nCurSel == LB_ERR)
		return; // or MessageBox
	m_X.erase(m_X.begin() + m_nCurSel);
	m_Y.erase(m_Y.begin() + m_nCurSel);
	// Update List
	m_CtrlPlotList.ResetContent();
	CString str;
	for (int i = 0; i < (int) m_X.size(); i++)
	{
		str = CString("<") + comboList[m_X[i]] + CString(", ") + comboList[m_Y[i]] + CString(">");
		m_CtrlPlotList.AddString(str);
	}
}


void CPlotManagerDlg::OnBnClickedBtnOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}



void CPlotManagerDlg::OnBnClickedBtnCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}
