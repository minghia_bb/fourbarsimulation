/*
	(c) Le Minh Nghia KSTN CDT k54
	Co cau 4 khau
	cac ham tinh cho gia tri phi: 0<= phi <=2Pi
	CMpView::bound();
	C4khau::compute();
*/
#pragma once
#include "StlReader.h"
#include "Matrix4x4d.h"	
#include "include\numericcomputing.h"
#include <fstream>
#include "ErrorNumericComputing.h"

class C4khau
{
	double l1, l2, l3;
	static double m1, m2, m3;
	MatrixXd		m_Input;
	VectorXd		m_Initial;
public:
	int numLink;					// the number of links, include rigid body, 2 base
	CStlReader	stlFile[5];			// stl files
	GLuint displayLists[5];			// optimus display
	GLfloat color[5][4];			// color
	double phi[3];					// phi of Craig
	double a[3];					// a of Craig
public:
	C4khau(void);
	~C4khau(void);
	static double phi21(double phiv);
	static double phi22(double phiv);
	static double phi31(double phiv);
	static double phi32(double phiv);
	static double phi2(double phiv);
	static double phi3(double phiv);
	static double dq1_q1(double phiv);			// dao ham q1 theo q, voi q<n1 or q>n2
	static double dq1_q2(double phiv);			// dao ham q1 theo q, voi n1<= q <=n2
	static double dq2_q1(double phiv);			// dao ham q2 theo q, voi q<n1 or q>n2
	static double dq2_q2(double phiv);			// dao ham q2 theo q, voi n1<= q <=n2
	static double dphi2(double phiv);			// q = phiv, q1 = phi2, q2 = phi3
	static double dphi3(double phiv);			// 
	static double dq1_qq1(double phiv);			// dao ham cap 2 q1 theo q, voi q<n1 or q>n2
	static double dq1_qq2(double phiv);			// dao ham cap 2 q1 theo q, voi n1<= q <=n2
	static double dq2_qq1(double phiv);
	static double dq2_qq2(double phiv);
	static double ddphi2(double phiv);
	static double ddphi3(double phiv);
	// ptvp chuyen dong M.ddq + V(q, dq) = tau
	static double M(double phiv);					// 
	static double V(double phiv, double dphiv);		//
	// my functions define
	static MatrixXd M(VectorXd q)
	{
		MatrixXd rs(1,1);
		double q0 = q[0];
		int heso = floor(q0 / (2*PI));
		q0 = q0 - heso* 2 * PI;
		rs[0][0] = M(q0);
		return rs;
	}
	static VectorXd V(VectorXd q, VectorXd dq)
	{
		VectorXd rs(1);
		double q0 = q[0];
		int heso = floor(q0 / (2*PI));
		q0 = q0 - heso* 2 * PI;
		rs[0] = V(q0, dq[0]);
		return rs;
	}
	static VectorXd ddq(MatrixXd M, VectorXd V, VectorXd tau)
	{
		MatrixXd invM;
		invM = NC::inv_gauss(M);
		if (normH(invM) <= 1e-6)
			throw CErrorNumericComputing();
		VectorXd	ddq;
		ddq = invM * (tau - V);
		return ddq;
	}
public:
	void compute();									// dong hoc nguoc
	bool	ReadData(CString filename);
	void	make_default(double q0, double dq0, double t0 = 0.0, double h = 0.01, double tf = 5.0)
	{
		m_Initial = VectorXd(2);
		m_Initial[0] = q0; m_Initial[1] = dq0;
		int row = floor(2 * (tf - t0) / h);
		m_Input = MatrixXd(row + 1, 2);
		for (int i = 0; i < m_Input.rows(); i++)
			for (int j = 0; j < m_Input.cols(); j++)
				m_Input[i][j] = 0.0;

		m_Input[0][0] = t0;
		m_Input[2][0] = h;
		m_Input[m_Input.rows()-1][0] = tf;
	}
	double bound(double q)
	{
		int heso = floor(q / (2*PI));
		return (q - heso * 2 * PI);
	}
	VectorXd	dlht(int& flag);				// dong luc hoc thuan, tra ve gia tri phi tai cac thoi diem
};

