// DialogControl.cpp : implementation file
//

#include "stdafx.h"
#include "mphong.h"
#include "DialogControl.h"


// CDialogControl

IMPLEMENT_DYNCREATE(CDialogControl, CFormView)

CDialogControl::CDialogControl()
	: CFormView(CDialogControl::IDD)
{
	m_strFileName = _T("");
	m_fileName = _T("");
	m_nLoaiBaiToan = 0;
	m_bPlay = false;
	m_q0 = 30.0;
	m_dq0 = 0.0;
}

CDialogControl::~CDialogControl()
{
}

void CDialogControl::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_FILENAME, m_strFileName);
	DDX_Radio(pDX, IDC_RADIO_DHT, m_nLoaiBaiToan);
	DDX_Text(pDX, IDC_EDIT_DQ0, m_dq0);
	DDX_Text(pDX, IDC_EDIT_Q0, m_q0);
}

BEGIN_MESSAGE_MAP(CDialogControl, CFormView)
	ON_BN_CLICKED(IDC_BTN_BROWSER, &CDialogControl::OnBrowser)
	ON_BN_CLICKED(IDC_BTN_COMPUTE, &CDialogControl::OnCompute)
	ON_BN_CLICKED(IDC_BTN_PLAY, &CDialogControl::OnBnClickedBtnPlay)
	ON_BN_CLICKED(IDC_CHECK_TRACE, &CDialogControl::OnDrawTrace)
	ON_BN_CLICKED(IDC_RADIO_DLHT, &CDialogControl::OnBnClickedRadioDlht)
	ON_BN_CLICKED(IDC_RADIO_DHT, &CDialogControl::OnBnClickedRadioDht)
END_MESSAGE_MAP()


// CDialogControl diagnostics

#ifdef _DEBUG
void CDialogControl::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDialogControl::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CDialogControl message handlers


void CDialogControl::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	//
	OnBnClickedRadioDht();
	//
	// TODO: Add your specialized code here and/or call the base class
	UpdateData(0);
}


// get CMpView Class
CMpView* CDialogControl::GetRenderView(void)
{
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CMpView* pView = (CMpView*) pMainFrame->m_wndSplitter.GetPane(0, 1);
	return pView;
}


void CDialogControl::OnBrowser()
{
	// TODO: Add your control notification handler code here
	// get file data path
	static TCHAR BASED_CODE szFilter1[]=_T("Text Files (*.txt)|*.txt|");
	CFileDialog fdlg(TRUE, // open
		NULL, 
		NULL, 
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		szFilter1);			// filter
	if (fdlg.DoModal()==IDOK)
	{
		CString str=fdlg.GetPathName();
		GetDlgItem(IDC_FILENAME)->SetWindowTextW(str);
		m_fileName = fdlg.GetFileName();
		//
		OnBnClickedRadioDlht();
	}
}


void CDialogControl::OnCompute()
{
	// TODO: Add your control notification handler code here
	// save data from dialog
	UpdateData();
	//
	if (m_nLoaiBaiToan == 2/*DLHT*/)
	{
		GetRenderView()->GetDocument()->m_nLoaiBaiToan = 2;		// DHLT
		if (m_strFileName.GetLength() <= 0)
		{
			m_q0 = GetRenderView()->bound(m_q0);
			// error if 210, 330 degree, n1, n2
			if (abs (m_q0 - 210) < 1.5)
				m_q0 = 211;
			if (abs(m_q0 - 330) < 1.5)
				m_q0 = 329;
			GetRenderView()->GetDocument()->m_OpenGLView.m_Robot.make_default(DEGTORAD(m_q0), m_dq0);
			m_fileName = m_strFileName;
		}
		else
			if (!GetRenderView()->GetDocument()->m_OpenGLView.m_Robot.ReadData(m_strFileName))
			{
				MessageBox(_T("Error While Reading File\n") + m_strFileName, _T("File Not Correct"));
				return;
			}
		int flag;
		GetRenderView()->m_vq = GetRenderView()->GetDocument()->m_OpenGLView.m_Robot.dlht(flag);
		if (flag)
		{
			MessageBox(_T("Error While Computing"), _T("Error"));
			GetRenderView()->GetDocument()->m_nLoaiBaiToan = 0;		// DHT
			return;
		}
	}
	MessageBox(_T("Computing Successfully"), _T("Filename: ") + m_fileName);
	GetDlgItem(IDC_BTN_PLAY)->EnableWindow(1);
}


void CDialogControl::OnBnClickedBtnPlay()
{
	// TODO: Add your control notification handler code here
	m_bPlay = !m_bPlay;
	if (m_bPlay)
	{
		GetRenderView()->SetTimer(GetRenderView()->GetDocument()->m_OpenGLView.m_nTimerID,
		GetRenderView()->GetDocument()->m_OpenGLView.m_nTimerElapsed, NULL);
		GetDlgItem(IDC_BTN_PLAY)->SetWindowTextW(_T("Stop"));
	}
	else
	{
		GetRenderView()->KillTimer(GetRenderView()->GetDocument()->m_OpenGLView.m_nTimerID);
		GetDlgItem(IDC_BTN_PLAY)->SetWindowTextW(_T("Play"));
		// 10/10/2013
		GetRenderView()->m_vListPhi.clear();
	}
}


void CDialogControl::OnDrawTrace()
{
	// TODO: Add your control notification handler code here
	GetRenderView()->m_bDrawTrace = !GetRenderView()->m_bDrawTrace;
}


void CDialogControl::OnBnClickedRadioDlht()
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_BTN_BROWSER)->EnableWindow(1);
	GetDlgItem(IDC_BTN_COMPUTE)->EnableWindow(1);
	GetDlgItem(IDC_EDIT_Q0)->EnableWindow(1);
	GetDlgItem(IDC_EDIT_DQ0)->EnableWindow(1);
	//
	GetDlgItem(IDC_BTN_PLAY)->EnableWindow(0);
	//
	GetRenderView()->GetDocument()->m_nLoaiBaiToan = 2;
}


void CDialogControl::OnBnClickedRadioDht()
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_BTN_BROWSER)->EnableWindow(0);
	GetDlgItem(IDC_BTN_COMPUTE)->EnableWindow(0);
	GetDlgItem(IDC_EDIT_Q0)->EnableWindow(0);
	GetDlgItem(IDC_EDIT_DQ0)->EnableWindow(0);
	//
	GetDlgItem(IDC_BTN_PLAY)->EnableWindow(1);
	//
	GetRenderView()->GetDocument()->m_nLoaiBaiToan = 0;
}
