
// mophongDoc.cpp : implementation of the CMpDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "mphong.h"
#endif

#include "mophongDoc.h"
/* Mycode begins*/
#include "MainFrm.h"
/* Mycode ends*/
#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMpDoc

IMPLEMENT_DYNCREATE(CMpDoc, CDocument)

BEGIN_MESSAGE_MAP(CMpDoc, CDocument)
	ON_COMMAND(ID_OPTIONS_DLG, &CMpDoc::OnOptionsDlg)
	ON_COMMAND(ID_VIEW_VIEWTOP, &CMpDoc::OnViewViewtop)
	ON_COMMAND(ID_VIEW_VIEWBOTTOM, &CMpDoc::OnViewViewbottom)
	ON_COMMAND(ID_VIEW_VIEWLEFT, &CMpDoc::OnViewViewleft)
	ON_COMMAND(ID_VIEW_VIEWRIGHT, &CMpDoc::OnViewViewright)
	ON_COMMAND(ID_VIEW_VIEWFRONT, &CMpDoc::OnViewViewfront)
	ON_COMMAND(ID_VIEW_VIEWBACK, &CMpDoc::OnViewViewback)
	ON_COMMAND(ID_VIEW_AXIS, &CMpDoc::OnViewAxis)
	ON_UPDATE_COMMAND_UI(ID_VIEW_AXIS, &CMpDoc::OnUpdateViewAxis)
	ON_COMMAND(ID_VIEW_VIEWPERSPECTIVE, &CMpDoc::OnViewViewperspective)
END_MESSAGE_MAP()


// CMpDoc construction/destruction
/**/
/**/
CMpDoc::CMpDoc()
{
	// TODO: add one-time construction code here
	m_Plots = CPlots();
	m_nLoaiBaiToan = 0;
}

CMpDoc::~CMpDoc()
{
}

BOOL CMpDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMpDoc serialization

void CMpDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CMpDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CMpDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CMpDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CMpDoc diagnostics

#ifdef _DEBUG
void CMpDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMpDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMpDoc commands


void CMpDoc::OnOptionsDlg()
{
	// TODO: Add your command handler code here
	
	// Fucking Code Here
	//switchOpenGLContext();
}


void CMpDoc::OnViewViewtop()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Top"));
	m_OpenGLView.OnViewTop();
}


void CMpDoc::OnViewViewbottom()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Bottom"));
	m_OpenGLView.OnViewBottom();
}


void CMpDoc::OnViewViewleft()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Left"));
	m_OpenGLView.OnViewLeft();
}


void CMpDoc::OnViewViewright()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Right"));
	m_OpenGLView.OnViewRight();
}


void CMpDoc::OnViewViewfront()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Front"));
	m_OpenGLView.OnViewFront();
}


void CMpDoc::OnViewViewback()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View Back"));
	m_OpenGLView.OnViewBack();
}


void CMpDoc::OnViewAxis()
{
	// TODO: Add your command handler code here
	m_OpenGLView.m_Scene.m_bAxis = !m_OpenGLView.m_Scene.m_bAxis;
	SendMessage(m_OpenGLView.hWnd, WM_PAINT, 0, 0);
}


void CMpDoc::OnUpdateViewAxis(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_OpenGLView.m_Scene.m_bAxis);
}


void CMpDoc::OnViewViewperspective()
{
	// TODO: Add your command handler code here
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame = (CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	pStatus->SetPaneText(3, _T("View perspective"));
	m_OpenGLView.OnViewPerspective();
}


void CMpDoc::switchOpenGLContext(void)
{
	wglMakeCurrent(m_OpenGLView.hDC, m_OpenGLView.hRC);
}
