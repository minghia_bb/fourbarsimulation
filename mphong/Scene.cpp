#include "StdAfx.h"
#include "Scene.h"


CScene::CScene(void)
{
	m_v3dColorBgrd = Vec3D(0.0, 0.5, 0.7);
	m_dAlpha = 1.0;
	m_v3dEye = Vec3D(0.0, 0.0, 1000.0);
	m_v3dCenter = Vec3D(0.0, 0.0, 0.0);
	m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	m_dFovy = 45.0;
	m_dNear = 0.1;
	m_dFar = 10000.0;
	m_bBlend = true;			// as default
	m_bLight = true;			// as default
	m_bPerspective = true;
	m_bAxis = true;
}


CScene::~CScene(void)
{
}
