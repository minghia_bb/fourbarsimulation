/*******************************************************************************/
/* Le Minh Nghia KSTN CDT 54 *17/09/2013* **************************************/
/* Class COpenGL - khoi tao moi truong OpenGL trong Lop CView*******************/
/*******************************************************************************/
// khoi tao moi truong OpenGL trong Lop CView
// Trong Lop CView
// Them doi tuong COpenGL myOpenGL trong lop CMpDoc;
// Them cs.style |= WS_CLIPSIBLINGS | WS_CLIPCHILDREN; vao phuong thuc CView::PreCreateWindow
// xu li message WM_CREATE
// myOpenGL.hWnd = m_hWnd;
// myOpenGL.OnCreate(::GetDC(m_hWnd));
// xu li message WM_DESTROY
// myOpenGL.OnDestroy();
// xu li message WM_ERASEBACKGROUND
// return TRUE;
// xu li message WM_PAINT /* Ve cac doi tuong here */
// xu li meaage WM_SIZE 

#pragma once
#include <GL\glut.h>

class COpenGL
{
public:
	HWND	hWnd;
	HDC		hDC;
	HGLRC	hRC;
	int m_GLPixelIndex;
	//
	int m_nWidth, m_nHeight;		// kich thuoc cua so
	//
public:
	COpenGL(void);

	~COpenGL(void);
	// setup pixel
	BOOL SetupPixelFormat(void);
	BOOL CreateViewGLContext(HDC hDC);
	int OnCreate(HDC _hDC);
	void OnDestroy(void);
	void ViewPort(double m_dEyex, double m_dEyey, double m_dEyez, double m_dCenterx, double m_dCentery, double m_dCenterz, double m_dUpx, double m_dUpy, double m_dUpz);
	// xO=yO=0 mean left up coner, xO=yO=1 mean right bottom coner, xO=yO=-1 middle
	// dinh huong cac truc co xu huong man hinh thuoc phan duong cua cac truc
	void UseOrthogonal(int xO = -1, int yO = -1);
	// goi ham nay di kem voi goi ham ViewPort()
	void UsePerspective(double m_dFovy = 45.0, double m_dNear = 0.001, double m_dFar = 10000.0);
};

