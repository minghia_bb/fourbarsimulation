#include "StdAfx.h"
#include "Plots.h"


CPlots::CPlots(void)
{
	m_Caption = "Plot";
	xtitle = "t";
	ytitle = "f(t)";
	bGrid = false;
	bAxes = true;
	xmax = xmin = ymax = ymin = 0.0;
	color_bgrd[0] = color_bgrd[1] = color_bgrd[2] = color_bgrd[3] = 1.0;
	// fit
	fitWindow = true;
}


CPlots::~CPlots(void)
{
}


// number of lines
int CPlots::size(void)
{
	return (int)plots.size();;
}


// dinh nghia lai phep toan []
CPlotData CPlots::operator[](int i_th)
{
	return (CPlotData)plots[i_th];
}


// copy construction
CPlots::CPlots(const CPlots& plots)
	:m_Caption(plots.m_Caption), xtitle(plots.xtitle), ytitle(plots.ytitle), plots(plots.plots)
{
	xmax = plots.xmax;	xmin = plots.xmin;
	ymax = plots.ymax;	ymin = plots.ymin;
	bGrid = plots.bGrid;
	bAxes = plots.bAxes;
	fitWindow = plots.fitWindow;
	for (int i = 0; i < 4; i++)
		color_bgrd[i] = plots.color_bgrd[i];
}


void CPlots::AddPlot(std::vector<double> X, std::vector<double> Y, std::string legend, float r, float g, float b, int line_type, double thickness)
{
	CPlotData plot;
	plot.X = X;
	plot.Y = Y;
	plot.legends = legend;
	plot.color[0] = r;	plot.color[1] = g; plot.color[2] = b; plot.color[3] = 1.0;
	plot.line_type = line_type; 
	plot.thickness = thickness;
	plot.numpoint = X.size();	// fuck code

	plots.push_back(plot);
}


void CPlots::clear(void)
{
	for (int i = 0; i < size(); i++)
		plots[i].clear();
	plots.clear();

}
