/*******************************************************************************/
/* Le Minh Nghia KSTN CDT 54 *17/09/2013* **************************************/
/* Class Scene - Luu giu cac bien khoi tao moi truong OpenGL trong lop CMpView */
/*******************************************************************************/

#pragma once
#include "Vec3D.h"

class CScene
{
public:// du lieu dau vao
	Vec3D	m_v3dColorBgrd;			// color back ground
	double	m_dAlpha;				// alpha parameter
	Vec3D	m_v3dEye;				// LookAt Eye
	Vec3D	m_v3dCenter;			// LookAt Center
	Vec3D	m_v3dUp;				// LookAt Up
	double	m_dFovy, m_dNear, m_dFar;		// gluPerspective(fovy, aspect, near, far)
	bool	m_bBlend;				// if blending or not?
	bool	m_bLight;				// if Ligting or not?
	bool	m_bPerspective;			// if viewing perspective or othorgonal ?
	bool	m_bAxis;				// ve truc toa do hay ko?
public:
	CScene(void);
	~CScene(void);
};

