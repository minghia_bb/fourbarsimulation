#include "StdAfx.h"
#include "OpenGLWnd.h"


COpenGLWnd::COpenGLWnd(void)
{
}


COpenGLWnd::~COpenGLWnd(void)
{
}


// create window view
void COpenGLWnd::CreateWindowView(CString WindowName, CRect &rect, CWnd *parent)
{
	CString ClassName
		= AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_OWNDC, NULL,
			(HBRUSH)GetStockObject(WHITE_BRUSH), NULL);
	Create(ClassName, WindowName, WS_VISIBLE, rect, parent, 0);
}

BEGIN_MESSAGE_MAP(COpenGLWnd, CWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


int COpenGLWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	hDC = ::GetDC(this->m_hWnd);
    if (SetupPixelFormat() == FALSE)
		return -1;
    if (CreateViewGLContext(hDC) == FALSE)
		return -1;
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glShadeModel (GL_SMOOTH);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	return 0;
}


void COpenGLWnd::OnDestroy()
{
	// TODO: Add your message handler code here
	COpenGL::OnDestroy();
	CWnd::OnDestroy();
}
