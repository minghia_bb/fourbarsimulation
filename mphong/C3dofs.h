#pragma once
#include "StlReader.h"
#include "Matrix4x4d.h"				//
//#include <numericcomputing.h>		// numeric computing
#include "include\numericcomputing.h"
#include "ErrorNumericComputing.h"	// error numeric computing
#include <fstream>					// for I/O file

#define DHT		0
#define DHN		1
#define	DLHT	2
#define	DLHN	3

class C3dofs
{
public:
	/*==========================================================================================================*/
	/* kich thuoc cac khau*/
	static double th1, th2, th3,
		d1, d2, d3,
		a1, a2, a3,
		alp1, alp2 , alp3;
	/* klg are not defined, it'll get default value     [m1, m2, m3]*/
	static double m1, m2, m3;
	/* gravity is not defined, it'll get default value  [g1, g2, g3]*/
	static double g1, g2, g3;				// vetor gia toc trong truong
	/* rCi... are not defined, it'll get default value  */
	static double xCi1, yCi1, zCi1;			// vi tri trong tam trong htd khau
	static double xCi2, yCi2, zCi2;
	static double xCi3, yCi3, zCi3;
	/* mtqt... are not defined, it'll get default value */
	static double			// mtqt
		Ix1, Iy1, Iz1,
		Ix2, Iy2, Iz2,
		Ix3, Iy3, Iz3;
	/* Hinh anh - mo phong */
	int numLink;					// the number of links, include rigid body
	CStlReader	stlFile[4];			// stl files
	GLuint displayLists[4];			// optimus display
	Vec3D pCenter;					// <=> center of , gluLookAt();
	int size;						// duong kinh cua hinh hop bao
	/* bang DH for drawing */
	double bDH[4][4];				// thong so DH -  for drawing
	double m_dInitial[4];			// luu gia tri initial cua bDH, dung khi reset
	GLfloat color[4][4];
	bool	m_bConfig[3];			//	cau hinh cua robot, true = ROT, false = pri
	// ket qua
	MatrixXd	m_Input;		// du lieu doc tu file,
	MatrixXd	m_Output;		// du lieu tinh toan
	VectorXd	m_Initial;
public:
	/*/==========================================================================================================
	// T-T-T * #0
	//==========================================================================================================
	double xE_TTT(double q1, double q2, double q3);
	double yE_TTT(double q1, double q2, double q3);
	double zE_TTT(double q1, double q2, double q3);
	double vEx_TTT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_TTT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_TTT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_TTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_TTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_TTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	void QQE_TTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3, double output[3]);
	//==========================================================================================================
	// R-T-T * #1
	//==========================================================================================================
	double xE_RTT(double q1, double q2, double q3);
	double yE_RTT(double q1, double q2, double q3);
	double zE_RTT(double q1, double q2, double q3);
	double vEx_RTT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_RTT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_RTT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_RTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_RTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_RTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	void QQE_RTT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3, double output[3]);
	//==========================================================================================================
	// T-R-T * #2
	//==========================================================================================================
	double xE_TRT(double q1, double q2, double q3);
	double yE_TRT(double q1, double q2, double q3);
	double zE_TRT(double q1, double q2, double q3);
	double vEx_TRT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_TRT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_TRT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_TRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_TRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_TRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	void QQE_TRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3, double output[3]);
	//==========================================================================================================
	// R-R-T * #3
	//==========================================================================================================
	double xE_RRT(double q1, double q2, double q3);
	double yE_RRT(double q1, double q2, double q3);
	double zE_RRT(double q1, double q2, double q3);
	double vEx_RRT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_RRT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_RRT(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_RRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_RRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_RRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	void QQE_RRT(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3, double output[3]);
	/==========================================================================================================
	// T-T-R * #4
	//==========================================================================================================
	double xE_TTR(double q1, double q2, double q3);
	double yE_TTR(double q1, double q2, double q3);
	double zE_TTR(double q1, double q2, double q3);
	double vEx_TTR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_TTR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_TTR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_TTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_TTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_TTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	void QQE_TTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3, double output[3]);
	//==========================================================================================================
	// R-T-R * #5
	//==========================================================================================================
	double xE_RTR(double q1, double q2, double q3);
	double yE_RTR(double q1, double q2, double q3);
	double zE_RTR(double q1, double q2, double q3);
	double vEx_RTR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_RTR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_RTR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_RTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_RTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_RTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	void QQE_RTR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3, double output[3]);
	//==========================================================================================================
	// T-R-R * #6
	//==========================================================================================================
	double xE_TRR(double q1, double q2, double q3);
	double yE_TRR(double q1, double q2, double q3);
	double zE_TRR(double q1, double q2, double q3);
	double vEx_TRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEy_TRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double vEz_TRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	double aEx_TRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEy_TRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	double aEz_TRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	void QQE_TRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3, double output[3]);
	*/
	//==========================================================================================================
	// R-R-R * #7
	//==========================================================================================================
	static double xE_RRR(double q1, double q2, double q3);
	static double yE_RRR(double q1, double q2, double q3);
	static double zE_RRR(double q1, double q2, double q3); // === > rE
	static double vEx_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	static double vEy_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	static double vEz_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3);
	static double aEx_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	static double aEy_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	static double aEz_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3);
	// dong hoc vi phan nguoc, ham xuat ra tu Maple
	static void Jacobi_RRR (  double q1,  double q2,  double q3,  double output[3][3]);	// ma tran jacobi
	static void diff_Jacobi_RRR (double q1, double q2, double q3, double dq1, double dq2, double dq3, double cgret[3][3]);	// dao ham ma tran Jacobi theo thoi gian
	// dong luc hoc M.dqq + H(q, dq) = tau(t) = QQE
	static void M_RRR (double q1, double q2, double q3, double output[3][3]);		// ma tran khoi luong
	static void H_RRR (double q1, double q2, double q3, double dq1, double dq2, double dq3, double cgret[3]);
	// my functions seft-define
	static MatrixXd Jacobian_RRR(VectorXd q)
	{
		double cgret1[3][3];
		MatrixXd result(3,3);
		Jacobi_RRR(q[0], q[1], q[2], cgret1);
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				result[i][j] = cgret1[i][j];
		return result;
	}
	static MatrixXd diff_Jacobian_RRR(VectorXd q, VectorXd dq)
	{
		double cgret1[3][3];
		MatrixXd cgret(3,3);
		diff_Jacobi_RRR(q[0], q[1], q[2], dq[0], dq[1], dq[2], cgret1);
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				cgret[i][j] = cgret1[i][j];
		return cgret;
	}
	static VectorXd rE_RRR(VectorXd q)
	{
		VectorXd result(3);
		result[0] = xE_RRR(q[0], q[1], q[2]);
		result[1] = yE_RRR(q[0], q[1], q[2]);
		result[2] = zE_RRR(q[0], q[1], q[2]);
		return result;
	}
	static MatrixXd M_RRR(double q1, double q2, double q3)
	{
		MatrixXd result(3,3);
		double M[3][3];
		M_RRR(q1, q2, q3, M);
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				result[i][j] = M[i][j];
		return result;
	}
	static VectorXd H_RRR (double q1, double q2, double q3, double dq1, double dq2, double dq3)
	{
		VectorXd result(3);
		double H[3];
		H_RRR(q1, q2, q3, dq1, dq2, dq3, H);
		for (int i = 0; i < 3; i++)
			result[i] = H[i];
		return result;
	}
	static VectorXd QQE_RRR(double q1, double q2, double q3, double dq1, double dq2, double dq3, double ddq1, double ddq2, double ddq3)
	{
		VectorXd Q(3), H(3), ddq(3);
		MatrixXd M(3,3);
		ddq[0] = ddq1; ddq[1] = ddq2; ddq[2] = ddq3;
		M = M_RRR(q1,q2,q3);
		H = H_RRR(q1,q2,q3,dq1,dq2,dq3);
		Q = M * ddq + H;
		return Q;
	}
	// NC::ode_45(ddq_RRR, M_RRR, H_RRR, Tau?, tspan, q0, dq0);
	static MatrixXd M_RRR(VectorXd q)
	{
		return M_RRR(q[0], q[1], q[2]);
	}
	static VectorXd H_RRR(VectorXd q, VectorXd dq)
	{
		return H_RRR(q[0], q[1], q[2], dq[0], dq[1], dq[2]);
	}
	static VectorXd ddq_RRR(MatrixXd M, VectorXd V, VectorXd tau)
	{
		MatrixXd invM;
		VectorXd ddq;
		invM = NC::inv_gauss(M);
		if (normH(invM) < 1e-5)			// chuan Euclide = 0
			throw CErrorNumericComputing();
		ddq = invM * (tau - V);
		return ddq;
	}
	// addition functions
	
public:
	C3dofs(void);
	~C3dofs(void);
	// doc du lieu tu file, luu vao MatrixXd m_Input
	bool ReadData(CString fileName, int LoaiBaiToan = DHT);
	// giai bai toan dong hoc thuan
	bool dht(void);
	// giai bai toan dong hoc nguoc
	bool dhn(void);
	// bai toan dong luc hoc thuan, cho Luc tac dong--> giai ptvp ==> bien khop
	bool dlht(void);
};

