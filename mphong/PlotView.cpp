#include "StdAfx.h"
#include "PlotView.h"


CPlotView::CPlotView(void)
{
	plots = CPlots();
	xO = yO = 100;		// tam O
	xpos = ypos = 10;
	scaleX = scaleY = 0.0;
	m_nResize = 0;
	bCursorInfor = false;				// duong giong he truc toa do
	// xu ly chuot
	m_bLeftDown = m_bRightDown = false;
}


CPlotView::~CPlotView(void)
{
}
BEGIN_MESSAGE_MAP(CPlotView, COpenGLWnd)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()



void CPlotView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);
	m_nHeight = cy; m_nWidth = cx;
	if (cx <=0 || cy <= 0 || nType == SIZE_MINIMIZED)
		return;
	// Set OpenGL perspective, viewport and mode
	double aspect;
	aspect = (double)m_nWidth / (double)m_nHeight;

	// Make screen-center view
	glViewport(0, 0, m_nWidth, m_nHeight);
	//
	glMatrixMode(GL_PROJECTION); glLoadIdentity();
	// y up, x right
	UseOrthogonal(0, 0);
	//
	glMatrixMode(GL_MODELVIEW);	glLoadIdentity();
	//
	//glDrawBuffer(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glFlush();
	if (m_nResize > 1)
	{	
		// 7,7, cx-1, cy-1
		MoveWindow(0, 0, cx, cy);
		// tinh toan lai goc toa do O, scaleX, scaleY
		InitialAxis();
		SendMessage(WM_PAINT, 0, 0);
	}
}


void CPlotView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call COpenGLWnd::OnPaint() for painting messages
	glClearColor(plots.color_bgrd[0], plots.color_bgrd[1], plots.color_bgrd[2], plots.color_bgrd[3]);
	// Xoa tat ca nhung gi con luu trong bo dem
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	drawPosition();
	if (plots.bAxes)
		drawAxes();
	if (plots.bGrid)
		drawGrid();
	VeDoThiHamSo();
	/*/ Double buffer
	glBegin(GL_LINES);
	glLineWidth(3.0);
	glColor3f(0.0,0.0,1.0);
	glVertex2d(-500, -500);
	glVertex2d(500, 500);
	glEnd();*/ 
	glFlush();
	SwapBuffers(dc.m_ps.hdc);
	ValidateRect(NULL);
}


void CPlotView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (m_bLeftDown)
	{
		plots.xmax -= (point.x - xpos) * scaleX;
		plots.xmin -= (point.x - xpos) * scaleX;
		plots.ymax += (point.y - ypos) * scaleY;
		plots.ymin += (point.y - ypos) * scaleY;
		xpos = point.x;
		ypos = point.y;
	}
	xpos = point.x;
	ypos = point.y;
	SendMessage(WM_PAINT, 0, 0);
	CWnd::OnMouseMove(nFlags, point);
}


BOOL CPlotView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default
	// zDelta la boi so cua 120 degree,
	// nFlags : VK_CONTROL, VK_LBUTTON, VK_RBUTTON..
	double m_dOldRatio = scaleY / scaleX;
	double m_dDeltaX = (double) zDelta / 120.0;
	plots.xmax += m_dDeltaX * scaleX;
	plots.xmin -= m_dDeltaX * scaleX;
	scaleX = (plots.xmax - plots.xmin) / m_nWidth;
	plots.xmin = (-xO * scaleX);
	plots.xmax = scaleX * m_nWidth + plots.xmin;
	// 
	double
		m_dDeltaY = (plots.ymax - plots.ymin) - m_dOldRatio * (plots.xmax - plots.xmin) * m_nHeight * 1.0/ m_nWidth;
	plots.ymax += m_dDeltaY / 2.0;
	plots.ymin -= m_dDeltaY / 2.0;
	scaleY = (plots.ymax - plots.ymin) / m_nHeight;
	plots.ymin = - yO * scaleY;
	plots.ymax = plots.ymin + scaleY * m_nHeight;

	SendMessage(WM_PAINT, 0, 0);
	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}


void CPlotView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_bLeftDown = true;
	CWnd::OnLButtonDown(nFlags, point);
}


void CPlotView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_bLeftDown = false;
	CWnd::OnLButtonUp(nFlags, point);
}


void CPlotView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_bRightDown = true;
	CWnd::OnRButtonDown(nFlags, point);
}


void CPlotView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_bRightDown = false;
	CWnd::OnRButtonUp(nFlags, point);
}




// OpenGL: draw a floating point number (for tic mark labels)
void CPlotView::drawNumber(double num)
{
	char buf[30];
    int i;
    sprintf(buf,"%.3f",num);  // chuyen 1 so thanh 1 xau
    for( i=0; i < (int)strlen(buf); ++i )
	{
		glutBitmapCharacter(FONT,buf[i]);
    }
}



// OpenGL: draws the axes tic marks and labels them
// ve so X tai vi tri atXY tren truc XY
// Ve cac gach bieu thi do chia cua cac truc toa do, cac diem quan trong hon se co do lon gach lon hon
// + Dau vao la vi tri cua diem tren toa do can ve gach "|, -", tic
// + X : = 0 truc x, = 1 la truc y
// + big - do lon cua gach xoc "|, -"
void CPlotView::drawTic(double tic, double atXY, int XY, double big)
{
	glRasterPos2d(XY == 0? atXY : xO + 10, XY == 0? yO - 20 : atXY);
	drawNumber(tic);
	glBegin(GL_LINES);
	glVertex2d( XY == 0 ? atXY : xO + big, XY == 0? yO + big : atXY);
	glVertex2d( XY == 0 ? atXY : xO - big, XY == 0? yO - big : atXY);
	glEnd();
}


/* OpenGL draws the x/y axes */
// Ve hai truc toa do x, y
// + Ve 2 duong thang lam 2 truc
// + ve diem chia
// + Viet gia tri cua cac diem chia do (drawticmark)
void CPlotView::drawAxes(void)
{
	//double tic;
    //int big;
	glColor4d(0.0, 0.0, 1.0, 1.0);
	glLineWidth(2.5);
    /* x axis */
    glPushMatrix();
    glBegin(GL_LINES);
    glVertex2d(0.0, yO);
    glVertex2d(m_nWidth, yO);
	glVertex2d(m_nWidth, yO);
	glVertex2d(m_nWidth - 13, yO + 5); 
	glVertex2d(m_nWidth, yO);
	glVertex2d(m_nWidth - 13, yO - 5); 
    glEnd();
    glPopMatrix();

    /* y axis */
    glPushMatrix();
    glBegin(GL_LINES);
    glVertex2d(xO, 0.0);
    glVertex2d(xO, m_nHeight);
	glVertex2d(xO, m_nHeight);
	glVertex2d(xO + 5, m_nHeight - 13);
	glVertex2d(xO, m_nHeight);
	glVertex2d(xO - 5, m_nHeight - 13);
    glEnd();
    glPopMatrix();

	glRasterPos2d(xO + 6, yO - 20);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, 'O');

	glRasterPos2d((m_nWidth + xO) / 2, yO - 50);
	for (int i = 0; i < (int) plots.xtitle.size(); i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, plots.xtitle[i]);
	
	int lng = 8 * plots.ytitle.size();

	glRasterPos2d(xO - 20 - lng, (m_nHeight + yO) / 2);
	for (int i = 0; i < (int) plots.ytitle.size(); i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, plots.ytitle[i]);
}


// ve vi tri cua con tro chuot
void CPlotView::drawPosition(void)
{
	char buf[50];
    int i;
	double xp = xpos - xO, 
		yp = (m_nHeight - ypos) - yO;
	double xmax = plots.xmax, xmin = plots.xmin, ymin = plots.ymin, ymax = plots.ymax;
    glPushMatrix();
	glColor3f(0.0, 0.0, 1.0);
	// ve vi tri toa do x, y 
    sprintf(buf,"[%.3f, %.3f]", xp * scaleX, yp * scaleY);
    glRasterPos2d(10, 10);
    for( i = 0; i < (int)strlen(buf); ++i ) 
	{
		glutBitmapCharacter(FONT, buf[i]);
    }
	if (bCursorInfor)
	{
		// ve duong giong diem tren cac truc toa do
		glLineWidth(0.5);
		glColor3f(0.0, 0.4, 0.4);
		glBegin(GL_LINES);
		glVertex2d(xp + xO, yp + yO);
		glVertex2d(xp + xO, 0);
		glVertex2d(xp + xO, yp + yO);
		glVertex2d(0, yp + yO);
		glEnd();
		glPopMatrix();
	}
	glPushMatrix();

	if (abs(scaleX) < 1e-10 || abs(scaleY) < 1e-10  || !plots.bAxes)
		return;
	// ve ticmark, cac diem quan trong
	int so_diem = 6; // so diem quan trong tren moi truc
	double deltaX = (xmax - xmin) / (2 * so_diem);
	double deltaY = (ymax - ymin) / (2 * so_diem);
	if (xmax > 0.0)
		for ( i = 1; i <= (int) (xmax / deltaX); i++)
			drawTic(i * deltaX, i * deltaX / scaleX + xO, 0, 3.5);
	
	if (xmin < 0.0)
		for ( i = 1; i <= (int) (-xmin / deltaX); i++)
			drawTic( -i * deltaX, -i * deltaX / scaleX + xO, 0, 3.5);
	
	if (ymax > 0.0)
		for (i = 1; i <= (int) (ymax / deltaY); i++)
			drawTic( i * ymax / so_diem, i * ymax / so_diem / scaleY + yO, 1, 3.5);

	if (ymin < 0.0)
		for (i = 1; i <= (int) (-ymin / deltaY); i++)
			drawTic(i * ymin / so_diem, i * ymin / so_diem / scaleY + yO, 1, 3.5);

	glPopMatrix();
}



void CPlotView::drawGrid(int xmark, int ymark)
{
	glColor4d(0.0, 0.0, 1.0, 1.0);
	glLineWidth(0.5);
	int xi = xO;
	glBegin(GL_LINES);
	while (xi < m_nWidth)		// x+
	{
		xi += xmark;
		glVertex2d(xi, 0);
		glVertex2d(xi, m_nHeight);
	}
	while (xi > xmark)		// x-
	{
		xi -= xmark;
		glVertex2d(xi, 0);
		glVertex2d(xi, m_nHeight);
	}
	int yi = yO;
	while (yi < m_nHeight)		// y+
	{
		yi += ymark;
		glVertex2d(0.0, yi);
		glVertex2d(m_nWidth, yi);
	}
	while (yi > ymark)		// y-
	{
		yi -= ymark;
		glVertex2d(0.0, yi);
		glVertex2d(m_nWidth, yi);
	}
	glEnd();
}




void CPlotView::VeDoThiHamSo(void)
{
	if (plots.size() <= 0)
		return;
	//
	InitialAxis();
	//
	// find maxsize in legends
	int maxlength, 
		linelength = 100;
	maxlength = plots[0].legends.size();
	for (int j = 0; j < (int)plots.size(); j++)
	{
		if ((int)plots[j].legends.size() > maxlength)
			maxlength = plots[j].legends.size();
	}
	maxlength = maxlength * 8 + 30;
	//
	double xi, yi;
	for (int i = 0; i < (int) plots.size(); i++)
	{
		glColor4fv(plots[i].color);
		glLineWidth(plots[i].thickness);
		float m_ftmpHeight = m_nHeight - 10.0 - i * 10.0;
		switch (plots[i].line_type)
		{
		case LINE_PLUS:
		case LINE_ASTERISK:
			{
				int character = '+';
				if (plots[i].line_type == LINE_ASTERISK)
					character = '*';
				for (int j = 0; j < (int) plots[i].numpoint; j++)
				{
					xi = plots[i].X[j] / scaleX + 1.0*xO;
					yi = plots[i].Y[j] / scaleY + 1.0*yO;
					glRasterPos2d(xi, yi);
					glutBitmapCharacter(FONT, character);
					// legend line
					for (int k = 0; k < linelength; k += 10)
					{
						xi = m_nWidth - linelength - maxlength + k;
						yi = m_ftmpHeight;
						glRasterPos2d(xi, yi);
						glutBitmapCharacter(FONT, '+');
					}
				}
				break;
			}
		case LINE_DASH:
		case LINE_DOT:
		case LINE_DASH_DOT:
			{
				glEnable(GL_LINE_STIPPLE);
				if (plots[i].line_type == LINE_DASH)
					glLineStipple(1, 0x00FF); // dashed
				if (plots[i].line_type == LINE_DOT)
					glLineStipple(1, 0x0101); // dotted
				if (plots[i].line_type == LINE_DASH_DOT)
					glLineStipple(2, 0x1C47); //dash/dot/dash
				glBegin(GL_LINE_STRIP);
				for (int j = 0; j < (int) plots[i].numpoint; j++)
				{
					xi = plots[i].X[j] / scaleX + 1.0*xO;
					yi = plots[i].Y[j] / scaleY + 1.0*yO;
					glVertex2d(xi, yi);
				}			
				glEnd();
				// legend line
				drawOneLine(m_nWidth - linelength - maxlength, m_ftmpHeight, m_nWidth - maxlength, m_ftmpHeight);
				glDisable(GL_LINE_STIPPLE);
				break;
			}
		default:// continuous line as default
			{
				glBegin(GL_LINE_STRIP);
				for (int j = 0; j < (int) plots[i].numpoint; j++)
				{
					xi = plots[i].X[j] / scaleX + 1.0*xO;
					yi = plots[i].Y[j] / scaleY + 1.0*yO;
					glVertex2d(xi, yi);
				}
				glEnd();
				// legend line
				drawOneLine(m_nWidth - linelength - maxlength, m_ftmpHeight, m_nWidth - maxlength, m_ftmpHeight);
			}
		}   
	}
	// legend label
	for (int j = 0; j < (int) plots.size(); j++)
	{
		glColor4fv(plots[j].color);
		glRasterPos2d(m_nWidth - maxlength + 5, m_nHeight - 13 - j * 10);
		for (int k = 0; k < (int) plots[j].legends.size(); k++)
			glutBitmapCharacter(FONT, plots[j].legends[k]);
	}
}

// set range [xmin, xmax]
void CPlotView::InitialAxis(double min_x, double max_x)
{
	// find minmax of Y axes
	if (plots.size() <= 0)
		return;
	double minY, maxY;
	double min_tmp, max_tmp;
	for (int i = 0; i < plots.size(); i++)
	{
		max_min(plots[i].Y, min_tmp, max_tmp);
		if (i == 0)	{minY = min_tmp; maxY = max_tmp;}
		else
		{
			minY = minY > min_tmp ? min_tmp : minY;
			maxY = maxY < max_tmp ? max_tmp : maxY;
		}
	}
	InitialAxis(min_x, max_x, minY, maxY);
}

// set range [xmin, xmax], [ymin, ymax]
void CPlotView::InitialAxis(double min_x, double max_x, double min_y, double max_y)
{
	// diable fitWindow
	plots.fitWindow = false;
	// set range
	plots.xmin = min_x;	plots.xmax = max_x;
	plots.ymin = min_y;	plots.ymax = max_y;
	// compute scaleX, scaleY, xO, yO
	InitialAxis();
}

// tinh lai scaleX, scaleY
void CPlotView::InitialAxis()
{
	if (abs(plots.xmax - plots.xmin) < 1e-3)
	{
		plots.xmax += 50.0;
		plots.xmin -= 50.0;
	}
	if (abs(plots.ymax - plots.ymin) < 1e-3)
	{
		plots.ymax += 50.0;
		plots.ymin -= 50.0;
	}
	scaleX = (plots.xmax - plots.xmin) / m_nWidth;
	scaleY = (plots.ymax - plots.ymin) / m_nHeight;
	xO = (int)(-plots.xmin / scaleX);
	yO = (int)(-plots.ymin / scaleY);
}

void CPlotView::max_min(std::vector<double> XY, double &minv, double &maxv)
{
	minv = XY[0];
	maxv = XY[0];
	for (int i = 1; i < (int) XY.size(); i++)
	{
		if (XY[i] > maxv)
			maxv = XY[i];
		if (XY[i] < minv)
			minv = XY[i];
	}
}

void CPlotView::FitWindow(void)
{
	if (plots.size() <= 0)
		return;
	std::vector<double> minX, minY, maxX, maxY;
	double tmpx, tmpy;
	for (int i = 0; i < (int) plots.size(); i++)
	{
		// tim min , max tren moi truc cua moi do thi ham so
		max_min(plots[i].X, tmpx, tmpy);
		minX.push_back(tmpx);
		maxX.push_back(tmpy);
		max_min(plots[i].Y, tmpx, tmpy);
		minY.push_back(tmpx);
		maxY.push_back(tmpy);
	}
	max_min(minX, plots.xmin, tmpx);
	max_min(maxX, tmpx, plots.xmax);
	max_min(minY, plots.ymin, tmpx);
	max_min(maxY, tmpx, plots.ymax);
	// tinh lai xO, yO, scaleX, scaleY
	if (plots.size() <= 1)
		InitialAxis();
	else		// good viewing volumn
	{
		double verySmallX = (plots.xmax - plots.xmin) / 100;
		double verySmallY = (plots.ymax - plots.ymin) / 100;
		InitialAxis(plots.xmin - verySmallX, plots.xmax + verySmallX, plots.ymin - verySmallY, plots.ymax + verySmallY);
	}
	//
	minX.clear(); minY.clear();
	maxX.clear(); maxY.clear();
	//
}


// methods for handling private data
std::string CPlotView::getCaption(void)
{
	return plots.m_Caption;
}


void CPlotView::SetData(const CPlots& Plots)
{
	plots = Plots;
}

CPlots CPlotView::GetData(void)
{
	return plots;
}

void CPlotView::SetBackGroundColor(COLORREF ref)
{
	plots.color_bgrd[0] = GetRValue(ref);
	plots.color_bgrd[1] = GetGValue(ref);
	plots.color_bgrd[2] = GetBValue(ref);
}


void CPlotView::SetBackGroundColor(double r, double g, double b)
{
	plots.color_bgrd[0] = r;
	plots.color_bgrd[1] = g;
	plots.color_bgrd[2] = b;
}


COLORREF CPlotView::GetBackGroundColor(void)
{
	return RGB(plots.color_bgrd[0], plots.color_bgrd[1], plots.color_bgrd[2]);
}


void CPlotView::SetDrawGridAxis(bool bGrid, bool bAxis)
{
	plots.bGrid = bGrid;
	plots.bAxes = bAxis;
}
