#include "StdAfx.h"
#include "StlReader.h"
//---------------------------------------------------------------------------
CStlReader::CStlReader(void)
//---------------------------------------------------------------------------
{
	Clear();
}

//---------------------------------------------------------------------------
CStlReader::~CStlReader(void)
//---------------------------------------------------------------------------
{
}

//copy constructor
//---------------------------------------------------------------------------
CStlReader::CStlReader(CStlReader& s)
//---------------------------------------------------------------------------
{
	*this = s;
}

//overload =
//---------------------------------------------------------------------------
CStlReader& CStlReader::operator=(const CStlReader& s) 
//---------------------------------------------------------------------------
{
	Facets.resize(s.Size());
	for (int i = 0; i<Facets.size(); i++)
		Facets[i] = s.Facets[i];

	return *this;
}

//---------------------------------------------------------------------------
bool CStlReader::Load(std::string filename, const Vec3D & offset)
//---------------------------------------------------------------------------
{
	FILE *fp;
	bool binary = false;
	fp = fopen(filename.c_str(), "r"); // 'r' doc file van ban
	if(fp == NULL) return false;

	/* Find size of file */
	fseek(fp, 0, SEEK_END);
	int file_size = ftell(fp);   // kich thuoc cua file (don vi bytes)
	int facenum;
	/* Check for binary or ASCII file */
	fseek(fp, STL_LABEL_SIZE, SEEK_SET);
	fread(&facenum, sizeof(int), 1, fp); 
	//neu la nhi phan thi facenum se la so tam giac co trong file
	//= tieu de + facenum (int 4 bytes) + (unit16 - thuoc tinh 2bytes + (3+1))*facenum

	int expected_file_size = STL_LABEL_SIZE + 4 + (sizeof(short) + 12 * sizeof(float))*facenum ;

	if(file_size ==  expected_file_size) 
		binary = true;

	unsigned char tmpbuf[128];  //128 byte

	fread(tmpbuf, sizeof(tmpbuf), 1, fp);

	for(unsigned int i = 0; i < sizeof(tmpbuf); i++)
	{
		if(tmpbuf[i] > 127)
		{
			binary = true;
			break;
		}
	}
	// Now we know if the stl file is ascii or binary.
	fclose(fp);
	bool RetVal;
	if(binary) RetVal =  LoadBinary(filename, offset);
	else RetVal = LoadAscii(filename, offset);

	return RetVal;
}

//---------------------------------------------------------------------------
bool CStlReader::LoadBinary(std::string filename, const Vec3D & offset)
//---------------------------------------------------------------------------
{
	FILE *fp;
	fp = fopen(filename.c_str(), "rb");
	if(fp == NULL) return false;

	int facenum;
	fseek(fp, STL_LABEL_SIZE, SEEK_SET);
	fread(&facenum, sizeof(int), 1, fp);

	Clear();

	// For each triangle read the normal, the three coords and a short set to zero
	float N[3];
	float P[9];
	short attr;

	for(int i=0;i<facenum;++i) {
		fread(&N,3*sizeof(float),1,fp);
		fread(&P,3*sizeof(float),3,fp);
		fread(&attr,sizeof(short),1,fp);
		AddFacet(N[0], N[1], N[2], P[0], P[1], P[2], P[3], P[4], P[5], P[6], P[7], P[8]);
	}
	fclose(fp);

	Translate(-offset);
	return true;
}

//---------------------------------------------------------------------------
bool CStlReader::LoadAscii(std::string filename, const Vec3D & offset)
//---------------------------------------------------------------------------
{
	FILE *fp;
	fp = fopen(filename.c_str(), "r");
	if(fp == NULL) return false;

	long currentPos = ftell(fp);		//dau file
	fseek(fp, 0L, SEEK_END);
	long fileLen = ftell(fp);			//cuoi file
	fseek(fp,currentPos,SEEK_SET);

	Clear();

	/* Skip the first line of the file */
	while(getc(fp) != '\n') { }

	float N[3];
	float P[9];
	int cnt=0;
	int lineCnt=0;
	int ret;
	/* Read a single facet from an ASCII .STL file */
	while(!feof(fp)){
		ret = fscanf(fp, "%*s %*s %f %f %f\n", &N[0], &N[1], &N[2]); // --> "facet normal 0 0 0"
		if(ret != 3){
			// we could be in the case of a multiple solid object, where after a endfaced instead of another facet we have to skip two lines:
			//     endloop
			//	 endfacet
			//endsolid     <- continue on ret==0 will skip this line
			//solid ascii  <- and this one.
			//   facet normal 0.000000e+000 7.700727e-001 -6.379562e-001
			lineCnt++;
			continue; 
		}
		ret=fscanf(fp, "%*s %*s"); // --> "outer loop"
		ret=fscanf(fp, "%*s %f %f %f\n", &P[0],  &P[1],  &P[2]); // --> "vertex x y z"
		if(ret!=3) return false;
		ret=fscanf(fp, "%*s %f %f %f\n", &P[3],  &P[4],  &P[5]); // --> "vertex x y z"
		if(ret!=3) return false;
		ret=fscanf(fp, "%*s %f %f %f\n", &P[6],  &P[7],  &P[8]); // --> "vertex x y z"
		if(ret!=3) return false;
		ret=fscanf(fp, "%*s"); // --> "endloop"
		ret=fscanf(fp, "%*s"); // --> "endfacet"
		lineCnt+=7;
		if(feof(fp)) break;

		AddFacet(N[0], N[1], N[2], P[0], P[1], P[2], P[3], P[4], P[5], P[6], P[7], P[8]);

	}
	fclose(fp);
	Translate(-offset);
	return true;
}

// writes ascii stl file...
//---------------------------------------------------------------------------
bool CStlReader::Save(std::string filename, bool Binary) const   
//---------------------------------------------------------------------------
{ 
	FILE *fp;

	if (Binary) fp = fopen(filename.c_str(),"wb");
	else fp = fopen(filename.c_str(),"w");

	if(fp==0) return false;
	int NumFaces = (int)Facets.size();

	if(Binary)
	{
		// Write Header
		std::string s, u;
		u = s+std::string("Hi");
		std::string tmp = ObjectName + "                                                                                                    "; 
		//char header[128]=;
		fwrite(tmp.c_str(),80,1,fp);
		// write number of facets
		fwrite(&NumFaces,1,sizeof(int),fp); 
		unsigned short attributes=0;

		for(int i=0; i<NumFaces; i++){
			float N[3] = {Facets[i].n.x, Facets[i].n.y, Facets[i].n.z};
			float P[9] = {Facets[i].v[0].x, Facets[i].v[0].y, Facets[i].v[0].z, 
				Facets[i].v[1].x, Facets[i].v[1].y, Facets[i].v[1].z, 
				Facets[i].v[2].x, Facets[i].v[2].y, Facets[i].v[2].z};
    
			// For each triangle write the normal, the three coords and a short set to zero
			fwrite(&N,3,sizeof(float),fp);
 			for(int k=0;k<3;k++){fwrite(&P[3*k],3,sizeof(float),fp);}
			fwrite(&attributes,1,sizeof(short),fp);
		}
	}
	else
	{
		fprintf(fp,"solid jdh\n");
		for(int i=0; i<NumFaces; i++)
		{
		  	// For each triangle write the normal, the three coords and a short set to zero
			fprintf(fp,"  facet normal %13e %13e %13e\n", Facets[i].n.x, Facets[i].n.y, Facets[i].n.z);
			fprintf(fp,"    outer loop\n");
			for(int k=0; k<3; k++)
			{
				fprintf(fp,"      vertex  %13e %13e %13e\n", Facets[i].v[k].x, Facets[i].v[k].y, Facets[i].v[k].z);			
			}
			fprintf(fp,"    endloop\n");
			fprintf(fp,"  endfacet\n");
		}
		fprintf(fp,"endsolid vcg\n");
	}
	fclose(fp);

	return 0;
}

//---------------------------------------------------------------------------
void CStlReader::Draw(bool bModelhNormals, bool bShaded)
//---------------------------------------------------------------------------
{
	if (bShaded) 
	{ //light
		glBegin(GL_TRIANGLES);
		for (int i=0; i<Facets.size(); i++) 
		{
			glNormal3d(Facets[i].n.x, Facets[i].n.y, Facets[i].n.z);
			for (int j=0; j<3; j++) 
			{
				glVertex3d(Facets[i].v[j].x,Facets[i].v[j].y,Facets[i].v[j].z);
			}
		}
		glEnd();
	} else { // wireframe
		for (int i=0; i<Facets.size(); i++)
		{
			glBegin(GL_LINE_LOOP);
			glNormal3d(Facets[i].n.x, Facets[i].n.y, Facets[i].n.z);
			for (int j=0; j<3; j++) 
			{
				glVertex3d(Facets[i].v[j].x,Facets[i].v[j].y,Facets[i].v[j].z);
			}
			glEnd();
		}
	}

	if (bModelhNormals)
	{
		glColor3d(1,1,0);
		glBegin(GL_LINES);
		for (int i=0; i<Facets.size(); i++) 
		{
			Vec3D c = (Facets[i].v[0] + Facets[i].v[1] + Facets[i].v[2])/3;
			Vec3D c2 = c - Facets[i].n*3;
			glVertex3d(c.x, c.y, c.z);
			glVertex3d(c2.x, c2.y, c2.z);
		}
		glEnd();
	}

}

//---------------------------------------------------------------------------
void CStlReader::ComputeBoundingBox(Vec3D &pmin, Vec3D &pmax)
//---------------------------------------------------------------------------
{
	if (Facets.size() == 0)
		return;

	pmin = pmax = Facets[0].v[0];
	
	for (int i=0; i<Facets.size(); i++)
	{
		for (int j=0; j<3; j++) {
			pmin.x = min(pmin.x, Facets[i].v[j].x);
			pmin.y = min(pmin.y, Facets[i].v[j].y);
			pmin.z = min(pmin.z, Facets[i].v[j].z);
			pmax.x = max(pmax.x, Facets[i].v[j].x);
			pmax.y = max(pmax.y, Facets[i].v[j].y);
			pmax.z = max(pmax.z, Facets[i].v[j].z);
		}
	}
}


//---------------------------------------------------------------------------
void CStlReader::Translate(const Vec3D & d)
//---------------------------------------------------------------------------
{// translate geometry
	for (int i=0; i<Facets.size(); i++) 
	{
		for (int j=0; j<3; j++) 
		{
			Facets[i].v[j] += d;
		}
	}
}

//---------------------------------------------------------------------------
void CStlReader::Scale(Vec3D s)
//---------------------------------------------------------------------------
{// scale geometry
	//check for zero scale factor
	if(s.x==0 || s.y==0 || s.z==0) return;
	for (int i=0; i<Facets.size(); i++) 
	{
		for (int j=0; j<3; j++)
		{
			Facets[i].v[j].x *= s.x;
			Facets[i].v[j].y *= s.y;
			Facets[i].v[j].z *= s.z;
		}
		Facets[i].n.x *= s.x;
		Facets[i].n.y *= s.y;
		Facets[i].n.z *= s.z;
///		Facets[i].n.Normalize();
	}
}

//---------------------------------------------------------------------------
void CStlReader::Rotate(Vec3D ax, double a)
//---------------------------------------------------------------------------
{
	for (int i=0; i<Facets.size(); i++) 
	{
		for (int j=0; j<3; j++) 
		{
			Facets[i].v[j] = Facets[i].v[j].Rot(ax, a);
		}
		Facets[i].n = Facets[i].n.Rot(ax, a);
	}
}

//---------------------------------------------------------------------------
void CStlReader::RotX(double a)
//---------------------------------------------------------------------------
{
	for (int i=0; i<Facets.size(); i++)
	{
		for (int j=0; j<3; j++) 
		{
			Facets[i].v[j].RotX(a);
		}
		Facets[i].n.RotX(a);
	}
}


//---------------------------------------------------------------------------
void CStlReader::RotY(double a)
//---------------------------------------------------------------------------
{
	for (int i=0; i<Facets.size(); i++) 
	{
		for (int j=0; j<3; j++)
		{
			Facets[i].v[j].RotY(a);
		}
		Facets[i].n.RotY(a);
	}
}


//---------------------------------------------------------------------------
void CStlReader::RotZ(double a)
//---------------------------------------------------------------------------
{
	for (int i=0; i<Facets.size(); i++)
	{
		for (int j=0; j<3; j++)
		{
			Facets[i].v[j].RotZ(a);
		}
		Facets[i].n.RotZ(a);
	}
}
