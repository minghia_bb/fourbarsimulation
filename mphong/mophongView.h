
// mophongView.h : interface of the CMpView class
//

#pragma once
#include "mophongDoc.h"

class CMpView : public CView
{
protected: // create from serialization only
	CMpView();
	DECLARE_DYNCREATE(CMpView)

// Attributes
public:
	CMpDoc* GetDocument() const;

// Operations
public:
	//COpenGLView myOpenGL;
// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CMpView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	bool	m_bDrawTrace;
	std::vector<double>			m_vListPhi;
	Vector<double>				m_vq;
	int i_th;
	double bound(double phi, bool bRad = false)
	{
		int heso;
		double mot_vong = 360.0;
		if (bRad)
			mot_vong = 2*PI;
		heso = floor(phi / mot_vong);
		return (phi - heso * mot_vong);
	}
};

#ifndef _DEBUG  // debug version in mophongView.cpp
inline CMpDoc* CMpView::GetDocument() const
   { return reinterpret_cast<CMpDoc*>(m_pDocument); }
#endif

