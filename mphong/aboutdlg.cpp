#include "stdafx.h"
#include "aboutdlg.h"
//#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(CAboutDlg, CDialogEx)

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
	m_strCaption = "About mphong";
	m_strStatic2 = "mphong, Version 2.0";
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here
	this->SetWindowTextW(m_strCaption);
	GetDlgItem(IDC_STATIC_2)->SetWindowTextW(m_strStatic2);
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


