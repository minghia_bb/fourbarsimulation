//Le Minh Nghia Kstn CDt 54;
//Class CMatrix4x4d 

#pragma once
#include <cmath>
#define PI 3.1415926535897932384626433832795
#define RADTODEG(rad) rad * 180.0 / PI
#define DEGTORAD(deg) deg * PI / 180.0
class CMatrix4x4d
{
public:
	double m[16];
public:
	// Constructions
	CMatrix4x4d(void);
	CMatrix4x4d(double m_[]) {for (int i = 0; i < 16; i++) m[i] = m_[i] ; }
	CMatrix4x4d(double m0,double m4,double m8, double m12,
		        double m1,double m5,double m9, double m13,
		        double m2,double m6,double m10,double m14,
		        double m3,double m7,double m11,double m15);
	// Destructions
	~CMatrix4x4d(void);
	//Operators
	void Identity();
	void zeros();
	CMatrix4x4d operator+(const CMatrix4x4d& mat);
	CMatrix4x4d operator-(const CMatrix4x4d& mat);
	CMatrix4x4d operator*(const CMatrix4x4d& mat);
	CMatrix4x4d operator*(const double& scalar);
	// phep gan 2 ma tran
	// CMatrix4x4d operator=(const CMatrix4x4d& mat);
	//  Methods
	void Transpose(const CMatrix4x4d& mat);
	void Translate(double x = 0, double y = 0, double z = 0);
	void Rotate_X(double XAngle = 0, bool bRad = true);
	void Rotate_Y(double YAngle = 0, bool bRad = true);
	void Rotate_Z(double ZAngle = 0, bool bRad = true);
	// Matrix Denavit-Harteberg
	void MatrixDH(double theta, double d, double a, double alpha, bool bRad = true);
	// Diff Matrix Denavit-Harteberg
	void DiffMatrixDH(double theta, double d, double a, double alpha, bool bRad = true);
	// Matrix Transform Craig - Rotx(alp) - Trans(x,a) - Rotz(theta) - Trans (z,d)
	void MatrixCraig(double alpha, double a, double theta, double d, bool bRad = true);
	// ma tran chuyen tiep giua 2 htd xac dinh bang cac goc Cardan = rotx(alp) roty(beta) rotz(gamma)
	void MatrixEuler(double psi, double theta, double phi, double X = 0.0, double Y =0.0, double Z=0.0, bool bRad = true);
	void MatrixEuler(double psi, double theta, double phi, double XYZ[3], bool bRad = true);
	void MatrixCardan(double alp, double beta, double gamma, double x = 0.0, double y =0.0, double z=0.0, bool bRad = true);
	// ma tran chuyen tiep xac dinh tu cac goc Roll-Pitch-Rall = rotz(phi) roty(theta) rotx(psi)
	void MatrixRoll_Pitch_Yall(double phi, double theta, double psi, double x = 0.0, double y = 0.0, double z= 0.0, bool bRad = true);
	// tach lay ma tran quay
	CMatrix4x4d RotateMatrix(void);
};

