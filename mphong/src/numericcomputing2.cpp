/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
	Giai hpt, tinh dinh thuc va ma tran nghich dao bang pp so
*/
#include "stdafx.h"
#include "..\include\numericcomputing.h"

#ifndef NUMERIC_COMPUTING_DEFINE
#define NUMERIC_COMPUTING_DEFINE

namespace NC
{
int pivot_gauss (MatrixXd &M, int col_th) // tin tru lon nhat
{
	int r = M.rows(), c = M.cols(), index = col_th, k_max = index;
	double max_val = abs(M[index][index]), tmp;
	// tim tru lon nhat
	for (int i = index; i < r ; i++)
	{
		if ( abs(M[i][index]) > max_val)
		{
			max_val = abs(M[i][index]);
			k_max = i;
		}
	}
	// swap
	if (k_max != index)
	{
		for (int i = 0; i < c; i++)
		{
			tmp = M[index][i];
			M[index][i] = M[k_max][i];
			M[k_max][i] = tmp;
		}
	}
	return k_max;
}

double det_gauss(const MatrixXd &M)
{
	assert(M.rows() == M.cols());
	double det = 1.0, pivot, tmp;
	int k_max;
	int r = M.rows(), c = M.cols();

	MatrixXd mat(M); // copy

	for (int i = 0; i < r; i++)
	{
		k_max = pivot_gauss(mat, i);
		if (k_max != i)			det *= -1;
		pivot = mat[i][i];
		if (abs(pivot) == 0.0)
			return 0.0;
		for (int j = i + 1; j < r; j++) // khu gauss
		{
			tmp = - mat[j][i] / pivot;
			for (int k = i; k < c; k++)
			{
				mat[j][k] += tmp * mat[i][k];
			}
		}
	}
	for (int i = 0; i < r; i ++)
		det *= mat[i][i];
	return det;
}

// solve A * x = b
VectorXd solve_gauss(const MatrixXd &A, const VectorXd &b)
{
	int r = A.rows(), c = A.cols();
	assert(r == c && c == b.size());
	int k_max;
	double pivot, tp;
	assert(r == c);
	VectorXd result(r);
	MatrixXd tmp(r, c + 1);
	for (int i = 0; i < r; i++)
	{
		tmp[i][c] = b[i];
		for (int j = 0; j < c; j++)
			tmp[i][j] = A[i][j];
	}
	for (int i = 0; i < r; i++)
	{
		k_max = pivot_gauss(tmp, i);
		pivot = tmp[i][i];
		if (pivot == 0.0)
			return result;
		for (int j = i + 1; j < r; j++) // khu gauss
		{
			tp = - tmp[j][i] / pivot;
			for (int k = i; k < c + 1; k++)
			{
				tmp[j][k] += tp * tmp[i][k];
			}
		}
	}
	// tim nghiem
	double s = 0.0;
	for (int i = r - 1; i >= 0; i--)
	{
		s = 0.0;
		for (int j = r - 1; j > i ; j--)
		{
			s += tmp[i][j] * result[j];
		}
		result[i] = (tmp[i][c] - s) / tmp[i][i];
	}
	return result;
}

// det_lu
double det_lu (const MatrixXd &M)
{
	assert(M.rows() == M.cols());
	pair<MatrixXd, MatrixXd> lu = LU(M);
	double det = 1.0;
	for (int i = 0; i < M.rows(); i++)
		det *= lu.first[i][i] * lu.second[i][i];
	return det;
}

// solve-lu
VectorXd solve_lu(const MatrixXd &A, const VectorXd &b)
{
	int r = A.rows(), c = A.cols();
	assert(r == c && c == b.size());
	VectorXd x(r), y(r);
	pair<MatrixXd, MatrixXd> lu = LU(A);
	double det = 1.0;
	for (int i = 0; i < r; i++)
		det *= lu.first[i][i] * lu.second[i][i];
	if (det == 0.0)
		return y;
	// solve b = Ax = LUx <=> solve Ly = b and Ux = y
	// solve L y = b, L la ma tran tam giac duoi,
	double s;
	for (int i = 0; i < r; i++)
	{
		s = 0.0;
		for (int j = 0; j < i ; j++)
			s += y[j] * lu.first[i][j];
		y[i] = (b[i] - s) / lu.first[i][i];
	}
	// solve Ux = y, U la ma tran tam giac tren
	for (int i = r - 1 ; i >=0 ; i--)
	{
		s = 0.0;
		for (int j = r - 1; j > i ; j--)
			s += x[j] * lu.second[i][j];
		x[i] = (y[i] - s) / lu.second[i][i];
	}
	return x;
}

VectorXd solve_gauss_jordan(const MatrixXd &A, const VectorXd &b)
{
	int r = A.rows(), c = A.cols();
	assert(r == c && c == b.size());
	MatrixXd mat(r, c+1);	// copy
	VectorXd x(r);
	for (int i = 0; i < r; i++)
	{
		mat[i][c] = b[i];
		for (int j = 0; j<c; j++)
			mat[i][j] = A[i][j];
	}
	// khu xuoi
	for (int i = 0; i < r; i ++)
	{
		pivot_gauss(mat, i);
		double
			pivot = mat[i][i];
		if (pivot == 0.0)
			return x;
		for (int j = i + 1; j < r; j++) // khu gauss, row
		{
			double
				tp = - mat[j][i] / pivot;
			for (int k = i; k < c + 1; k++) // col
			{
				mat[j][k] += tp * mat[i][k];
			}
		}
	}
	// khu nguoc
	for (int i = r-1; i >= 0; i--)
	{
		for (int j = i - 1; j >= 0; j--) // row
			mat[j][c] -= mat[i][c] * mat[j][i] / mat[i][i];
		x[i] = mat[i][c] / mat[i][i];
	}
	return x;
}

MatrixXd inv_gauss(const MatrixXd &M) // ma tran nghich dao bang pp gauss: find B : A*B = I, if det==0, return matran 0
{
	assert(M.rows() == M.cols());
	MatrixXd inv_mat(M.rows(), M.cols());
	/*MatrixXd mI(M.rows(), M.cols());
					mI.identity();  // ma tran donvi
	VectorXd  vtmp(M.rows());
	for (int i = 0; i < M.rows(); i++)
	{
		vtmp = solve_gauss(M, mI[i]);
		for (int j = 0; j < M.rows(); j++)
			inv_mat[j][i] = vtmp[j];
	}*/
	MatrixXd mat_tmp(M.rows(), M.rows() + M.cols());
	for (int i = 0; i < mat_tmp.rows(); i++)
	{
		for (int j = 0; j < mat_tmp.cols(); j++)
			if (j < M.cols())
				mat_tmp[i][j] = M[i][j];
		mat_tmp[i][M.cols() + i] = 1;
	}
	// bien doi gauss-giai n-hpt
	// khu xuoi
	double pivot, tp;
	for (int i = 0; i < mat_tmp.rows(); i++)
	{
		pivot_gauss(mat_tmp, i);
		pivot = mat_tmp[i][i];
		if (pivot == 0.0)
			return inv_mat;		// matran 0
		for (int j = i + 1; j < mat_tmp.rows(); j++) // row
		{
			tp = - mat_tmp[j][i] / pivot;
			for (int k = i; k < mat_tmp.cols(); k++) // col
				mat_tmp[j][k] += tp * mat_tmp[i][k];
		}
	}
	// khu nguoc
	for (int i = mat_tmp.rows() - 1; i >= 0; i--)
		for (int j = i - 1; j >= 0; j--) // row
			for (int k = mat_tmp.rows(); k < mat_tmp.cols(); k++)
				mat_tmp[j][k] -= mat_tmp[i][k] * mat_tmp[j][i] / mat_tmp[i][i];
	// get inv_mat
	for (int i = 0; i < inv_mat.rows(); i++)
		for (int j = 0; j < inv_mat.cols(); j++)
			inv_mat[i][j] = mat_tmp[i][j + inv_mat.cols()] / mat_tmp[i][i];
	return inv_mat;
}


MatrixXd inv_lu(const MatrixXd &M)  // ma tran nghic dao bang phan tich LU
{
	assert(M.rows() == M.cols());

	MatrixXd	mI(M.rows(), M.cols());
					mI.identity();  // ma tran donvi
	VectorXd  vtmp(M.rows());
	MatrixXd inv_mat(M.rows(), M.cols());
	for (int i = 0; i < M.rows(); i++)
	{
		vtmp = solve_lu(M, mI[i]);
		for (int j = 0; j < M.rows(); j++)
			inv_mat[j][i] = vtmp[j];
	}
	return inv_mat;
}

// create matrix from pointer
MatrixXd create_matrix(double **mat, int rowNum, int colNum)
{
	MatrixXd result(rowNum, colNum);
	for (int i = 0; i < rowNum; i++)
		for (int j = 0; j < colNum; j++)
			result[i][j] = mat[i][j];
	return result;
}


// create vector from pointer
VectorXd create_vector(double *vec, int num)
{
	VectorXd result(num);
	for (int i = 0; i < num; i++)
		result[i] = vec[i];
	return result;
}

// giai hpt eqn(x) = 0, bang pp so
template <class EQN, class DEQN>
double fsolve(EQN eqn, DEQN deqn, double x0, int maxIter, double eps)		// giai pt eqn(x) = 0, bang pp lap Newton
{
	// dau vao:
	//	+ eqn : phuong trinh
	//	+ deqn: dao ham cua phuong trinh
	//  + x0 : gia tri dau
	//	+ maxIter : so vong lap toi da
	//	+ eps : sai so
	// x_(k+1) = x_k - f(x_k) / f'(x_k)
	double x_k = x0, f_k, df_k;
	for (int i = 0; i < maxIter; i++)
	{
		f_k = eqn(x_k);
		df_k = deqn(x_k);
		if (abs(f_k) < eps)
			return x_k;		// nghiem cua phuong trinh
		if (df_k == 0.0)
			return x_k;		// diem ki di, nghiem khong tin tuong
		x_k -= f_k / df_k;
	}
	return x_k;
}

// giai pt eqn(x) = fE, bang pp so
template <class EQN, class DEQN>
double fsolve(EQN eqn, DEQN deqn, double x0, double fE, int maxIter, double eps)		// giai pt eqn(x) = fE, bang pp lap Newton
{
	// dau vao:
	//	+ eqn : phuong trinh
	//	+ deqn: dao ham cua phuong trinh
	//	+ fE : gia tri can dat den
	//  + x0 : gia tri dau
	//	+ maxIter : so vong lap toi da
	//	+ eps : sai so
	// x_(k+1) = x_k - f(x_k) / f'(x_k)
	double x_k = x0, f_k, df_k;
	for (int i = 0; i < maxIter; i++)
	{
		f_k = eqn(x_k) - fE;
		df_k = deqn(x_k);
		if (abs(f_k) < eps)
			return x_k;		// nghiem cua phuong trinh
		if (df_k == 0.0)
			return x_k;		// diem ki di, nghiem khong tin tuong
		x_k -= f_k / df_k;
	}
	return x_k;
}

// giai hpt phi tuyen bang pp lap Newton-Raphson
template <class FUNC, class DIFF_FUNC>
VectorXd fsolve(FUNC eqns, DIFF_FUNC deqns, VectorXd x0, int maxIter, double eps) // giai hpt eqns(vx) = 0
{
	// dau vao:
	//	+ eqns : he phuong trinh phi tuyen size = n x 1
	//	+ deqns : dao ham cua hpt theo bien vector, ma tran Jacobian, size = n x n
	//	+ x0	: gia tri ban dau, size = n x 1
	//  + maxIter : so vong lap toi da
	//	+ eps : sai so
	int dim = x0.size();		// so phuong trinh
	double det;					// dinh thuc cua ma tran
	VectorXd x_k(x0), 
		x_temp(x0);
	VectorXd f_k(dim);
	MatrixXd df_k(dim, dim),
		inv_df_k(dim, dim);					// ma tran nghich dao cua ma tran jacobi df_k
	for (int i = 0; i < maxIter; i++)
	{
		f_k = eqns(x_k);
		df_k = deqns(x_k);
		if (norm2(f_k) < eps)
			return x_k;
		/*// not so good - phai khu gauss 2 lan, O(2*n*n)
		det = det_gauss(df_k);				// dinh thuc cua ma tran 
		if (det == 0.0)
			return x_k;
		inv_df_k = inv_gauss(df_k);			// tinh ma tran nghich dao bang pp gauss
		x_k -= inv_df_k * f_k;
		*/
		// khu gauss 1 lan
		x_temp = solve_gauss(df_k, f_k);		// df_k * x_temp = f_k, x_temp co the la nghiem hinh thuc do det(df_k) = 0.0
		// kiem tra lai nghiem
		det = norm2(df_k * x_temp - f_k);
		if (det > eps)
			return x_k;							// thoat vi det = 0
		x_k -= x_temp;
	}
	return x_k;
}

// giai hpt eqns(x) = fE
template <class FUNC, class DIFF_FUNC>
VectorXd fsolve(FUNC eqns, DIFF_FUNC deqns, VectorXd x0,  VectorXd fE, int maxIter, double eps) // giai hpt eqns(vx) = fE
{
	// dau vao:
	//	+ eqns : he phuong trinh phi tuyen size = n x 1
	//	+ deqns : dao ham cua hpt theo bien vector, ma tran Jacobian, size = n x n
	//	+ x0	: gia tri ban dau, size = n x 1
	//  + maxIter : so vong lap toi da
	//	+ eps : sai so
	int dim = x0.size();		// so phuong trinh
	double det;					// dinh thuc cua ma tran
	VectorXd x_k(x0),
		x_temp(x0);
	VectorXd f_k(dim);
	MatrixXd df_k(dim, dim),
		inv_df_k(dim, dim);					// ma tran nghich dao cua ma tran jacobi df_k
	for (int i = 0; i < maxIter; i++)
	{
		f_k = eqns(x_k) - fE;
		df_k = deqns(x_k);
		if (norm2(f_k) < eps)				// tim thay nghiem
			return x_k;
		/*// not so good - khu gauss 2 lan
		det = det_gauss(df_k);				// dinh thuc cua ma tran 
		if (det == 0.0)
			return x_k;
		inv_df_k = inv_gauss(df_k);			// tinh ma tran nghich dao bang pp gauss
		x_k -= inv_df_k * f_k;
		*/
		x_temp = solve_gauss(df_k, f_k);
		det = norm2(df_k * x_temp - f_k);
		if (det > eps)		// det == 0
			return x_k;						// thoat vi det = 0;
		x_k -= x_temp;
	}
	return x_k;								// nghiem khong tin tuong, i== maxIter
}

template <class FUNCS>
MatrixXd	ode_45(FUNCS fty, VectorXd tspan, VectorXd y0) // y' = f(t,y)
{
	assert(tspan.size() >= 3);

	VectorXd tspan0;
	double t0, h, tf;
	if (tspan.size() > 3)
	{
		tspan0 = tspan;
		t0 = tspan[0]; tf = tspan[tspan.size()-1]; h = tspan[1] - tspan[0];
	}
	else
	{
		t0 = tspan[0]; tf = tspan[2]; h = tspan[1];
		int N = (int)((tf-t0)/h);
		tspan0 = VectorXd(N+1);
		for (int i = 0; i < N; i++)
			tspan[0] = t0 + i*h;
		tspan0[N] = tf;
	}
	VectorXd yi(y0);
	VectorXd k1, k2, k3, k4;
	int row = tspan0.size();
	int col = y0.size() + 1;
	MatrixXd result(row, col);
	result[0][0] = tspan0[0];
	for (int j = 1; j < col; j++)
		result[0][j] = yi[j-1];
	for (int i = 0; i < row - 1; i++)
	{
		k1 = fty(tspan0[i], yi);
		k2 = fty(tspan0[i] + h/2, yi + k1*h/2);
		k3 = fty(tspan0[i] + h/2, yi + k2*h/2);
		k4 = fty(tspan0[i] + h, yi + k3*h);
		yi += (k1 + k2*2 + k3*2 + k4)*h/6;
		result[i+1][0] = tspan0[i+1];
		for (int j = 1; j < col; j++)
			result[i+1][j] = yi[j-1];
	}
	return result;
}

// giai hpt vi phan bang pp Kunge-Kutta bac 4
// y' = f(t,y) = g(y) + h(t)
// M.q" + H(q, q') = tau(t) 
// y1 = q  => dy1 = q'
// y2 = q' => dy2 = q" = inv(M)(tau(t) - H(q, q')  
// ==>
// y = [op(y1), op(y2)], dim(y) = dim(y1) + dim(y2) = 2 * dim(q)
// g(y) = - inv(M) * H(q, q')
// h(t) = inv(M) * tau(t)
template <class FUNC1, class FUNC2>
MatrixXd	ode_45(FUNC1 gy, FUNC2 ht, VectorXd tspan, VectorXd y0)
{
	// input: gy, ht
	// ht la ham so lien tuc theo t
	// tspan = [t0, h, tf] or [t0, t1, .., tn]
	// y0 = [y00, y01, y02, .., y0n]
	assert(tspan.size() >= 3);
	//
	double t0, h, tf;
	VectorXd tspan0;
	if (tspan.size() > 3)
	{
		t0 = tspan[0]; h = tspan[1] - tspan[0]; tf = tspan[tspan.size() - 1];
		tspan0 = tspan;
	}
	else
	{
		t0 = tspan[0]; h = tspan[1]; tf = tspan[2];
		int N = (int)((tf - t0) / h);
		tspan0 = VectorXd(N+1);
		for (int i = 0; i < N; i++)
			tspan0[i] = t0 + i * h;
		tspan0[N] = tf;
	}
	int row = tspan0.size();
	int col = y0.size() + 1;
	MatrixXd	result(row, col);
	VectorXd yi(y0);
	VectorXd k1, k2, k3, k4;
	double ti = 0.0;
	//
	result[0][0] = tspan0[0];
	for (int i = 1; i < col; i++)
		result[0][i] = yi[i-1];
	//
	for (int i = 0; i < row-1; i++)
	{
		ti = tspan0[i];
		k1 = gy(yi) + ht(ti);
		k2 = gy(yi + k1*h/2) + ht(ti + h/2);
		k3 = gy(yi + k2*h/2) + ht(ti + h/2);
		k4 = gy(yi + k3*h) + ht(ti + h);
		
		yi += (k1 + k2*2 + k2*2 + k4) * h/ 6;

		result[i+1][0] = ti+h;
		for (int j = 1; j < col; j++)
			result[i+1][j] = yi[j-1];
	}
	return result;
}

template <class FUNC>
MatrixXd	ode_45(FUNC gy, MatrixXd ht, VectorXd tspan0, VectorXd y0) // y' = f(t,y) = g(y) + h(ti)
{
	// input: ht : ma tran chua cac gia tri tai cac thoi diem ti + i*h/2, i = 0..2*n
	
	assert(ht.cols() == y0.size() && tspan0.size() >= 3);
	VectorXd tspan;
	double t0, tf, h;
	if (tspan0.size() > 3)
	{
		t0 = tspan0[0]; tf = tspan0[tspan0.size() - 1]; h = tspan0[1] - tspan0[0];
		tspan = tspan0;
	}
	else
	{
		t0 = tspan0[0]; h = tspan0[1]; tf = tspan0[2];
		int N = (int)((tf-t0)/h);
		tspan = VectorXd(N+1);
		for (int i = 0; i < N; i++)
			tspan[0] = t0 + i*h;
		tspan[N] = tf;
	}
	int row = tspan.size(), col = ht.cols() + 1;
	MatrixXd result(row, col);
	result[0][0] = tspan[0];
	VectorXd yi(y0), hi;
	VectorXd k1, k2, k3, k4;

	for (int i = 1; i < col; i++)
		result[0][i] = yi[i-1];
	for (int i = 0; i < row-1; i++)
	{
		hi = ht[2*i];
		k1 = gy(yi) + hi;			hi = ht[2*i+1];		// ht(ti + h/2);
		k2 = gy(yi + k1*h/2) + hi;	
		k3 = gy(yi + k2*h/2) + hi;	hi = ht[2*i+2];		// ht(ti + h)
		k4 = gy(yi + k3*h) + hi;

		yi += (k1 + k2*2 + k2*2 + k4) * h/ 6;

		result[i+1][0] = tspan[i+1];
		for (int j = 1; j < col; j++)
			result[i+1][j] = yi[j-1];
	}
	return result;
}


template <class DDQ, class MTKL, class V>
MatrixXd	ode_45(DDQ ddq, MTKL M1, V V1, MatrixXd Tau, VectorXd tspan, VectorXd q0, VectorXd dq0)
{
	// dau vao
	// ddq: tinh dao ham cap 2 tai thoi diem ti
	// M la ma tran khoi luong, V = V(q,q'), tau la gia tri luc tai thoi diem ti
	// Tau : gia tri dau vao tai cac thoi diem i*h/2, tspan, q0, dq0
	assert(q0.size() == dq0.size() && q0.size() == Tau.cols() && tspan.size() >= 3);
	double t0, h, tf;
	VectorXd tspan0;
	if (tspan.size() > 3)
	{
		tspan0 = tspan;
		t0 = tspan[0]; tf = tspan[tspan.size() - 1]; h = tspan[1] - tspan[0];
	}
	else
	{
		t0 = tspan[0]; tf = tspan[2]; h= tspan[1];
		int N = (int)((tf-t0)/h);
		tspan0 = VectorXd(N+1);
		for (int i = 0; i < N; i++)
			tspan0[i] = t0 + i*h;
		tspan0[N] = tf;
	}
	int dim = q0.size();
	int col = q0.size() + dq0.size() + dim + 1; // t, q, dq, ddq
	int row = tspan0.size();
	VectorXd qi(q0), dqi(dq0), ddqi, k1(2*dim), k2(2*dim), k3(2*dim), k4(2*dim), taui, Vtmp, tmp;
	MatrixXd result(row, col), Mtmp;
	tmp = combine_vector(qi, dqi);
	result[0][0] = tspan0[0];					// yi
	for (int i = 1; i < 2*dim + 1; i++)
		result[0][i] = tmp[i-1];				// q, dq
	
	for (int i = 0; i < row-1; i++)
	{
		// ti
		Mtmp = M1(qi);
		Vtmp = V1(qi, dqi);
		taui = Tau[2*i];
		ddqi = ddq(Mtmp, Vtmp, taui);
		//
		for (int j = 2*dim+1; j < col; j++)			// ddq
			result[i][j] = ddqi[j - 2 * dim - 1];
		//
		k1 = combine_vector(dqi, ddqi);
		// ti + h / 2
		qi = sub_vector(tmp + k1 * h / 2, 0, dim - 1);
		dqi = sub_vector(tmp + k1 * h / 2, dim, 2 * dim - 1);
		Mtmp = M1(qi);
		Vtmp = V1(qi, dqi);
		taui = Tau[2*i+1];
		ddqi = ddq(Mtmp, Vtmp, taui);
		k2 = combine_vector(dqi, ddqi);
		// 
		qi = sub_vector(tmp + k2 * h / 2, 0, dim - 1);
		dqi = sub_vector(tmp + k2 * h / 2, dim, 2 * dim - 1);
		Mtmp = M1(qi);
		Vtmp = V1(qi, dqi);
		//taui = Tau[2*i+1];		// not change
		ddqi = ddq(Mtmp, Vtmp, taui);
		k3 = combine_vector(dqi, ddqi);
		//
		qi = sub_vector(tmp + k3 * h, 0, dim - 1);
		dqi = sub_vector(tmp + k3 * h, dim, 2 * dim - 1);
		Mtmp = M1(qi);
		Vtmp = V1(qi, dqi);
		taui = Tau[2*i+2];
		ddqi = ddq(Mtmp, Vtmp, taui);
		k4 = combine_vector(dqi, ddqi);
		// 
		tmp += (k1 + k2*2 + k3*2 + k4)*h/6;
		qi = sub_vector(tmp, 0, dim - 1);
		dqi = sub_vector(tmp, dim, 2*dim-1);
		//
		result[i+1][0] = tspan0[i+1];			// ti
		for (int j = 1; j < 2*dim + 1; j++)		// q, dq
			result[i+1][j] = tmp[j-1];
	}
	// compute ddq at tf
	Mtmp = M1(qi);	Vtmp = V1(qi, dqi);	taui = Tau[Tau.rows() - 1];
	ddqi = ddq(Mtmp, Vtmp, taui);
	for (int j = 2*dim+1; j < col; j++)			// ddq
		result[row - 1][j] = ddqi[j - 2 * dim - 1];
	return result;
}

VectorXd sub_vector(const VectorXd &v_source, int from, int to)
{
	int size = to - from + 1;
	VectorXd rs(size);
	for (int i = from; i <= to; i++)
		rs[i - from] = v_source[i];
	return rs;
}

MatrixXd sub_matrix(const MatrixXd &m_source, int r_f, int r_t, int c_f, int c_t)
{
	int row = r_t - r_f + 1,
		col = c_t - c_f + 1;
	MatrixXd	rs(row, col);
	for (int r = r_f; r <= r_t; r++)
		for (int c = c_f; c <= c_t; c++)
			rs[r-r_f][c-c_f] = m_source[r][c];
	return rs;
}

VectorXd combine_vector(const VectorXd &v1, const VectorXd &v2)
{
	int size1 = v1.size(), size2 = v2.size();
	VectorXd rs(size1 + size2);
	for (int i = 0; i < size1; i++)
		rs[i] = v1[i];
	for (int i = 0; i < size2; i++)
		rs[i+size1] = v2[i];
	return rs;
}

} // namespace
#endif