/*
    (c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
*/

#include "stdafx.h"
#include "..\include\vecnorm.h"

double norm1(const Vector<double> &v)
{
   double result(0);
   for(int i=0;i<int(v.size());i++) result = result + fabs(v[i]);
   return result;
}
double normI(const Vector<double> &v)
{
   double maxItem(fabs(v[0])), temp;
   for(int i=1;i<int(v.size());i++)
   {
      temp = fabs(v[i]);
      if(temp > maxItem) maxItem = temp;
   }
   return maxItem;
}
