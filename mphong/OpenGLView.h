/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 26/9/2013
	Khoi tao moi truong OpenGL cho Class CMpView va mo phong
*/

#pragma once
#include "OpenGL.h"
#include "Scene.h"
//#include "C3dofs.h"
#include "C4khau.h"

class COpenGLView :
	public COpenGL
{
public: // du lieu 
	CScene					m_Scene;			// Thong tin ve moi truong
	int						m_nTimerID;			// ID cua timer
	int						m_nTimerElapsed;	// khoang thoi gian, (ms)
	// my robot - 3 dofs - RRR
	//C3dofs					m_Robot;
	C4khau					m_Robot;
	bool m_bInitialation;
public: // bien trung gian
	double	m_dZooming;				// zoom ma hinh ( khi bPerspective = true)
	Vec3D	m_v3dScale;				// zoom man hinh (khi bPerspective = false)
	Vec3D	m_v3dTranslate;			// re chuot sang trai, phai,  an chuot trai
	Vec3D	m_v3dRotate;			// xoay chuot, an chuot phai
	CPoint	m_pOldMouse;			// vi tri chuot
	bool m_bLMouseDown,				// an chuot trai
		m_bRMouseDown,				// an chuot phai
		m_bMMouseDown;				// an chuot giua
private: // du lieu trung gian
	Vec3D	m_v3d_Left, m_v3d_Up, m_v3d_Out;
public:
	COpenGLView(void);
	~COpenGLView(void);
	// reset settng
	void Reset();
	// draw objects here - call this function on CxxxView::OnPaint
	void OnPaint(void);
	// handler message WM_SIZE on CxxxView::OnSize
	void OnSize(UINT nType, int cx, int cy);
	void drawAxis(double lengthAxis = 100.0);
	// view from top y+ to zx surface
	void OnViewTop(void);
	// view from y- to xz surface 
	void OnViewBottom(void);
	// view from left x- to yz surface
	void OnViewLeft(void);
	// view from right x+ to yz surface
	void OnViewRight(void);
	// view from front z+ to xy surface
	void OnViewFront(void);
	// view from back z- to xy surface
	void OnViewBack(void);
	void OnViewPerspective(void);
};

