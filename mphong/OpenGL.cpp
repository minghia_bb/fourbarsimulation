#include "StdAfx.h"
#include "OpenGL.h"


COpenGL::COpenGL(void)
{
	m_GLPixelIndex = 0;
}


COpenGL::~COpenGL(void)
{
}


// setup pixel
BOOL COpenGL::SetupPixelFormat(void)
{
	PIXELFORMATDESCRIPTOR pixelDesc;
	
	pixelDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixelDesc.nVersion = 1;	
	pixelDesc.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER | PFD_STEREO_DONTCARE;	
	pixelDesc.iPixelType = PFD_TYPE_RGBA;
	pixelDesc.cColorBits = 32;
	pixelDesc.cRedBits = 8;
	pixelDesc.cRedShift = 16;
	pixelDesc.cGreenBits = 8;
	pixelDesc.cGreenShift = 8;
	pixelDesc.cBlueBits = 8;
	pixelDesc.cBlueShift = 0;
	pixelDesc.cAlphaBits = 0;
	pixelDesc.cAlphaShift = 0;
	pixelDesc.cAccumBits = 64;
	pixelDesc.cAccumRedBits = 16;
	pixelDesc.cAccumGreenBits = 16;
	pixelDesc.cAccumBlueBits = 16;
	pixelDesc.cAccumAlphaBits = 0;
	pixelDesc.cDepthBits = 32;
	pixelDesc.cStencilBits = 8;
	pixelDesc.cAuxBuffers = 0;
	pixelDesc.iLayerType = PFD_MAIN_PLANE;
	pixelDesc.bReserved = 0;
	pixelDesc.dwLayerMask = 0;
	pixelDesc.dwVisibleMask = 0;
	pixelDesc.dwDamageMask = 0;	
	m_GLPixelIndex = ChoosePixelFormat(hDC, &pixelDesc);
	if(m_GLPixelIndex == 0) // Choose default
	{
		m_GLPixelIndex = 1;
		if(DescribePixelFormat(hDC, m_GLPixelIndex,
			sizeof(PIXELFORMATDESCRIPTOR), &pixelDesc) == 0)
			return FALSE;
	}	
	if(!SetPixelFormat(hDC, m_GLPixelIndex, &pixelDesc))
		return FALSE;	
	return TRUE;
}


BOOL COpenGL::CreateViewGLContext(HDC _hDC)
{
	hDC = _hDC;
	hRC = wglCreateContext(hDC);
	
	if(hRC == NULL)
		return FALSE;
	
	if(wglMakeCurrent(hDC,hRC) == FALSE)
		return FALSE;
	
	return TRUE;
}


int COpenGL::OnCreate(HDC _hDC)
{
	hDC = _hDC;
    if (SetupPixelFormat() == FALSE)
		return -1;
    if (CreateViewGLContext(hDC) == FALSE)
		return -1;

	// Light definitions
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 100.0 };
	GLfloat position[] = { 0.5, 0.8, 1, 0.0 };
	GLfloat ambient[] = {0.0, 0.0, 0.0, 1.0};
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat specular[] = {1.0, 1.0, 1.0, 1.0};
	//
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glLightfv(GL_LIGHT0, GL_POSITION, position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	// 
	glClearColor(0.8f, 0.8f, 0.5f, 1.0f);
	glClearDepth(1.0);
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 1.0);
	glShadeModel (GL_SMOOTH);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	// enable light as default
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	// clear color back ground
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	return 0;
}


void COpenGL::OnDestroy(void)
{
	if(wglGetCurrentContext() != NULL)
		wglMakeCurrent(NULL, NULL);	
	if(hRC != NULL)
	{
		wglDeleteContext(hRC);
		hRC = NULL;
	}
	::ReleaseDC(hWnd, hDC);

}



void COpenGL::ViewPort(double m_dEyex, double m_dEyey, double m_dEyez, 
	double m_dCenterx, double m_dCentery, double m_dCenterz, 
	double m_dUpx, double m_dUpy, double m_dUpz)
{
	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	gluLookAt(m_dEyex, m_dEyey, m_dEyez, m_dCenterx, m_dCentery, m_dCenterz, m_dUpx, m_dUpy, m_dUpz);
}


// xO=yO=0 mean left up coner, xO=yO=1 mean right bottom coner, xO=yO=-1 middle
void COpenGL::UseOrthogonal(int xO, int yO)
{
	// left bottom corner
	// y+ up-down, x+ left-right
	if (xO == 0 && yO == 0)
	{
		glMatrixMode(GL_PROJECTION); 
		glLoadIdentity();
		glOrtho(0.0, m_nWidth, 0.0, m_nHeight, -10.0, 10.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		return;
	}
	// left top corner
	// y+ down-up, x+ left-right
	if (xO == 0 && yO >= 1)
	{
		glMatrixMode(GL_PROJECTION); 
		glLoadIdentity();
		glOrtho(0.0, m_nWidth, m_nHeight, 0.0, -10.0, 10.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		return;
	}
	// right - up - corner
	// y+ up-down, x+ right-left
	if (xO >=1 && yO == 0)
	{
		glMatrixMode(GL_PROJECTION); 
		glLoadIdentity();
		glOrtho(m_nWidth, 0.0, 0.0, m_nHeight, -10.0, 10.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		return;
	}
	// right - bottom - corner
	// y + down-up, x+ right-left
	if (xO >=1 && yO >= 1)
	{
		glMatrixMode(GL_PROJECTION); 
		glLoadIdentity();
		glOrtho(m_nWidth, 0.0, m_nHeight, 0.0, -10.0, 10.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		return;
	}
	// default - middle, xleft right, y up -down
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();
	glOrtho(-m_nWidth/2.0, m_nWidth/2.0, -m_nHeight/2.0, m_nHeight/2.0, -10.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void COpenGL::UsePerspective(double m_dFovy, double m_dNear, double m_dFar)
{
	double m_dAspect = m_nHeight != 0 ? m_nWidth / (1.0*m_nHeight) : m_nWidth;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();							// Start using Projection Matrix
	gluPerspective(m_dFovy, m_dAspect, m_dNear, m_dFar);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
