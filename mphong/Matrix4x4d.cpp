#include "StdAfx.h"
#include "Matrix4x4d.h"

//---------------------------------------------------------------------------
CMatrix4x4d::CMatrix4x4d(void)
//---------------------------------------------------------------------------
{
	Identity();
}

//---------------------------------------------------------------------------
CMatrix4x4d::CMatrix4x4d(double m0,double m4,double m8, double m12,
		                 double m1,double m5,double m9, double m13,
		                 double m2,double m6,double m10,double m14,
		                 double m3,double m7,double m11,double m15)
//---------------------------------------------------------------------------
{
	m[0] = m0; m[4] = m4; m[8] = m8; m[12] = m12;
	m[1] = m1; m[5] = m5; m[9] = m9; m[13] = m13;
	m[2] = m2; m[6] = m6; m[10] = m10; m[14] = m14;
	m[3] = m3; m[7] = m7; m[11] = m11; m[15] = m15;
}

//---------------------------------------------------------------------------
CMatrix4x4d::~CMatrix4x4d(void)
//---------------------------------------------------------------------------
{
}

//---------------------------------------------------------------------------
void CMatrix4x4d::Identity()
//---------------------------------------------------------------------------
{
	m[0] = 1.0f; m[4] = 0.0f; m[8] =0.0f; m[12] = 0.0f;
    m[1] = 0.0f; m[5] = 1.0f; m[9] =0.0f; m[13] = 0.0f;
    m[2] = 0.0f; m[6] = 0.0f; m[10] = 1.0f; m[14] = 0.0f;
    m[3] = 0.0f; m[7] = 0.0f; m[11] = 0.0f; m[15] = 1.0f;
}

//---------------------------------------------------------------------------
void CMatrix4x4d::zeros()
//---------------------------------------------------------------------------
{
	for (int i = 0; i < 15; i++)
		m[i] = 0.0;
}

//---------------------------------------------------------------------------
CMatrix4x4d CMatrix4x4d::operator + (const CMatrix4x4d &mat)
//---------------------------------------------------------------------------
{
	CMatrix4x4d result;

	int i;
	for (i = 0; i < 16; i++)
		result.m[i] = m[i] + mat.m[i];
	return result;
}

//---------------------------------------------------------------------------
CMatrix4x4d CMatrix4x4d::operator - (const CMatrix4x4d &mat)
//---------------------------------------------------------------------------
{
	CMatrix4x4d result;
	int i;
	for (i = 0; i < 16; i++)
		result.m[i] = m[i] - mat.m[i];
	return result;
}

//---------------------------------------------------------------------------
CMatrix4x4d CMatrix4x4d::operator * (const CMatrix4x4d &mat)
//---------------------------------------------------------------------------
{
	CMatrix4x4d result;
	result.m[0] = m[0] * mat.m[0] + m[4] * mat.m[1]  +  m[8] * mat.m[2] + m[12] * mat.m[3];
	result.m[1] = m[1] * mat.m[0] + m[5] * mat.m[1]  +  m[9] * mat.m[2] + m[13] * mat.m[3];
	result.m[2] = m[2] * mat.m[0] + m[6] * mat.m[1]  + m[10] * mat.m[2] + m[14] * mat.m[3];
	result.m[3] = m[3] * mat.m[0] + m[7] * mat.m[1]  + m[11] * mat.m[2] + m[15] * mat.m[3];

	result.m[4] = m[0] * mat.m[4] + m[4] * mat.m[5]  +  m[8] * mat.m[6] + m[12] * mat.m[7];
	result.m[5] = m[1] * mat.m[4] + m[5] * mat.m[5]  +  m[9] * mat.m[6] + m[13] * mat.m[7];
	result.m[6] = m[2] * mat.m[4] + m[6] * mat.m[5]  + m[10] * mat.m[6] + m[14] * mat.m[7];
	result.m[7] = m[3] * mat.m[4] + m[7] * mat.m[5]  + m[11] * mat.m[6] + m[15] * mat.m[7];

	result.m[8] = m[0] * mat.m[8] + m[4] * mat.m[9]  +  m[8] * mat.m[10] + m[12] * mat.m[11];
	result.m[9] = m[1] * mat.m[8] + m[5] * mat.m[9]  +  m[9] * mat.m[10] + m[13] * mat.m[11];
	result.m[10] = m[2] * mat.m[8] + m[6] * mat.m[9]  + m[10] * mat.m[10] + m[14] * mat.m[11];
	result.m[11] = m[3] * mat.m[8] + m[7] * mat.m[9]  + m[11] * mat.m[10] + m[15] * mat.m[11];

	result.m[12] = m[0] * mat.m[12] + m[4] * mat.m[13] +  m[8] * mat.m[14] + m[12] * mat.m[15];
	result.m[13] = m[1] * mat.m[12] + m[5] * mat.m[13] +  m[9] * mat.m[14] + m[13] * mat.m[15];
	result.m[14] = m[2] * mat.m[12] + m[6] * mat.m[13] + m[10] * mat.m[14] + m[14] * mat.m[15];
	result.m[15] = m[3] * mat.m[12] + m[7] * mat.m[13] + m[11] * mat.m[14] + m[15] * mat.m[15];

	return result;
}

//---------------------------------------------------------------------------
CMatrix4x4d CMatrix4x4d::operator * (const double &scalar)
//---------------------------------------------------------------------------
{
	CMatrix4x4d result;
	int i;
	for (i = 0; i < 16; i++)
		result.m[i] = scalar * m[i];
	return result;
}
/*
CMatrix4x4d::CMatrix4x4d(const CMatrix4x4d& mat)
{
	for (int i = 0; i < 16; i++)
		m[i] = mat.m[i];
}
*/
//  Methods
//---------------------------------------------------------------------------
void CMatrix4x4d::Transpose(const CMatrix4x4d &mat)
//---------------------------------------------------------------------------
{
	Identity();
	m[0] = mat.m[0];  m[4] = mat.m[1];  m[8] = mat.m[2];  m[12] = mat.m[3];
	m[1] = mat.m[4];  m[5] = mat.m[5];  m[9] = mat.m[6];  m[13] = mat.m[7];
	m[2] = mat.m[8];  m[6] = mat.m[9];  m[10]= mat.m[10]; m[14] = mat.m[11];
	m[3] = mat.m[12]; m[7] = mat.m[14]; m[11]= mat.m[14]; m[15] = mat.m[15];
}

//---------------------------------------------------------------------------
void CMatrix4x4d::Translate(double x, double y, double z)
//---------------------------------------------------------------------------
{
	Identity();
	m[12] = x;
	m[13] = y;
	m[14] = z;
}

//---------------------------------------------------------------------------
void CMatrix4x4d::Rotate_X(double XAngle, bool bRad)
//---------------------------------------------------------------------------
{
	Identity();
	double Xrad;
	if (bRad)
		Xrad = XAngle;
	else 
		Xrad = DEGTORAD(XAngle);
	m[5] = cos(Xrad);
	m[6] = sin(Xrad);
	m[9] = -sin(Xrad);
	m[10] = cos(Xrad);
}

//---------------------------------------------------------------------------
void CMatrix4x4d::Rotate_Y(double YAngle, bool bRad)
//---------------------------------------------------------------------------
{
	Identity();
	double Yrad;
	if (bRad)		Yrad = YAngle;
	else 		Yrad = DEGTORAD(YAngle);
	m[0] = cos(Yrad);
	m[2] = -sin(Yrad);
	m[8] = sin(Yrad);
	m[10] = cos(Yrad);
}

//---------------------------------------------------------------------------
void CMatrix4x4d::Rotate_Z(double ZAngle, bool bRad)
//---------------------------------------------------------------------------
{
	Identity();
	double Zrad;
	if (bRad)		Zrad = ZAngle;
	else 		Zrad = DEGTORAD(ZAngle);
	m[0] = cos(Zrad);
	m[1] = sin(Zrad);
	m[4] = -sin(Zrad);
	m[5] = cos(Zrad);
}


// Matrix Denavit-Harteberg
//---------------------------------------------------------------------------
void CMatrix4x4d::MatrixDH(double theta, double d, double a, double alpha, bool bRad)
//---------------------------------------------------------------------------
{
	double t_,al_;
	Identity();
	if (bRad)	{ t_ = theta; al_ = alpha ; }
	else	{ t_ = DEGTORAD(theta) ; al_ = DEGTORAD(alpha) ; }

	m[0] = cos(t_); m[4] = -sin(t_) * cos(al_); m[8] = sin(t_) * sin(al_); m[12] = a * cos(t_);
	m[1] = sin(t_); m[5] = cos(t_) * cos(al_); m[9] = -cos(t_) * sin(al_); m[13] = a * sin(t_);
	m[2] = 0;       m[6] = sin(al_)        ; m[10] = cos(al_)        ; m[14] = d;
}


// Diff Matrix Denavit-Harteberg
//---------------------------------------------------------------------------
void CMatrix4x4d::DiffMatrixDH(double theta, double d, double a, double alpha, bool bRad)
//---------------------------------------------------------------------------
{
	double t_, al_;
	zeros();
	if (bRad)	{t_ = theta; al_ = alpha ; }
	else	{ t_ = DEGTORAD(theta) ; al_ = DEGTORAD(alpha) ; }

	m[0] = -sin(t_); m[4] = -cos(t_) * cos(al_); m[8] = cos(t_) * sin(al_); m[12]= -a * sin(t_);
	m[1] = cos(t_); m[5] = -sin(t_) * cos(al_); m[9] = sin(t_) * sin(al_); m[13] = a * cos(t_);
}


// Matrix Transform Craig - Rotx(alp) - Trans(x,a) - Rotz(theta) - Trans (z,d)
//---------------------------------------------------------------------------
void CMatrix4x4d::MatrixCraig(double alpha, double a, double theta, double d, bool bRad)
//---------------------------------------------------------------------------
{
	double t_, al_;
	Identity();
	if (bRad)	{ t_ = theta; al_ = alpha; }
	else	{ t_ = DEGTORAD(theta); al_ = DEGTORAD(alpha) ; }

	m[0] = cos(t_);          m[4] = -sin(t_);                           m[12] = a;
	m[1] = sin(t_) * cos(al_); m[5] = cos(t_) * cos(al_); m[9] = -sin(al_); m[13] = -d * sin(al_);
	m[2]= sin(t_) * sin(al_); m[6] = cos(t_) * sin(al_); m[10] = cos(al_); m[14] = d * cos(al_);
}


// ma tran chuyen tiep xac dinh tu 3 goc Euler, rotz-psi -- rotx-theta -- rotz-phi
void CMatrix4x4d::MatrixEuler(double psi_, double theta_, double phi_, double X, double Y, double Z, bool bRad)
{
	double psi, theta, phi;
	if (bRad)
	{
		psi = psi_; theta = theta_; phi = phi_;
	}
	else
	{
		psi = DEGTORAD(psi_);
		theta = DEGTORAD(theta_);
		phi = DEGTORAD(phi_);
	}
	Translate(X, Y, Z);
	m[0] = cos(psi) * cos(phi) - sin(psi) * cos(theta) * sin(phi);
	m[1] = sin(psi) * cos(phi) - cos(psi) * cos(theta) * sin(phi);
	m[2] = sin(theta) * sin(phi);

	m[4] = -cos(psi) * sin(phi) - sin(psi) * cos(theta) * cos(phi);
	m[5] = -sin(psi) * sin(phi) + cos(psi) * cos(theta) * cos(phi);
	m[6] = sin(theta) * cos(phi);

	m[8] = sin(psi) * sin(theta);
	m[9] = -cos(psi) * sin(theta);
	m[10] = cos(theta);
}

void CMatrix4x4d::MatrixEuler(double psi_, double theta_, double phi_, double XYZ[3], bool bRad)
{
	MatrixEuler(psi_, theta_, phi_, XYZ[0], XYZ[1], XYZ[2], bRad);
}


// ma tran chuyen tiep giua 2 htd xac dinh bang cac goc Cardan = rotx(alp) roty(beta) rotz(gamma)
void CMatrix4x4d::MatrixCardan(double alp_, double beta_, double gamma_, double x, double y, double z, bool bRad)
{
	double alp, beta, gamma;
	if (bRad)
	{
		alp = alp_; beta = beta_; gamma = gamma_;
	}
	else
	{
		alp = DEGTORAD(alp_);
		beta = DEGTORAD(beta_);
		gamma = DEGTORAD(gamma_);
	}
	Translate(x, y, z);
	m[0] = cos(beta) * cos(gamma);
	m[1] = cos(alp) * sin(gamma) + sin(alp) * sin(beta) * cos(gamma);
	m[2] = sin(alp) * sin(gamma) - cos(alp) * sin(beta) * cos(gamma);

	m[4] = -cos(beta) * sin(gamma);
	m[5] = cos(alp) * cos(gamma) - sin(alp) * sin(beta) * sin(gamma);
	m[6] = sin(alp) * cos(gamma) + cos(alp) * sin(beta) * sin(gamma);

	m[8] = sin(beta);
	m[9] = -sin(alp) * cos(beta);
	m[10] = cos(alp) * cos(beta);
}


// ma tran chuyen tiep xac dinh tu cac goc Roll-Pitch-Rall = rotz(phi) roty(theta) rotx(psi)
void CMatrix4x4d::MatrixRoll_Pitch_Yall(double phi_, double theta_, double psi_, double x, double y, double z, bool bRad)
{
	double psi, theta, phi;
	if (bRad)
	{
		psi = psi_; theta = theta_; phi = phi_;
	}
	else
	{
		psi = DEGTORAD(psi_);
		theta = DEGTORAD(theta_);
		phi = DEGTORAD(phi_);
	}
	Translate(x, y, z);
	m[0] = cos(phi) * cos(theta);
	m[1] = sin(phi) * cos(theta);
	m[2] = -sin(theta);

	m[4] = cos(phi) * sin(theta) * sin(psi) - sin(phi) * cos(psi);
	m[5] = sin(phi) * sin(theta) * sin(psi) + cos(phi) * cos(psi);
	m[6] = cos(theta) * sin(psi);

	m[8] = cos(phi) * sin(theta) * cos(psi) + sin(phi) * sin(psi);
	m[9] = sin(phi) * sin(theta) * cos(psi) - cos(phi) * sin(psi);
	m[10] = cos(theta) * cos(psi);
}


// tach lay ma tran quay
CMatrix4x4d CMatrix4x4d::RotateMatrix(void)
{
	CMatrix4x4d rs;
	rs = (*this);
	rs.m[12] = 0.0;
	rs.m[13] = 0.0;
	rs.m[14] = 0.0;
	rs.m[15] = 1.0;
	return rs;
}
