#include "StdAfx.h"
#include "OpenGLView.h"


COpenGLView::COpenGLView(void)
{
	//m_Scene = CScene();
	//m_Robot = C3dofs();			// load in CMpView::OnCreate(LPCREATESTRUCT lpCreateStruct)
	Reset();
	m_bInitialation = false;
	m_nTimerID = 10;
	m_nTimerElapsed = 10;
}


COpenGLView::~COpenGLView(void)
{
}

// reset ve trang thai ban dau
void COpenGLView::Reset()
{
	// trung gian
	m_dZooming = 0.0;
	m_v3dScale = Vec3D(1.0, 1.0, 1.0);
	m_v3dTranslate = Vec3D();
	m_v3dRotate = Vec3D();
	m_bLMouseDown = false;
	m_bRMouseDown = false;
	m_bMMouseDown = false;
	// tinh toan bien trung gian
	m_v3d_Up = m_Scene.m_v3dUp;
	m_v3d_Out = m_Scene.m_v3dEye.Normalized();
	m_v3d_Left = m_v3d_Up.Cross(m_v3d_Out);
}

// draw objects here - call this function on CxxxView::OnPaint
void COpenGLView::OnPaint(void)
{
	// ColorBack ground
	//glClearColor(m_Scene.m_v3dColorBgrd.x, m_Scene.m_v3dColorBgrd.y, m_Scene.m_v3dColorBgrd.z, m_Scene.m_dAlpha);
	//
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//
	//glLoadIdentity();  // fucking code
	//glPushMatrix();
	// xu li chuot
	// zooming
	Vec3D m_v3dZoom = m_v3d_Out * m_dZooming;
	if (m_Scene.m_bPerspective)						// zooming for perspective, mouse wheel
	{
		glTranslatef(m_v3dZoom.x, m_v3dZoom.y, m_v3dZoom.z);
	}
	else											// zooming for othorgonal
		glScalef(m_v3dScale.x, m_v3dScale.y, m_v3dScale.z);
	// re chuot phai - sang trai, phai, len, xuong: 
	// v3dTranslate = y*(up x eye) + x*up
	Vec3D
		m_v3dTrans = m_v3dTranslate.y * m_v3d_Left + m_v3dTranslate.x * m_v3d_Up;
	glTranslatef(m_v3dTrans.x, m_v3dTrans.y, m_v3dTrans.z);
	// re chuot trai sang trai-phai, quay quanh y
	// re chuot trai len xuong --> quay sang x
	glRotatef(m_v3dRotate.x, m_v3d_Left.x, m_v3d_Left.y, m_v3d_Left.z);
	glRotatef(m_v3dRotate.y, m_v3d_Up.x, m_v3d_Up.y, m_v3d_Up.z);
	//	z-up
	//glRotatef(-45.0, 0.0, 1.0, 0.0);
	//glRotatef(30.0, 1.0, 0.0, 0.0);
	
	// Double buffer
	//glFlush();
	//SwapBuffers(hDC);
	//ValidateRect(hWnd, NULL);
}


// handler message WM_SIZE on CxxxView::OnSize
void COpenGLView::OnSize(UINT nType, int cx, int cy)
{
	m_nHeight = cy, m_nWidth = cx;
	glViewport(0, 0, m_nWidth, m_nHeight);
	if (cx <=0 || cy <= 0 || nType == SIZE_MINIMIZED)
		return;
	// perspective projection
	float aspect;
	aspect= (float)m_nWidth / (float)m_nHeight;
	// Make default perspective view
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();							// Start using Projection Matrix
	gluPerspective(m_Scene.m_dFovy, aspect, m_Scene.m_dNear, m_Scene.m_dFar);
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();							// Return to Model Matrix
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
		m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
		m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	if (!m_bInitialation)
	{
		//OnViewPerspective();
		OnViewFront();
		m_bInitialation = true;
	}
}


void COpenGLView::drawAxis(double lengthAxis)
{
	glPushMatrix();
	// draw axis
	// disable Light
	if (m_Scene.m_bAxis)
	{
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
		// x
		glColor3f(1.0, 0.0, 0.0);
		glLineWidth(3.0);
		glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(lengthAxis, 0.0, 0.0);
		// y
		glColor3f(0.0, 1.0, 0.0);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, lengthAxis, 0.0);
		// z
		glColor3f(0.0, 0.0, 1.0);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, 0.0, lengthAxis);
		glEnd();	
		if (m_Scene.m_bLight)
		{
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
		}
	}
	glPopMatrix();
}

// view from top y+ to zx surface
void COpenGLView::OnViewTop(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	//
	m_Scene.m_v3dEye = Vec3D(0.0, m_dMaxEye, 0.0);
	m_Scene.m_v3dCenter = Vec3D();
	m_Scene.m_v3dUp = Vec3D(0.0, 0.0, 1.0);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	//  dinh huong cua cac truc toa do
	Reset();
	//
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


// view from y- to xz surface
void COpenGLView::OnViewBottom(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	//
	m_Scene.m_v3dEye = Vec3D(0.0, -m_dMaxEye, 0.0);
	m_Scene.m_v3dCenter = Vec3D();
	m_Scene.m_v3dUp = Vec3D(0.0, 0.0, 1.0);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	//  dinh huong cua cac truc toa do
	Reset();
	//
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


// view from left x- to yz surface
void COpenGLView::OnViewLeft(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	//
	m_Scene.m_v3dEye = Vec3D(-m_dMaxEye, 0.0, 0.0);
	m_Scene.m_v3dCenter = Vec3D();
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	//  dinh huong cua cac truc toa do
	Reset();
	//
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


// view from right x+ to yz surface
void COpenGLView::OnViewRight(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	//
	m_Scene.m_v3dEye = Vec3D(m_dMaxEye, 0.0, 0.0);
	m_Scene.m_v3dCenter = Vec3D();
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	//  dinh huong cua cac truc toa do
	Reset();
	//
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


// view from front z+ to xy surface
void COpenGLView::OnViewFront(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	//
	m_Scene.m_v3dEye = Vec3D(0.0, 0.0, m_dMaxEye);
	m_Scene.m_v3dCenter = Vec3D();
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	//  dinh huong cua cac truc toa do
	Reset();
	//
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


// view from back z- to xy surface
void COpenGLView::OnViewBack(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	//
	m_Scene.m_v3dEye = Vec3D(0.0, 0.0, -m_dMaxEye);
	m_Scene.m_v3dCenter = Vec3D();
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	//  dinh huong cua cac truc toa do
	Reset();
	//
	SendMessage(hWnd, WM_PAINT, 0, 0);
}


void COpenGLView::OnViewPerspective(void)
{
	double m_dMaxEye;
	m_dMaxEye = ((abs(m_Scene.m_v3dEye.x) >= abs(m_Scene.m_v3dEye.y)) ? abs(m_Scene.m_v3dEye.x) : abs(m_Scene.m_v3dEye.y));
	m_dMaxEye = ((m_dMaxEye >= abs(m_Scene.m_v3dEye.z)) ? m_dMaxEye : abs(m_Scene.m_v3dEye.z));
	//
	m_Scene.m_v3dEye = Vec3D(m_dMaxEye, 90.0, -m_dMaxEye);
	m_Scene.m_v3dCenter = Vec3D(0.0, 10.0, 0.0);
	m_Scene.m_v3dUp = Vec3D(0.0, 1.0, 0.0);
	//
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_Scene.m_v3dEye.x, m_Scene.m_v3dEye.y, m_Scene.m_v3dEye.z,
			m_Scene.m_v3dCenter.x, m_Scene.m_v3dCenter.y, m_Scene.m_v3dCenter.z,
			m_Scene.m_v3dUp.x, m_Scene.m_v3dUp.y, m_Scene.m_v3dUp.z);
	//  dinh huong cua cac truc toa do
	Reset();
	//
	SendMessage(hWnd, WM_PAINT, 0, 0);
}