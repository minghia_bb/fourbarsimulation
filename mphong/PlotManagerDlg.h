#pragma once
#include <vector>
//#include "include\numericcomputing.h"
// CPlotManagerDlg dialog

class CPlotManagerDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPlotManagerDlg)

public:
	CPlotManagerDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPlotManagerDlg();

// Dialog Data
	enum { IDD = IDD_PLOT_MANAGER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	// Get render View
	//CMpView* GetRenderView(void);
	CComboBox m_CtrlComboX;
	CComboBox m_CtrlComboY;
	CListBox m_CtrlPlotList;
	// pair X-Y in plot list
	std::vector<int>			m_X;
	std::vector<int>			m_Y;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnAdd();
	afx_msg void OnBnClickedBtnRemove();
	afx_msg void OnBnClickedBtnOk();
	afx_msg void OnBnClickedBtnCancel();
};
