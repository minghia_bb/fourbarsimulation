#include "stdafx.h"
#include "PlotData.h"
#include <fstream>

CPlotData::CPlotData(void)
{
	numpoint = 0;
	thickness = 1.0;
	color[0] = 0.0; color[1] = 0.0; color[2] = 1.0; color[3] = 1.0;
	legends = "f(x)";
	clear();
	// 
	line_type = LINE_CONTINUOUS;
}


CPlotData::~CPlotData(void)
{
}


void CPlotData::clear(void)
{
	X.clear();
	Y.clear();
}
/*

int CPlotData::CreateVector(std::vector<double> &XY, double maxv, double minv, int num)
{
	XY.clear();
	numpoint = num;
	double step = (maxv - minv) / numpoint;
	double tmp = minv;
	while (tmp < maxv)
	{
		XY.push_back(tmp);
		tmp += step;
	}
	return (int)(XY.size());
}



// tinh gia tri ham so
void CPlotData::mapFunction(double(*hamso)(double), std::vector<double> Xv, std::vector<double> &Yv)
{
	Yv.clear();
	int size = Xv.size();
	double fx;
	std::ofstream f("XY.txt", std::ios::out);
	for (int i = 0; i< size; i++)
	{
		fx = hamso(Xv[i]);
		Yv.push_back(fx);
		f<<Xv[i]<<"\t"<<fx<<"\n";
	}
	
	
	f.close();
}


CPlotData::CPlotData(double(*hamso)(double), double minv, double maxv, std::string legend, float colour[4], double thick, bool fx, int numpoint)
{
	if (fx)
	{
		CreateVector(X, maxv, minv, numpoint);
		mapFunction(hamso, X, Y);
	}
	else
	{	
		CreateVector(Y, maxv, minv, numpoint);
		mapFunction(hamso, Y, X);
	}
	legends = legend;
	thickness = thick;
	for (int j = 0; j < 4; j++)
		color[j] = colour[j];
}*/


	// copy construction
CPlotData::CPlotData(const CPlotData& plot)
{
	numpoint = plot.numpoint;
	legends = plot.legends;
	thickness = plot.thickness;
	for (int i = 0; i<4; i++)	color[i] = plot.color[i];
	X = plot.X;
	Y = plot.Y;
	line_type = plot.line_type;
}
