/*******************************************************************************/
/* Le Minh Nghia KSTN CDT 54 *17/09/2013* **************************************/
/* Class COpenGLWnd - khoi tao moi truong OpenGL cho Dialog ********************/
/*******************************************************************************/
#pragma once
#include "opengl.h"
#define FONT (void *)GLUT_BITMAP_8_BY_13

class COpenGLWnd :
	public CWnd
	, public COpenGL
{
public:
	COpenGLWnd(void);
	~COpenGLWnd(void);
	// // create window view
	void CreateWindowView(CString WindowName, CRect &rect, CWnd *parent);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()
};

