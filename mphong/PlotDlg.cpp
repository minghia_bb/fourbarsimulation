// PlotDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mphong.h"
#include "PlotDlg.h"
#include "afxdialogex.h"

// CPlotDlg dialog

IMPLEMENT_DYNAMIC(CPlotDlg, CDialogEx)
	/*
static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,  //X
	ID_SEPARATOR,  //Y
	ID_SEPARATOR,  //Time
};
*/
CPlotDlg::CPlotDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPlotDlg::IDD, pParent)
{
	bGrid = false;
	bAxis = true;
}

CPlotDlg::~CPlotDlg()
{
}

void CPlotDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPlotDlg, CDialogEx)
	ON_WM_SIZE()
	ON_COMMAND(ID_HELP_ABOUTPLOTDLG, &CPlotDlg::OnHelpAboutplotdlg)
	ON_COMMAND(ID_OPTIONS_BCKGRDCOLOR, &CPlotDlg::OnOptionsBckgrdcolor)
	ON_COMMAND(ID_OPTIONS_DRAWGRID, &CPlotDlg::OnOptionsDrawgrid)
	ON_COMMAND(ID_OPTIONS_DRAWAXES, &CPlotDlg::OnOptionsDrawaxes)
	ON_UPDATE_COMMAND_UI(ID_OPTIONS_DRAWGRID, &CPlotDlg::OnUpdateOptionsDrawgrid)
	ON_UPDATE_COMMAND_UI(ID_OPTIONS_DRAWAXES, &CPlotDlg::OnUpdateOptionsDrawaxes)
	ON_WM_INITMENUPOPUP()
	ON_COMMAND(ID_OPTIONS_CURSORINFOR, &CPlotDlg::OnOptionsCursorinfor)
	ON_UPDATE_COMMAND_UI(ID_OPTIONS_CURSORINFOR, &CPlotDlg::OnUpdateOptionsCursorinfor)
	ON_COMMAND(ID_OPTIONS_FITWINDOW, &CPlotDlg::OnOptionsFitwindow)
END_MESSAGE_MAP()


// CPlotDlg message handlers



BOOL CPlotDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// TODO:  Add extra initialization here
	//m_bar.Create(this);
	//m_bar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));
	//RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
	//
	CRect rect;
	// get size and position of the picture control
	//GetDlgItem(IDC_OPENGL)->GetWindowRect(&rect);
	GetClientRect(&rect);
	// create window with OpenGL
	// Create OpenGL control window
	m_Plot.CreateWindowView(_T("MyWindowOpenGL"), rect, this);
	SetWindowTextW(CString(m_Plot.getCaption().c_str()));
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CPlotDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Plot.m_nResize++;
	if (m_Plot.m_nResize > 1)
	{
		m_Plot.OnSize(nType, cx, cy);
		m_Plot.m_nResize = 1;
	}
}


void CPlotDlg::OnHelpAboutplotdlg()
{
	// TODO: Add your command handler code here
	CAboutDlg	dlg;
	dlg.m_strCaption = "About plot";
	dlg.m_strStatic2 = "myplot, Version 2.0";
	dlg.DoModal();
}


void CPlotDlg::OnOptionsBckgrdcolor()
{
	// TODO: Add your command handler code here
	CColorDialog dlg;
	if (dlg.DoModal() == IDOK) 
	{
		m_Plot.SetBackGroundColor(dlg.GetColor());
		m_Plot.Invalidate(0);
	}
}


void CPlotDlg::OnOptionsDrawgrid()
{
	// TODO: Add your command handler code here
	bGrid = !bGrid;
	m_Plot.SetDrawGridAxis(bGrid, bAxis);
	m_Plot.Invalidate(0);
}


void CPlotDlg::OnOptionsDrawaxes()
{
	// TODO: Add your command handler code here
	bAxis = !bAxis;
	m_Plot.SetDrawGridAxis(bGrid, bAxis);
	m_Plot.Invalidate(0);
}


void CPlotDlg::OnUpdateOptionsDrawgrid(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(bGrid);
}


void CPlotDlg::OnUpdateOptionsDrawaxes(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(bAxis);
}


// UpdateCommand message Handler not spported in menu dialog, so we need to handler this message
void CPlotDlg::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
	CDialogEx::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);

	// TODO: Add your message handler code here
	ASSERT(pPopupMenu != NULL);
    // Check the enabled state of various menu items.

    CCmdUI state;
    state.m_pMenu = pPopupMenu;
    ASSERT(state.m_pOther == NULL);
    ASSERT(state.m_pParentMenu == NULL);

    // Determine if menu is popup in top-level menu and set m_pOther to
    // it if so (m_pParentMenu == NULL indicates that it is secondary popup).
    HMENU hParentMenu;
    if (AfxGetThreadState()->m_hTrackingMenu == pPopupMenu->m_hMenu)
        state.m_pParentMenu = pPopupMenu;    // Parent == child for tracking popup.
    else if ((hParentMenu = ::GetMenu(m_hWnd)) != NULL)
    {
        CWnd* pParent = this;
           // Child windows don't have menus--need to go to the top!
        if (pParent != NULL &&
           (hParentMenu = ::GetMenu(pParent->m_hWnd)) != NULL)
        {
           int nIndexMax = ::GetMenuItemCount(hParentMenu);
           for (int nIndex = 0; nIndex < nIndexMax; nIndex++)
           {
            if (::GetSubMenu(hParentMenu, nIndex) == pPopupMenu->m_hMenu)
            {
                // When popup is found, m_pParentMenu is containing menu.
                state.m_pParentMenu = CMenu::FromHandle(hParentMenu);
                break;
            }
           }
        }
    }

    state.m_nIndexMax = pPopupMenu->GetMenuItemCount();
    for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax;
		state.m_nIndex++)
	{
		state.m_nID = pPopupMenu->GetMenuItemID(state.m_nIndex);
		if (state.m_nID == 0)
           continue; // Menu separator or invalid cmd - ignore it.

        ASSERT(state.m_pOther == NULL);
        ASSERT(state.m_pMenu != NULL);
        if (state.m_nID == (UINT)-1)
        {
           // Possibly a popup menu, route to first item of that popup.
           state.m_pSubMenu = pPopupMenu->GetSubMenu(state.m_nIndex);
           if (state.m_pSubMenu == NULL ||
            (state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 ||
            state.m_nID == (UINT)-1)
           {
            continue;       // First item of popup can't be routed to.
           }
           state.DoUpdate(this, TRUE);   // Popups are never auto disabled.
        }
        else
        {
           // Normal menu item.
           // Auto enable/disable if frame window has m_bAutoMenuEnable
           // set and command is _not_ a system command.
           state.m_pSubMenu = NULL;
           state.DoUpdate(this, FALSE);
        }

        // Adjust for menu deletions and additions.
        UINT nCount = pPopupMenu->GetMenuItemCount();
        if (nCount < state.m_nIndexMax)
        {
           state.m_nIndex -= (state.m_nIndexMax - nCount);
           while (state.m_nIndex < nCount &&
            pPopupMenu->GetMenuItemID(state.m_nIndex) == state.m_nID)
           {
            state.m_nIndex++;
           }
        }
        state.m_nIndexMax = nCount;
    }
}


void CPlotDlg::OnOptionsCursorinfor()
{
	// TODO: Add your command handler code here
	m_Plot.bCursorInfor = !m_Plot.bCursorInfor;
	m_Plot.SendMessage(WM_PAINT, 0, 0);
}


void CPlotDlg::OnUpdateOptionsCursorinfor(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_Plot.bCursorInfor);
}


void CPlotDlg::OnOptionsFitwindow()
{
	// TODO: Add your command handler code here
	m_Plot.FitWindow();
	m_Plot.Invalidate(0);
}
