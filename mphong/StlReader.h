#ifndef _STL_READER_H
#define _STL_READER_H

#include <vector>
#include "Vec3D.h"
#include <GL\glut.h>
#include <string>

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#pragma warning( disable : 4018 4996 4244)

#define STL_LABEL_SIZE 80

//structure to hold each facet
class CFacet
{
public:
	CFacet() {}
	~CFacet() {}
	CFacet(const Vec3D& an, const Vec3D& av1, const Vec3D& av2, const Vec3D& av3) { n = an; v[0] = av1; v[1] = av2; v[2] = av3;}
	CFacet(const CFacet& p) { v[0]=p.v[0]; v[1]=p.v[1]; v[2]=p.v[2]; n=p.n;}
	Vec3D n;    //normal
	Vec3D v[3]; //vertices
};

class CStlReader
{
public:
	CStlReader(void);
	~CStlReader(void);

	CStlReader(CStlReader& s);
	CStlReader& operator=(const CStlReader& s);

	std::string ObjectName;

	bool Load(std::string filename, const Vec3D & offset = Vec3D());
	bool Save(std::string filename, bool Binary = true) const;

	void ComputeBoundingBox(Vec3D& pmin, Vec3D& pmax);
	 
	std::vector<CFacet> Facets; // doi tuong chua cac mat ,1 dang danh sach, container

	void Clear() { Facets.clear(); ObjectName = "Default";} // clear/reset the list of trianges
	int Size() const { return (int)Facets.size(); }

	void AddFacet(const Vec3D& n, const Vec3D& v1, const Vec3D& v2, const Vec3D& v3) 
	{
		Facets.push_back(CFacet(n,v1,v2,v3));	
	}
	void AddFacet(float nx, float ny, float nz, 
		float v1x, float v1y, float v1z, 
		float v2x, float v2y, float v2z, 
		float v3x, float v3y, float v3z)
	{ 
		AddFacet(Vec3D(nx, ny, nz), Vec3D(v1x,v1y,v1z), Vec3D(v2x,v2y,v2z), Vec3D(v3x,v3y,v3z)); 
	}

public:
	// file i/o
	bool LoadBinary(std::string filename, const Vec3D & offset = Vec3D());
	bool LoadAscii(std::string filename, const Vec3D & offset = Vec3D());

	void Draw(bool bModelhNormals, bool bShaded);

	//add a facet
		
	//manipulation...
	void Rotate(Vec3D ax, double a);
	void RotX(double a);
	void RotY(double a);
	void RotZ(double a);
	void Scale(Vec3D d);
	void Translate(const Vec3D & d);
};

#endif