//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by mphong.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_DIALOGCONTROL               101
#define IDR_MAINFRAME                   128
#define IDR_mphongTYPE                  130
#define ID_INDICATOR_STATUS             310
#define ID_INDICATOR_X                  311
#define ID_INDICATOR_Y                  312
#define IDD_DIALOG_PLOT                 316
#define IDR_MENU_PLOT                   317
#define IDI_ICON_DHBKHN                 318
#define IDD_PLOT_MANAGER                319
#define IDD_PLOT_SETTING                324
#define IDC_BTN_BROWSER                 1095
#define IDC_RADIO_DHT                   1098
#define IDC_RADIO_DHN                   1099
#define IDC_RADIO_DLHT                  1100
#define IDC_RADIO_DLHN                  1101
#define IDC_FILENAME                    1102
#define IDC_BTN_COMPUTE                 1104
#define IDC_STATIC_2                    1106
#define IDC_COMBO_X                     1107
#define IDC_COMBO_Y                     1108
#define IDC_PLOT_LIST                   1109
#define IDC_BTN_ADD                     1110
#define IDC_BTN_REMOVE                  1111
#define IDC_BTN_CANCEL                  1113
#define IDC_BTN_OK                      1114
#define IDC_BTN_PLAY                    1116
#define IDC_CHECK_TRACE                 1118
#define IDC_EDIT_Q0                     1119
#define IDC_EDIT2                       1120
#define IDC_EDIT_DQ0                    1120
#define ID_OPTIONS_DLG                  32771
#define ID_VIEW_VIEWTOP                 32772
#define ID_VIEW_VIEWBOTTOM              32773
#define ID_VIEW_VIEWLEFT                32774
#define ID_VIEW_VIEWRIGHT               32775
#define ID_VIEW_VIEWFRONT               32776
#define ID_VIEW_VIEWBACK                32777
#define ID_VIEW_AXIS                    32778
#define ID_VIEW_VIEWPERSPECTIVE         32779
#define ID_OPTIONS_BCKGRDCOLOR          32780
#define ID_HELP_ABOUTPLOTDLG            32781
#define ID_OPTIONS_DRAWGRID             32782
#define ID_OPTIONS_DRAWAXES             32783
#define ID_OPTIONS_SETTINGS             32784
#define ID_OPTIONS_PROPERTIES           32785
#define ID_OPTIONS_CURSORINFOR          32786
#define ID_OPTIONS_FITWINDOW            32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        325
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1121
#define _APS_NEXT_SYMED_VALUE           313
#endif
#endif
