/*
   (c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
*/


// vecnorm.h
// Norms of Vectors

#ifndef MVECNORM_H
#define MVECNORM_H

#include <iostream>
#include <math.h>
#include "vector.h"
using namespace std;

template <class T> T norm1(const Vector<T> &v)		// chuan cot/hang
{
	T result(0);
	for(int i = 0; i < int(v.size()); i++) 
		result = result + abs(v[i]);
	return result;
}

double norm1(const Vector<double> &v);


template <class T> double norm2(const Vector<T> &v) // chuan Euclid
{
	T result(0);
	for(int i=0;i<int(v.size());i++) 
		result = result + v[i]*v[i];
	return sqrt(double(result));
}

template <class T> T normI(const Vector<T> &v)
{
   T maxItem(abs(v[0])), temp;
   for(int i=1;i<int(v.size());i++)
   {
      temp = abs(v[i]);
      if(temp > maxItem) maxItem = temp;
   }
   return maxItem;
}

double normI(const Vector<double> &v);


template <class T> Vector<T> normalize(const Vector<T> &v)
{
   Vector<T> result(v.size());
   double length = norm2(v);
   for(int i=0;i<int(v.size());i++) result[i] = v[i]/length;
   return result;
}
#endif
