/*
    (c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
*/


#ifndef IDENTITY_H
#define IDENTITY_H

#include <iostream>
#include <cstdlib>
#include <complex>
using namespace std;

template <class T> T zero(T) { static const T z = T() - T(); return z; }

template <class T> T one(T)
{
 static T z(zero(T()));
 static const T o = ++z;
 return o;
}

template <> char zero(char);
template <> char one(char);
template <> short zero(short);
template <> short one(short);
template <> int zero(int);
template <> int one(int);
template <> long zero(long);
template <> long one(long);
template <> float zero(float);
template <> float one(float);
template <> double zero(double);
template <> double one(double);


template <class T>
complex<T> zero(complex<T>) { return complex<T>(zero(T())); }

template <class T>
complex<T> one(complex<T>) { return complex<T>(one(T())); }

#endif
