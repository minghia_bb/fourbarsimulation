/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
	Giai hpt, tinh dinh thuc va ma tran nghich dao bang pp so
	Giai ptvp bang Runge-Kutta 4
	Xay dung giai thuat dieu khien PD, PID
*/

#ifndef NUMERIC_COMPUTING
#define NUMERIC_COMPUTING
#include <math.h>
#include "matnorm.h"

namespace NC
{
	typedef Matrix<double>	MatrixXd;
	typedef	Vector<double>	VectorXd;

	/* My methods - tinh toan so - numeric computing T = {float or double} */
	int pivot_gauss (MatrixXd &M, int col_th);							// tin tru lon nhat
	double det_gauss(const MatrixXd &M);									// tinh dinh thuc bang pp gauss
	double det_lu (const MatrixXd &M);									// tinh dinh thuc bang phan tich LU
	VectorXd solve_gauss(const MatrixXd &A, const VectorXd &b);		// giai hpt dai so tuyen tinh bang pp gauss
	VectorXd solve_lu(const MatrixXd &A, const VectorXd &b);	// giai hpt dstt bang pp phan tich LU
	VectorXd solve_gauss_jordan(const MatrixXd &A, const VectorXd &b);	// ghpt dstt bang pp gauss-jordan
	MatrixXd inv_gauss(const MatrixXd &M);			// tim ma tran nghich dao bang pp gauss
	MatrixXd inv_lu(const MatrixXd &M);				// tim ma tran nghich dao bang pp ptich LU
	// khoi tao ma tran 
	MatrixXd create_matrix(double **mat, int rowNum, int colNum);
	VectorXd create_vector(double *vec, int num);
	//
	template <class EQN, class DEQN>
	double	fsolve(EQN eqn, DEQN deqn, double x0, int maxIter = 100, double eps = 1e-6);				// giai pt eqn(x) = 0, bang pp lap Newton
	//
	template <class EQN, class DEQN>
	double			fsolve(EQN eqn, DEQN deqn, double x0, double fE, int maxIter = 100, double eps = 1e-6);		// giai pt eqn(x) = fE, bang pp lap Newton
	//
	template <class FUNC, class DIFF_FUNC>
	VectorXd	fsolve(FUNC eqns, DIFF_FUNC deqns, VectorXd x0, int maxIter = 100, double eps = 1e-6); // giai hpt eqns(vx) = 0;
	//
	template <class FUNC, class DIFF_FUNC>
	VectorXd	fsolve(FUNC eqns, DIFF_FUNC deqns, VectorXd x0, VectorXd fE, int maxIter = 100, double eps = 1e-6); // giai hpt eqns(vx) = fE;
	// Giai he phuong trinh vi phan bac 1 - bang pp Runge-Rutta 4
	template <class FUNCS>
	MatrixXd	ode_45(FUNCS fty, VectorXd tspan, VectorXd y0); // y' = f(t,y) = ham lien tuc
	// y' = f(t,y) = g(y) + h(t)
	template <class FUNC1, class FUNC2>
	MatrixXd	ode_45(FUNC1 gy, FUNC2 ht, VectorXd tspan, VectorXd y0); // y' = f(t,y) = g(y) + h(t) = h(t) la ham lien tuc
	//
	template <class FUNC>
	MatrixXd	ode_45(FUNC gy, MatrixXd ht, VectorXd tspan, VectorXd y0); // y' = f(t,y) = g(y) + h(ti) = h(ti) tap cac diem
	// giai he ptvp chuyen dong cua he nhieu vat
	// M.q" + V(q,q') = tau
	template <class DDQ, class MTKL, class V>
	MatrixXd	ode_45(DDQ ddq, MTKL M, V V1, MatrixXd Tau, VectorXd tspan, VectorXd q0, VectorXd dq0); //
	VectorXd	sub_vector(const VectorXd &v_source, int from, int to);
	VectorXd	combine_vector(const VectorXd &v1, const VectorXd &v2);		// noi vecto 2 vao sau vector 1
	MatrixXd	sub_matrix(const MatrixXd &m_source, int r_from, int r_to, int c_from, int c_to);
	// double eig_mises(const MatrixXd &A);					// tim tri rieng bang pp lap Mises
	// double eig_rayleigh(const MatrixXd &A);				// tim tri rieng bang pp Rayleigh
	// double eig_jacobi(const MatrixXd A);					// tim tri rieng va vector rieng cho ma tran doi xung bang pp lap Jacobi
	// Giai thuat dieu khien PD

}
#endif