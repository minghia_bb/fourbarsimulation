/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
	Giai hpt, tinh dinh thuc va ma tran nghich dao bang pp so
	Giai ptvp bang Runge-Kutta 4
	Xay dung giai thuat dieu khien PD, PID
*/

#ifndef NUMERIC_COMPUTING
#define NUMERIC_COMPUTING
#include <math.h>
#include "matnorm.h"

typedef Matrix<double>	MatrixXd;
typedef	Vector<double>	VectorXd;

class NC
{
public:
	/* My methods - tinh toan so - numeric computing T = {float or double} */
	static int pivot_gauss (MatrixXd &M, int col_th);							// tin tru lon nhat
	static double det_gauss(const MatrixXd &M);									// tinh dinh thuc bang pp gauss
	static double det_lu (const MatrixXd &M);									// tinh dinh thuc bang phan tich LU
	static VectorXd solve_gauss(const MatrixXd &A, const VectorXd &b);		// giai hpt dai so tuyen tinh bang pp gauss
	static VectorXd solve_lu(const MatrixXd &A, const VectorXd &b);	// giai hpt dstt bang pp phan tich LU
	static VectorXd solve_gauss_jordan(const MatrixXd &A, const VectorXd &b);	// ghpt dstt bang pp gauss-jordan
	static MatrixXd inv_gauss(const MatrixXd &M);			// tim ma tran nghich dao bang pp gauss
	static MatrixXd inv_lu(const MatrixXd &M);				// tim ma tran nghich dao bang pp ptich LU
	// khoi tao ma tran 
	static MatrixXd create_matrix(double **mat, int rowNum, int colNum);
	static VectorXd create_vector(double *vec, int num);
	//
	static double			fsolve(double (*eqn)(double), double (*deqn)(double), double x0, int maxIter = 100, double eps = 1e-6);		// giai pt eqn(x) = 0, bang pp lap Newton
	static double			fsolve(double (*eqn)(double), double (*deqn)(double), double x0, double fE, int maxIter = 100, double eps = 1e-6);		// giai pt eqn(x) = fE, bang pp lap Newton
	static VectorXd	fsolve(VectorXd (*eqns)(VectorXd), MatrixXd (*deqns)(VectorXd), VectorXd x0, int maxIter = 100, double eps = 1e-6); // giai hpt eqns(vx) = 0;
	static VectorXd	fsolve(VectorXd (*eqns)(VectorXd), MatrixXd (*deqns)(VectorXd), VectorXd x0, VectorXd fE, int maxIter = 100, double eps = 1e-6); // giai hpt eqns(vx) = fE;
	// Giai he phuong trinh vi phan bac 1 - bang pp Runge-Rutta 4
	static MatrixXd	ode_45(VectorXd (*fty)(double t, VectorXd y), VectorXd tspan, VectorXd y0); // y' = f(t,y) = ham lien tuc
	// y' = f(t,y) = g(y) + h(t)
	static MatrixXd	ode_45(VectorXd (*gy)(VectorXd y), VectorXd (*ht)(double t), VectorXd tspan, VectorXd y0); // y' = f(t,y) = g(y) + h(t) = h(t) la ham lien tuc
	static MatrixXd	ode_45(VectorXd (*gy)(VectorXd y), MatrixXd ht, VectorXd tspan, VectorXd y0); // y' = f(t,y) = g(y) + h(ti) = h(ti) tap cac diem
	// giai he ptvp chuyen dong cua he nhieu vat
	// M.q" + V(q,q') = tau
	static MatrixXd	ode_45(VectorXd (*ddq)(MatrixXd M, VectorXd V, VectorXd tau), MatrixXd (*M1)(VectorXd q), VectorXd (*V1)(VectorXd q, VectorXd dq), MatrixXd Tau, VectorXd tspan, VectorXd q0, VectorXd dq0); //
	static VectorXd	sub_vector(const VectorXd &v_source, int from, int to);
	static VectorXd	combine_vector(const VectorXd &v1, const VectorXd &v2);		// noi vecto 2 vao sau vector 1
	static MatrixXd	sub_matrix(const MatrixXd &m_source, int r_from, int r_to, int c_from, int c_to);
	//static double eig_mises(const MatrixXd &A);					// tim tri rieng bang pp lap Mises
	//static double eig_rayleigh(const MatrixXd &A);				// tim tri rieng bang pp Rayleigh
	//static double eig_jacobi(const MatrixXd A);					// tim tri rieng va vector rieng cho ma tran doi xung bang pp lap Jacobi
	// Giai thuat dieu khien PD

};
#endif