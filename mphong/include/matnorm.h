/*
    (c) by Le Minh Nghia - KSTN CDT k54 - HUST - 23/9/2013
*/


// matnorm.h
// Norms of Matrices

#ifndef MATNORM_H
#define MATNORM_H

#include <iostream>
#include <math.h>
#include "vector.h"
#include "vecnorm.h"
#include "matrix.h"
#include "identity.h"

using namespace std;

template <class T> T norm1(const Matrix<T> &m) // chuan cot
{
	T maxItem(0), temp;
	int i,j;
	for(i=0;i < m.rows();i++)
		maxItem += m[i][0];
	//maxItem = norm(m(0));			// operator (int) -> tra ve vector column
	for(i=1; i<m.cols();i++)
	{
		temp = zero(T());
		for(j = 0;j < m.rows(); j++) 
			temp += abs(m[j][i]);
		if(temp > maxItem) 
			maxItem = temp;
	}
	return maxItem;
}

template <class T> T normI(const Matrix<T> &m) // chuan vo cung, chuan hang
{
	T maxItem(norm1(m[0]));

	for(int i=1;i<m.rows();i++)
		if(norm1(m[i]) > maxItem) maxItem = norm1(m[i]);
	return maxItem;
}

template <class T> T normH(const Matrix<T> &m)		// chuan Euclide
{
	return	sqrt((m*(m.transpose())).trace());
}
#endif
