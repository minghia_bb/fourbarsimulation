/*
	(c) by Le Minh Nghia - KSTN CDT k54 - HUST - 26/9/2013
	Du lieu 1 plot - 1 line
*/

#pragma once
#include <vector>
#define LINE_CONTINUOUS			0
#define LINE_PLUS				1
#define LINE_DASH				2
#define LINE_DOT				3
#define LINE_DASH_DOT			4
#define LINE_ASTERISK			5

#define drawOneLine(x1,y1,x2,y2) glBegin(GL_LINES); glVertex2f((x1),(y1));glVertex2f((x2),(y2));glEnd();

class CPlotData
{
public:
	int numpoint;	
	std::string legends;			// legend
	double thickness;				// lines thickness
	float color[4];					// color
	std::vector<double> X;			// data of x axes
	std::vector<double> Y;			// data for y axes
	int line_type;					// type of line

	CPlotData(void);
	~CPlotData(void);
	void clear(void);
	// copy construction
	CPlotData(const CPlotData& plot);
};

