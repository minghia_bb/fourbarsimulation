#pragma once
#include "mophongView.h"
#include "MainFrm.h"

// CDialogControl form view

class CDialogControl : public CFormView
{
	DECLARE_DYNCREATE(CDialogControl)

protected:
	CDialogControl();           // protected constructor used by dynamic creation
	virtual ~CDialogControl();

public:
	enum { IDD = IDD_DIALOGCONTROL };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	bool	m_bPlay;
	virtual void OnInitialUpdate();
	// get CMpView Class
	CMpView* GetRenderView(void);
	CString m_strFileName;
	CString m_fileName;
	int m_nLoaiBaiToan;
	afx_msg void OnBrowser();
	afx_msg void OnCompute();
	afx_msg void OnBnClickedBtnPlay();
	afx_msg void OnDrawTrace();
	afx_msg void OnBnClickedRadioDlht();
	afx_msg void OnBnClickedRadioDht();
	double m_q0;
	double m_dq0;
};


