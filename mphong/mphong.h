
// mphong.h : main header file for the mphong application
//
#pragma once
#include "aboutdlg.h"

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMpApp:
// See mphong.cpp for the implementation of this class
//

class CMpApp : public CWinApp
{
public:
	CMpApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMpApp theApp;


