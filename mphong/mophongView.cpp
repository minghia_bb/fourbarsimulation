
// mophongView.cpp : implementation of the CMpView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "mphong.h"
#endif

#include "mophongDoc.h"
#include "mophongView.h"
/* Mycode begins*/
#include "MainFrm.h"
/* Mycode ends*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMpView

IMPLEMENT_DYNCREATE(CMpView, CView)

BEGIN_MESSAGE_MAP(CMpView, CView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CMpView construction/destruction

CMpView::CMpView()
{
	// TODO: add construction code here
	m_bDrawTrace = false;
	m_vListPhi.clear();
	m_vq .clear();
	i_th = 0;
}

CMpView::~CMpView()
{
}

BOOL CMpView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.style |= WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	return CView::PreCreateWindow(cs);
}

// CMpView drawing

void CMpView::OnDraw(CDC* /*pDC*/)
{
	CMpDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CMpView diagnostics

#ifdef _DEBUG
void CMpView::AssertValid() const
{
	CView::AssertValid();
}

void CMpView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMpDoc* CMpView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMpDoc)));
	return (CMpDoc*)m_pDocument;
}
#endif //_DEBUG


// CMpView message handlers


int CMpView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	/* MyCode Begins*/
	GetDocument()->m_OpenGLView.hWnd = m_hWnd;
	GetDocument()->m_OpenGLView.OnCreate(::GetDC(m_hWnd));
	//
	FILE* f = fopen(GetDocument()->m_OpenGLView.m_Robot.stlFile[0].ObjectName.c_str(), "r");
	if (!f)
	{
		FILE* ff = fopen("stl/link1.stl", "r");
		if (!ff)
		{
			GetDocument()->m_OpenGLView.m_Robot.stlFile[0].ObjectName = "link1.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[1].ObjectName = "link2.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[2].ObjectName = "link3.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[3].ObjectName = "base0.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[4].ObjectName = "base1.stl";
		}
		else
		{
			GetDocument()->m_OpenGLView.m_Robot.stlFile[0].ObjectName = "stl/link1.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[1].ObjectName = "stl/link2.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[2].ObjectName = "stl/link3.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[3].ObjectName = "stl/base0.stl";
			GetDocument()->m_OpenGLView.m_Robot.stlFile[4].ObjectName = "stl/base1.stl";
			fclose(ff);
		}
	}
	else
		fclose(f);
	//
	for (int i = 0; i < GetDocument()->m_OpenGLView.m_Robot.numLink; i++)
		GetDocument()->m_OpenGLView.m_Robot.stlFile[i].Load(GetDocument()->m_OpenGLView.m_Robot.stlFile[i].ObjectName);
	// optimus display
	// creating display list
	GetDocument()->m_OpenGLView.m_Robot.displayLists[0] = glGenLists(GetDocument()->m_OpenGLView.m_Robot.numLink);
	for (int i = 0; i < GetDocument()->m_OpenGLView.m_Robot.numLink ;i ++)
	{
		if (i != 0)
			GetDocument()->m_OpenGLView.m_Robot.displayLists[i] = 
			GetDocument()->m_OpenGLView.m_Robot.displayLists[i-1] + 1; // xac dinh dia chi cua list tiep theo
		glNewList(GetDocument()->m_OpenGLView.m_Robot.displayLists[i], GL_COMPILE);
		GetDocument()->m_OpenGLView.m_Robot.stlFile[i].Draw(false, true);
		glEndList();
	}
	// setting viewing
	GetDocument()->m_OpenGLView.m_Scene.m_dFovy = 45.0;
	/* MyCode Ends*/

	return 0;
}


void CMpView::OnDestroy()
{
	CView::OnDestroy();

	// TODO: Add your message handler code here
	/* MyCodes Begin*/
	GetDocument()->m_OpenGLView.OnDestroy();
	/* MyCodes End*/
}


BOOL CMpView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default

	//return CView::OnEraseBkgnd(pDC);
	/* MyCodes Begin*/
	return TRUE;
	/* MyCodes End*/
}

/* Ve cac doi tuong here */
void CMpView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CView::OnPaint() for painting messages
	// My datas --------
	// -----------------
	glClearColor(GetDocument()->m_OpenGLView.m_Scene.m_v3dColorBgrd.x, 
		GetDocument()->m_OpenGLView.m_Scene.m_v3dColorBgrd.y, 
		GetDocument()->m_OpenGLView.m_Scene.m_v3dColorBgrd.z, 
		GetDocument()->m_OpenGLView.m_Scene.m_dAlpha);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//
	glPushMatrix();
		GetDocument()->m_OpenGLView.OnPaint();		// xu li chuot
		glTranslatef(-300, 0.0, 0.0);
		//glRotatef(-90.0, 1.0, 0.0, 0.0);				// quay quanh x 1 goc -90, z - is up
		//GetDocument()->m_OpenGLView.drawAxis();
		//
		glPushMatrix();
		GetDocument()->m_OpenGLView.m_Robot.compute();
		//
		int i = GetDocument()->m_OpenGLView.m_Robot.numLink - 2;
		glMaterialfv(GL_FRONT, GL_DIFFUSE, GetDocument()->m_OpenGLView.m_Robot.color[i]);
		glMaterialfv(GL_FRONT, GL_SPECULAR, GetDocument()->m_OpenGLView.m_Robot.color[i]);
		glCallList(GetDocument()->m_OpenGLView.m_Robot.displayLists[i]);
		//
		i = GetDocument()->m_OpenGLView.m_Robot.numLink - 1;
		glPushMatrix();
		glTranslatef(350.0, -100.0, 0.0);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, GetDocument()->m_OpenGLView.m_Robot.color[i]);
		glMaterialfv(GL_FRONT, GL_SPECULAR, GetDocument()->m_OpenGLView.m_Robot.color[i]);
		glCallList(GetDocument()->m_OpenGLView.m_Robot.displayLists[i]);
		glPopMatrix();
		for (int i =0 ; i < GetDocument()->m_OpenGLView.m_Robot.numLink - 2; i++)
		{
			glTranslated(GetDocument()->m_OpenGLView.m_Robot.a[i], 0.0, 0.0);
			glRotated(GetDocument()->m_OpenGLView.m_Robot.phi[i], 0.0, 0.0, 1.0);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, GetDocument()->m_OpenGLView.m_Robot.color[i]);
			glMaterialfv(GL_FRONT, GL_SPECULAR, GetDocument()->m_OpenGLView.m_Robot.color[i]);
			glCallList(GetDocument()->m_OpenGLView.m_Robot.displayLists[i]);
		}
		glPopMatrix();
		// draw trace
		glPushMatrix();
		if (m_bDrawTrace) // dht
		{
			double xe, ye, ze, phi0;
			glDisable(GL_LIGHTING);
			glDisable(GL_LIGHT0);
			glLineWidth(1.5);
			glColor3f(1.0, 1.0, 1.0);			// white
			
			int size;
			if (GetDocument()->m_nLoaiBaiToan == 0)
				size = m_vListPhi.size();
			else
				size = i_th;
			glBegin(GL_LINE_STRIP);
			for (int j = 0; j < size; j++)
			{
				if (GetDocument()->m_nLoaiBaiToan == 0)
					phi0 = m_vListPhi[j];
				else
					phi0 = bound(RADTODEG(m_vq[j]));
				xe = 200.0 * cos(DEGTORAD(phi0));
				ye = 200.0 * sin(DEGTORAD(phi0));
				ze = 0.0;
				glVertex3d(xe, ye, ze);
			}
			glEnd();
			//
			glColor3f(0.0, 0.0, 1.0);			// blue
			glBegin(GL_LINE_STRIP);
			for (int j = 0; j < size; j++)
			{
				if (GetDocument()->m_nLoaiBaiToan == 0)
					phi0 = m_vListPhi[j];
				else
					phi0 = bound(RADTODEG(m_vq[j]));
				xe = 200 * cos(DEGTORAD(phi0)) + 400.0 * cos(GetDocument()->m_OpenGLView.m_Robot.phi2(DEGTORAD(phi0)));
				ye = 200 * sin(DEGTORAD(phi0)) + 400.0 * sin(GetDocument()->m_OpenGLView.m_Robot.phi2(DEGTORAD(phi0)));
				ze = 0.0;
				glVertex3d(xe, ye, ze);
			}
			glEnd();
			//
			glColor3f(0.1, 0.2, 0.4);
			glBegin(GL_LINE_STRIP);
			for (int j = 0; j < size; j++)
			{
				if (GetDocument()->m_nLoaiBaiToan == 0)
					phi0 = bound(m_vListPhi[j]);
				else
					phi0 = bound(RADTODEG(m_vq[j]));
				xe = 200 * cos(DEGTORAD(phi0)) + 250.0 * cos(GetDocument()->m_OpenGLView.m_Robot.phi2(DEGTORAD(phi0)));
				ye = 200 * sin(DEGTORAD(phi0)) + 250.0 * sin(GetDocument()->m_OpenGLView.m_Robot.phi2(DEGTORAD(phi0)));
				ze = 0.0;
				glVertex3d(xe, ye, ze);
			}
			glEnd();

			if (GetDocument()->m_OpenGLView.m_Scene.m_bLight)
			{
				glEnable(GL_LIGHTING);
				glEnable(GL_LIGHT0);
			}
			glLineWidth(1.0);
		}
		glPopMatrix();
	glPopMatrix();
	// Double buffer
	//glFlush();
	SwapBuffers(GetDocument()->m_OpenGLView.hDC);
}


void CMpView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	GetDocument()->m_OpenGLView.OnSize(nType, cx, cy);
}

void CMpView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == GetDocument()->m_OpenGLView.m_nTimerID)
	{
		if (GetDocument()->m_nLoaiBaiToan == 0)
		{
			m_vListPhi.push_back(GetDocument()->m_OpenGLView.m_Robot.phi[0]);
			//
			GetDocument()->m_OpenGLView.m_Robot.phi[0] += 1.11;
			if (GetDocument()->m_OpenGLView.m_Robot.phi[0] > 360.0)
				GetDocument()->m_OpenGLView.m_Robot.phi[0] = 0.0;
			//
			if (m_vListPhi.size() > 0 && GetDocument()->m_OpenGLView.m_Robot.phi[0] == m_vListPhi[0])
				m_vListPhi.clear();
		}
		else
		{
			GetDocument()->m_OpenGLView.m_Robot.phi[0] = RADTODEG(m_vq[i_th]);
			i_th++;
			if (i_th >= m_vq.size())
				i_th = 0;
		}
		SendMessage(WM_PAINT, 0, 0);
	}
	CView::OnTimer(nIDEvent);
}


void CMpView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bLMouseDown = true;	// chuot trai duoc an
	GetDocument()->m_OpenGLView.m_pOldMouse = point;
	CView::OnLButtonDown(nFlags, point);
}


void CMpView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bLMouseDown = false;		// chuot trai duoc nha ra
	CView::OnLButtonUp(nFlags, point);
}


void CMpView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bRMouseDown = true;
	GetDocument()->m_OpenGLView.m_pOldMouse = point;
	CView::OnRButtonDown(nFlags, point);
}


void CMpView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bRMouseDown = false;
	CView::OnRButtonUp(nFlags, point);
}


void CMpView::OnMButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bMMouseDown = true;
	CView::OnMButtonDown(nFlags, point);
}


void CMpView::OnMButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	GetDocument()->m_OpenGLView.m_bMMouseDown = false;
	CView::OnMButtonUp(nFlags, point);
}


void CMpView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	/* Mycodes Begin */
	// an 2 chuot de zooming
	if (GetDocument()->m_OpenGLView.m_bLMouseDown && GetDocument()->m_OpenGLView.m_bRMouseDown)
	{
		if (GetDocument()->m_OpenGLView.m_Scene.m_bPerspective)
		{
			GetDocument()->m_OpenGLView.m_dZooming += (point.y - GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
		}
		else
		{
			double m_dScale = (point.y - GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
			if (m_dScale != 0.0)
				GetDocument()->m_OpenGLView.m_v3dScale += Vec3D(m_dScale);
			if (GetDocument()->m_OpenGLView.m_v3dScale.x <= 0.0001)  // scale < 0
				GetDocument()->m_OpenGLView.m_v3dScale = Vec3D(0.01);
		}
		GetDocument()->m_OpenGLView.m_pOldMouse = point;
		SendMessage(WM_PAINT, 0, 0);
		return;
	}
	// rotation
	if (GetDocument()->m_OpenGLView.m_bLMouseDown && GetDocument()->m_OpenGLView.m_Scene.m_bPerspective)
	{
		if (abs(GetDocument()->m_OpenGLView.m_v3dRotate.x) >= 360.0)
			GetDocument()->m_OpenGLView.m_v3dRotate.x = 0.0;
		if (abs(GetDocument()->m_OpenGLView.m_v3dRotate.y) >= 360.0)
			GetDocument()->m_OpenGLView.m_v3dRotate.y = 0.0;
		GetDocument()->m_OpenGLView.m_v3dRotate.x += (point.y - GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
		GetDocument()->m_OpenGLView.m_v3dRotate.y += (point.x - GetDocument()->m_OpenGLView.m_pOldMouse.x) / 10.0;
		// update position of mouse
		GetDocument()->m_OpenGLView.m_pOldMouse = point;
		SendMessage(WM_PAINT, 0, 0);
	}
	// moving
	if (GetDocument()->m_OpenGLView.m_bRMouseDown)
	{
		GetDocument()->m_OpenGLView.m_v3dTranslate.x += (-point.y + GetDocument()->m_OpenGLView.m_pOldMouse.y) / 10.0;
		GetDocument()->m_OpenGLView.m_v3dTranslate.y += (point.x - GetDocument()->m_OpenGLView.m_pOldMouse.x) / 10.0;
		// update position of mouse
		GetDocument()->m_OpenGLView.m_pOldMouse = point;
		SendMessage(WM_PAINT, 0, 0);
	}
	
	CMpApp* pApp=(CMpApp*) AfxGetApp();
	CMainFrame* pMainFrame=(CMainFrame*) pApp->m_pMainWnd;
	CStatusBar* pStatus= &pMainFrame->m_wndStatusBar;
	CString str;
	str.Format(_T("X: %d"),point.x);
	pStatus->SetPaneText(1, str);
	str.Format(_T("Y: %d"),point.y);
	pStatus->SetPaneText(2, str);
	/* Mycodes End */
	CView::OnMouseMove(nFlags, point);
}

BOOL CMpView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default
	// zooming for perspective
	if (GetDocument()->m_OpenGLView.m_Scene.m_bPerspective)
	{
		GetDocument()->m_OpenGLView.m_dZooming += (float) zDelta / 10.0;
	}
	else	// zooming for othorgonal
	{
		double m_dScale = zDelta / 100.0;
		if (m_dScale > 0)
		{
			GetDocument()->m_OpenGLView.m_v3dScale /= Vec3D(m_dScale);
		}
		else
		{
			GetDocument()->m_OpenGLView.m_v3dScale *= Vec3D(-m_dScale);
		}
	}
	SendMessage(WM_PAINT, 0, 0);
	return CView::OnMouseWheel(nFlags, zDelta, pt);
}
